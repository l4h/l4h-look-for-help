<?php
session_start();
include ("../php/outils/fonctionsConnexion.php");
include ("../php/outils/fonctionsOffresDemandes.php");
include ("../php/outils/fonctionsUtilisateurs.php");
include ("../php/outils/requetes.php");
include ("../php/outils/fonctions.php");
include ("../php/outils/messagesErreurs.php");
include ("utils.php"); 
const MY_REQUEST = 3;
const REQUEST = 2;
const MY_OFFRES = 1;
const OFFRES = 0;
main();
?>

<?php

	function main(){
		$bdd = connect_bdd();
		$data = array('return' => null);
		$filtre = getFilter($bdd);
		if(connected() && validPostFields(['mode'], $bdd)){
			switch ($_POST['mode']){
				case MY_OFFRES: $liste = liste_mes_offres_query($bdd, $_SESSION['id'], $filtre);
					break;
				case OFFRES: $liste = liste_offres_valables($bdd, $filtre);
					break;
				case REQUEST: $liste = liste_demandes_valable($bdd, $filtre);
					break;
				case MY_REQUEST: $liste = liste_mes_demandes_query($bdd, $_SESSION['id'], $filtre);
					break;
				default: 
					$data['return'] = ERROR;
					sendToAndroid($data);
					$bdd->close();
					return;
					
			}		
			$data['return'] = OK;
			$data['size'] = $liste->num_rows;
			for($i = 0; $i < $liste->num_rows; $i++)
				$data[] = mysqli_fetch_array($liste); //ajoute une ligne au tableau
			sendToAndroid($data);
		}
		
		$bdd->close();
	}
	
	/**
	 * Renvoie les filtres vérifiés contre les injections
	 * @param unknown $bdd
	 * @return unknown|string
	 */
	function getFilter($bdd){
		if(validPostFields(array('filters'), $bdd))
			return $_POST['filters']; 
		else return '%';
	}
?>
