<?php
session_start();
include ("../php/outils/fonctions.php");
include ("../php/outils/fonctionsCaptcha.php");
include ("../php/outils/fonctionsConnexion.php");
include ("../php/outils/fonctionsImage.php");
include ("../php/outils/fonctionsOffresDemandes.php");
include ("../php/outils/fonctionsUtilisateurs.php");
include ("../php/outils/recaptchalib.php");
include ("../php/outils/messagesErreurs.php");
include ("../php/outils/requetes.php");
include ("../android/utils.php");

main();
?>

<?php

function main(){
	
	if(connected()){
		$bdd = connect_bdd();
		
		fill_var_session(getUser($_SESSION['email'], $bdd));
		$data = array(	'id' => $_SESSION['id'],
				'prenom' => $_SESSION['prenom'],
				'nom' => $_SESSION['nom'],
				'rue' => $_SESSION['rue'],
				'email'=>$_SESSION['email'],
				'numero' => $_SESSION['numero'],
				'codePostal'=>$_SESSION['codePost'],
				'ville' => $_SESSION['ville'],
				'pays'=>$_SESSION['pays'],
				
				'return' => OK);
		$bdd->close();
	}else $data = array('return' => 'non connecté');
		
		sendToAndroid($data);
		
}

/**
 * Cherche dans la bdd et renvoie l'utilisateur correspondant a l'email en param
 * @param unknown $email
 * @param unknown $bdd
 */
function getUser($email, $bdd){
	return mysqli_fetch_array(select_all_from_users($bdd, $email));
}

?>
