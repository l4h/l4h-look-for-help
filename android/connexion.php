<?php
session_start();
include ("../php/outils/fonctions.php");
include ("../php/outils/fonctionsCaptcha.php");
include ("../php/outils/fonctionsConnexion.php");
include ("../php/outils/fonctionsImage.php");
include ("../php/outils/fonctionsOffresDemandes.php");
include ("../php/outils/fonctionsUtilisateurs.php");
include ("../php/outils/recaptchalib.php");
include ("../php/outils/requetes.php");
include ("../php/outils/messagesErreurs.php"); 
include ("utils.php");
main ($tab_messErreurs);
?>


<?php

	function main($err){
		$bdd = connect_bdd();
		$data = array('return' => 'Connexion impossible');

		$param = array('email', 'mdp');
		if(validPostFields($param, $bdd)){
			$user = getUser($_POST['email'], $bdd);
			if(decrypter_pwd_salt ( $_POST ['mdp'], $user) and profilIsValid($user['id'], $bdd)){
				$data = array(	'id' => $user['id'],
								'return' => '0');
				$data['types'] = getAdvertTypes();
				$data['errors'] = $err;
				fill_var_session($user);
			}else 
				$data = array('return' => 'Identifiants incorrects');
		}
		sendToAndroid($data);
		
		$bdd->close();
	}
	
	/**
	 * Cherche dans la bdd et renvoie l'utilisateur correspondant a l'email en param
	 * @param unknown $email
	 * @param unknown $bdd
	 */
	function getUser($email, $bdd){
		return mysqli_fetch_array(select_all_from_users($bdd, $email));
	}
	
	/**
	 * 
	 * @param $id
	 * @param $bdd
	 * @return boolean true si utilisateur actif
	 */
	function profilIsValid($id, $bdd){
		$profilId = mysqli_fetch_array (select_max_profilValue($bdd,$id));
		return $profilId['valeur'] <= 30;
	}
	
	/**
	 * Liste de tous les tagId
	 */
	function getAdvertTypes(){
		$data = array(); $mysqli = connect_bdd();
		$query = "SELECT tagId, tagNom from tags";
		if ($result = $mysqli->query ( $query )) {
				
			/* Récupération un tableau associatif */
			while ( $row = mysqli_fetch_array($result) ) {
				$data[$row['tagId']] = $row['tagNom'];
			}
			$result->free ();
		}
		$mysqli->close ();
		return $data;
	}	
?>