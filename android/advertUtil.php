<?php 
/*
 * La page permet la création d'une offre. 
 * A placer en post :
 * mode = string, demande ou offre
 * titre = string, titre de l'annonce
 * description = string
 * time = string, date d'expiration au format Y-m-d H:i:s
 */
session_start();
include ("../php/outils/fonctionsConnexion.php");
include ("../php/outils/fonctionsOffresDemandes.php");
include ("../php/outils/fonctionsUtilisateurs.php");
include ("../php/outils/fonctionsOffresRelations.php");
include ("../php/outils/requetes.php");
include ("../php/outils/fonctions.php");
include ("../php/outils/fonctionsAnnoncesCreation.php");
include ("../php/outils/messagesErreurs.php");
include ("utils.php");

const REQUEST = 'demande';
const OFFRE = 'offre';
const ANS_ADVERT = 4;

if(isset($_POST['mode']) && connected())
		main();
?>

<?php
	function main(){
		if(isCreation($_POST['mode']))
			createAdvert();
		if(isAnswer($_POST['mode']))
			answerAdvert();
	}
	

?>
<?php 
/**
 * Lie une annonce a l'utilisateur
 * Besoin en POST advertId et mode
 */
function answerAdvert(){
	$bdd = connect_bdd();
	
	$data = array('return' => null);
	if(isValidAnswer($bdd)){
		insertAnnonceListeAttente($bdd, $_POST['mode'], $_POST['advertId'], $_SESSION['id']);
		$data['return']= ANSWERED;
	}else
		$data['return'] = ERROR.'erreur d input';
	sendToAndroid($data);
	
	$bdd->close();
}

/**
 * Besoin en POST du mode, titre, description, date-heure
 */
function createAdvert(){
	$bdd = connect_bdd();
	$valid = true;
	$data = array('return' => null);
	$params = array('titre','description', 'time', 'tagId');
	
	if(validPostFields($params, $bdd)){
		$today = date("Y-m-d H:i:s");
		$date_limit = date('Y-m-d H:i:s', strtotime("+14 days"));
			
			
		$valid *= compareDateAnnonceCreation($bdd,$today,$_POST['time'],$date_limit,$data['return']);
		
		$temp = verifPossibiliteCreationAnnonce($bdd,$_SESSION['id'],$_POST['mode']);
		if(!$temp) $data['return'][] = CREATION_ANNONCE_PAS_POSSIBLE;
		$valid *= $temp;
			
		if($valid){
			insertNewAnnonce($bdd,$_POST['mode'],
								$_POST['description'],
								$_POST['titre'],
								$today,
								$_POST['time'],
								$_POST['tagId']);
			$data['return'] = INSERTED;
		}
			
	} else $data['return'] = ERROR;
	sendToAndroid($data);
	$bdd->close();
}

function isCreation($mode){
	return ($mode == REQUEST) || ($mode == OFFRE);
}

function isAnswer($mode){
	return $mode == ANS_ADVERT;
}

/**
 * Test si l'annonce est répondable
 * @param unknown $bdd ouverte
 */
function isValidAnswer($bdd){
	$param = array('advertId');
	$valid = validPostFields($param, $bdd); //pas d'injection
	$valid *= $valid
				&& !ownAnnonce($bdd, $_POST['advertId'],$_SESSION ['id'],$_POST['mode']) //pas de réponse a sa propre annonce
				&& annonceExiste($bdd, $_POST['mode'], $_POST['advertId']); //l'annonce existe
	$valid *= $valid
				&& !isAlreadyAnswered($bdd, $_POST['advertId'], $_SESSION['id'], $_POST['mode']); //on ne répond qu'une fois a une annonce
	return $valid;
}

/**
 * Test si l'advert n'est pas déjà reliée à l'utilisateur
 * @param unknown $bdd ouverte
 * @param unknown $adId id de l'advert
 */
function isAlreadyAnswered($bdd, $adId, $userId, $mode){
	$query = $bdd->query('SELECT COUNT(*) AS attente FROM annonce_liste_attente 
			WHERE annonceId = ' . $adId . '
			AND userId = ' . $userId);
	return mysqli_fetch_array($query)['attente'];
}
?>