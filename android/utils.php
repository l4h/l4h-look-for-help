<?php

	/**
	 * Post : Les valeurs ne sont pas vides et ne risquent pas d'injections sql
	 * @param String[] namesFields
	 */
	function validPostFields($namesFields, $bdd){
		$valid = true;
		foreach ($namesFields as $x){
			$valid *= (isset($_POST[$x]) or !empty($_POST[$x]));
			if($valid)
				$_POST[$x] = $bdd->real_escape_string($_POST[$x]);
		}
		return $valid;
	}
	
	/**
	 * Converti au format JSON le tableau en param
	 * @param unknown $data un tableau
	 */
	function sendToAndroid($data){
		print (json_encode($data));
	}

?>