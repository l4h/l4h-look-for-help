<?php
session_start ();
include ("../php/outils/fonctions.php");
include ("../php/outils/fonctionsCaptcha.php");
include ("../php/outils/fonctionsConnexion.php");
include ("../php/outils/fonctionsImage.php");
include ("../php/outils/fonctionsOffresDemandes.php");
include ("../php/outils/fonctionsUtilisateurs.php");
include ("../php/outils/recaptchalib.php");
include ("../php/outils/requetes.php");
include ("../php/outils/messagesErreurs.php");
include ("utils.php");

main ();

?>

<?php
function main() {
	$bdd = connect_bdd ();
	$params = array (
			'nom',
			'prenom',
			'rue',
			'numero',
			'codePostal',
			'ville',
			'pays' 
	);
	$id = $_SESSION ['id'];
	
	$result= array('return' => 'paramètres non valide');
	if (validPostFields ( $params, $bdd )) {
		
		
		$nom = $_POST ['nom'];
		$prenom = $_POST ['prenom'];
		$rue = $_POST ['rue'];
		$numero = $_POST ['numero'];
		$codePostal = $_POST ['codePostal'];
		$ville = $_POST ['ville'];
		$pays = $_POST ['pays'];
		
		update_nom ( $bdd, $id, $nom );
		update_prenom ( $bdd, $id, $prenom );
		update_rue($bdd, $id, $rue);
		update_numero($bdd, $id, $numero);
		update_codePost($bdd, $id, $codePostal);
		update_ville($bdd, $id, $ville);
		update_pays($bdd, $id, $pays);
		
		$result['return']=OK;
	}
	sendToAndroid($result);
}

?>