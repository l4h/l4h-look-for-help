<?php include('php/header.php');?>
<body>
	<!-- Carousel
    ================================================== -->
    <?php if (connected()){?>
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
			</ol>
			
			
			<div class="carousel-inner">
			
			<div class="item active">
			<blockquote>
              <div class="row">
                <div class="col-sm-4 text-center">
                  <img class="img-circle" src="/images/site/l4h.png" style="width: 100%;height:100%;">
                </div>
                <div class="col-sm-1">
		        </div>
                <div class="col-sm-5">
                </br></br></br></br></br>
                  <h1>Besoin d'aide ?</h1>
							<p>Accédez à la liste des offres ou faites une demande!</p>
							</br>
										<div class="">
										<a class="btn btn-lg btn-primary"
											href="/php/offres/listeOffres.php" role="button">Liste des offres</a>
											<a class="btn btn-lg btn-success"
											href="/php/demandes/demande.php" role="button">Demander</a>
										</div>
							</br>
                </div>
              </div>
            </blockquote>
				</div>
				
				<div class="item">
					<blockquote>
		              <div class="row">
		                <div class="col-sm-4 text-center">
		                  <img class="img-circle" src="/images/site/l4h.png" style="width: 100%;height:100%;">
		                </div>
		                <div class="col-sm-1">
		                </div>
		                <div class="col-sm-5">
		                </br></br></br></br></br>
		                  <h1>Vous voulez aider quelqu'un?</h1>
							<p>Accédez à la liste des demandes ou faites une offre!</p>
							</br>
										<div class="">
										<a class="btn btn-lg btn-primary"
											href="/php/offres/listeOffres.php" role="button">Liste des demandes</a>
											<a class="btn btn-lg btn-success"
											href="/php/demandes/demande.php" role="button">Créer une offre</a>
										</div>
		                </div>
		              </div>
		            </blockquote>
				</div>
	
				
				<div class="item">
					<blockquote>
		              <div class="row">
		                <div class="col-sm-4 text-center">
		                  <img class="img-circle" src="/images/site/l4h.png" style="width: 100%;height:100%;">
		                </div>
		                <div class="col-sm-1">
		                </div>
		                <div class="col-sm-5">
		                </br></br></br></br></br>
		                  <h1>Plus d'avantages?</h1>
									<p>Afin d'obtenir un quota d'offres et demandes illimité, l'affichage de vos annonces dans les newsletters et plus encore ...</br>
									Souscrivez rapidement un compte Premium.</p>
										</br>
										<div class="">
										<a class="btn btn-lg btn-warning"
											href="/php/premium/infoPremium.php" role="button">Plus d'informations</a>
										</div>
									
		                </div>
		              </div>
		            </blockquote>
				</div>
				<div class="item">
					<blockquote>
		              <div class="row">
		                <div class="col-sm-4 text-center">
		                  <img class="img-circle" src="/images/site/android.png">
		                </div>
		                <div class="col-sm-1">
		                </div>
		                <div class="col-sm-5">
		                </br></br></br></br></br>
		                  <h1>Android</h1>
									<p>L'application android est disponible !</p>
									<p>Elle vous permet un accès rapide et facile à L4H.</p>
										</br>
										<div class="">
										<a class="btn btn-lg btn-success"
											href="/download/L4H.apk" role="button">Télécharger</a>
										</div>
									
		                </div>
		              </div>
		            </blockquote>
				</div>
			</div>
			<a class="left carousel-control" href="#myCarousel" role="button"
				data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#myCarousel" role="button"
				data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
		</div>
		
		
	<?php }else{?>
		
		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
				<li data-target="#myCarousel" data-slide-to="3"></li>
				<li data-target="#myCarousel" data-slide-to="4"></li>
			</ol>
			
			
			<div class="carousel-inner">
			
			<div class="item active">
			<blockquote>
              <div class="row">
                <div class="col-sm-4 text-center">
                  <img class="img-circle" src="/images/site/l4h.png" style="width: 100%;height:100%;">
                </div>
                <div class="col-sm-1">
		        </div>
                <div class="col-sm-5">
                </br></br></br></br></br>
                  <h1>Besoin d'aide ?</h1>
							<p>Un service ? Une offre à proposer? Du temps libre?</br>
							 &nbsp; &nbsp; &nbsp;Nous sommes là pour vous!</p>
                </div>
              </div>
            </blockquote>
				</div>
				
				<div class="item">
					<blockquote>
		              <div class="row">
		                <div class="col-sm-4 text-center">
		                  <img class="img-circle" src="/images/site/l4h.png" style="width: 100%;height:100%;">
		                </div>
		                <div class="col-sm-1">
		                </div>
		                <div class="col-sm-5">
		                </br></br></br></br></br>
		                  <h1>Qui sommes-nous ?</h1>
									<p>Looking for help est un site web d'entraide sur lequel vous pouvez
					non seulement trouver de l'aide, mais également proposer la vôtre !
					</br></br>Nous proposons un service unique dans votre région: un service d'entraide,
					convivial et surtout gratuit !
					</p>	
					</br>
										<div class="">
										<a class="btn btn-lg btn-warning"
											href="/docs/about.php" role="button">Plus d'informations</a>
										</div>
		                </div>
		              </div>
		            </blockquote>
				</div>
	
				
				<div class="item">
					<blockquote>
		              <div class="row">
		                <div class="col-sm-4 text-center">
		                  <img class="img-circle" src="/images/site/l4h.png" style="width: 100%;height:100%;">
		                </div>
		                <div class="col-sm-1">
		                </div>
		                <div class="col-sm-5">
		                </br></br></br></br></br>
		                  <h1>Comment cela fonctionne-t-il?</h1>
									<p>En quelques cliques, inscrivez-vous et créez vos propres offres ou demandes selon vos besoins!</p>
									<p> 15 Jours de compte premium offerts à l'inscription </p>
										</br>
										<div class="">
										<a class="btn btn-lg btn-danger"
											href="/php/inscription/inscription.php" role="button">Inscription</a>
										</div>
									
		                </div>
		              </div>
		            </blockquote>
				</div>
				<div class="item">
					<blockquote>
		              <div class="row">
		                <div class="col-sm-4 text-center">
		                  <img class="img-circle" src="/images/site/android.png" >
		                </div>
		                <div class="col-sm-1">
		                </div>
		                <div class="col-sm-5">
		                </br></br></br></br></br>
		                  <h1>Android</h1>
									<p>L'application android est disponible !</p>
									<p>Elle vous permet un accès rapide et facile à L4H.</p>
										</br>
										<div class="">
										<a class="btn btn-lg btn-success"
											href="/download/L4H.apk" role="button">Télécharger</a>
										</div>
									
		                </div>
		              </div>
		            </blockquote>
				</div>
					<div class="item">
					<blockquote>
		              <div class="row">
		                <div class="col-sm-4 text-center">
		                  <img class="img" src="/images/site/asterisk.png">
		                </div>
		                <div class="col-sm-1">
		                </div>
		                <div class="col-sm-5">
		                </br></br></br></br></br>
		                  <h1>Asterisk</h1>
									<p>En vous inscrivant sur l4h.be, vous obtenez un compte SIP gratuit afin de pouvoir communiquer sans limite en VOIP avec d'autres utilisateurs !</p>
									
										</br>
										<div class="">
										<a class="btn btn-lg btn-danger"
											href="/php/inscription/inscription.php" role="button">Inscription</a>
										</div>
									
		                </div>
		              </div>
		            </blockquote>
				</div>
			</div>
			<a class="left carousel-control" href="#myCarousel" role="button"
				data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
			<a class="right carousel-control" href="#myCarousel" role="button"
				data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
		</div>
		
		
		
		
	<?php }?>
	<!-- /.carousel -->

	<hr>
	<div class="container">
	<?php 
	$mysqli = connect_bdd();
	$annonce = getAnnonceHasard($mysqli,'offre');
	$lien =	creeLienImageIndex($annonce['tagId']);
	?>
	<div class="row text-center">
		<?php if($annonce != 0){?>
		<div class="col-md-3 col-sm-6 hero-feature">
			<div class="thumbnail">
			Exemple d'offre
				<img <?php echo $lien; ?> alt="<?php echo 'image '.$annonce['tagNom'];?>">
				<div class="caption">
					<h3><?php echo '<small>'.$annonce['tagNom'].'</small>: '.$annonce['titre']?></h3>
					<p><?php echo $annonce['annonce']?></p>
					<p>
						
					</p>
				</div>
			</div>
		</div>
		<?php }
		
			$annonce = getAnnonceHasard($mysqli,'offre'); 
			$lien =	creeLienImageIndex($annonce['tagId']);
			if($annonce != 0){
		?>
		<div class="col-md-3 col-sm-6 hero-feature">
			<div class="thumbnail">
			Exemple d'offre
				<img <?php echo $lien; ?> alt="<?php echo 'image '.$annonce['tagNom'];?>">
				<div class="caption">
					<h3><?php echo '<small>'.$annonce['tagNom'].'</small>: '.$annonce['titre']?></h3>
					<p><?php echo $annonce['annonce']?></p>
					<p>
						
					</p>
				</div>
			</div>
		</div>
		<?php 
			}
			$annonce = getAnnonceHasard($mysqli,'demande'); 
			$lien =	creeLienImageIndex($annonce['tagId']);
			if($annonce != 0){
		?>
		<div class="col-md-3 col-sm-6 hero-feature">
			<div class="thumbnail">
			Exemple de demande
				<img <?php echo $lien; ?> alt="<?php echo 'image '.$annonce['tagNom'];?>">
				<div class="caption">
					<h3><?php echo '<small>'.$annonce['tagNom'].'</small>: '.$annonce['titre']?></h3>
					<p><?php echo $annonce['annonce']?></p>
					<p>
						
					</p>
				</div>
			</div>
		</div>

		<?php 
			}
				$annonce = getAnnonceHasard($mysqli,'demande'); 
				$lien =	creeLienImageIndex($annonce['tagId']);
				if($annonce != 0){
		?>
		<div class="col-md-3 col-sm-6 hero-feature">
			<div class="thumbnail">
			Exemple de demande
				<img <?php echo$lien;?> alt="<?php echo 'image '.$annonce['tagNom'];?>">
				<div class="caption">
					<h3><?php echo '<small>'.$annonce['tagNom'].'</small>: '.$annonce['titre']?></h3>
					<p><?php echo $annonce['annonce']?></p>
					<p>
						
					</p>
				</div>
			</div>
		</div>

	</div>


	
	<?php }
	$mysqli->close();?>
	</div>
	<hr>

	</div>
	<!-- FOOTER -->
     <?php include('php/footer.php'); ?>
  </body>
</html>
