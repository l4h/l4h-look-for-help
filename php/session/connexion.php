<?php

include ('../header.php');

if (connected ())
	redirect('/index.php');
?>
<body>
<div class="container" style="margin-top:100px">
		<div class="row">
			<div class="col-sm-6 col-md-4 col-md-offset-4">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong>Connexion</strong>
					</div>
					<div class="panel-body">
						<form role="form" action="#" method="POST">
							<fieldset>
								<div class="row">
									<div class="center-block">
										<img class="profile-img"
											src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120" alt="">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-10  col-md-offset-1 ">
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-user"></i>
												</span> 
												<input class="form-control" placeholder="E-mail" name="email" type="text" autofocus>
											</div>
										</div>
										<div class="form-group">
											<div class="input-group">
												<span class="input-group-addon">
													<i class="glyphicon glyphicon-lock"></i>
												</span>		
												
												<?php 
						$reac = false; // variable pour les cas de réactivation
						
						// on vérifie si il y a un GET et ensuite si la valeur commence par 'reac' pour les cas de réactivation
							if(!empty($_GET['activationCode'])){	
								 if(substr($_GET['activationCode'], 0, 4) == 'reac'){
								 	$reac = true; //on met une variable a true pour faciliter la tache dans les conditions plus loin
						?>
									<input class="form-control" placeholder="Mot de passe" name="password1" type="password">
									<input class="form-control" placeholder="Confirmation du mot de passe" name="password2" type="password" value="">
						<?php 	}
						//Sinon on affiche la page de connexion
								else{
						?>
									<input class="form-control" placeholder="Mot de passe" name="password" type="password">
						<?php 	}
							}
						//Sinon on affiche la page de connexion
							else{
						?>
								<input class="form-control" placeholder="Mot de passe" name="password" type="password">
								<?php }?>
												
											</div>
										</div>
										<div class="form-group">
											<input type="submit" class="btn btn-lg btn-primary btn-block" value="Connexion" name="Connexion">
										</div>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
					<div class="panel-footer ">
					<a href="password_reset.php"><button href="password_reset.php"  class="btn btn-default btn-xs info center-block">Mot de passe perdu ?</button></a>
					</div>
                </div>
			

						


<?php

$err_message = '';

$var_validité = true;

if (isset ( $_POST ['Connexion'] )) {
	if (empty ( $_POST ['email'] )) {
		
		$err_message .= ' Le champ email est vide.</br>';
		
		$var_validité = false;
	}
	
	if (empty ( $_POST ['password'] ) && !$reac) {
		
		$err_message .= ' Le champ mot de passe est vide.</br>';
		
		$var_validité = false;
	}
	// dans ce cas on va vérifier que les champs nouveau mot de passe et la confirmation sont remplis et les mêmes 
	else{
		if ($reac && (empty ( $_POST ['password1'] ) || empty ( $_POST ['password2'] ) || ($_POST ['password1'] != $_POST ['password2']))) {
			
			$err_message .= 'Un champ mot de passe n\'a pas été complété ou ils ne sont pas identiques.</br>';
			
			$var_validité = false;
		}		
	}
	
	if ($var_validité) {
		
		// Connexion a la bdd
		
		$mysqli = connect_bdd ();
		
		$email = $_POST ['email'];
		
		$user = mysqli_fetch_array ( select_all_from_users($mysqli,$email) );
		
		$id = $user ['id'];
		
		//exécution du code si ce n'est pas le cas d'une réactivation
		if(!$reac){
			if (decrypter_pwd_salt ( $_POST ['password'], $user )) {
				//recherche du profilId le plus récent du user (4 = reacti 5 = acti 6 = ban) 
				$profilId = mysqli_fetch_array (select_max_profilId($mysqli,$id) );
				// on regarde si l'utilisateur n'est pas banni
				if($profilId['profilId'] == 6){
					$err_message .= 'Ce compte a été bloqué.</br>';
					$err_message .= 'Contactez un administrateur pour de plus amples explications.</br>';
					
					$var_validité = false;
				}
				
				
				else if (empty ( $_GET ['activationCode'] )) {
					//recherche d'un potentiel code d'activation danss la table activation vi l'id du user
					$activation = mysqli_fetch_array ( select_activationCode($mysqli,$id) );
					
					if (empty ( $activation ['activationCode'] )) {
						
						$mysqli->close ();
						register_var_session ( $user );
					}
					// on prend la clé d'activation et on vérifie si elle comme pas par 'réac' du coup
					// mot de passe retrouvé, connexion autorisée et suppression de la clé
					 else if (substr($activation ['activationCode'], 0, 4) == 'reac'){
						reactivation_user($mysqli,$id,$activation ['activationCode']);
						
						$mysqli->close ();
						register_var_session ( $user );
						
					}else{
						$err_message .= 'Votre compte n\'a pas encore été activé. Merci de le faire via l\'e-mail reçu.</br>';
						
						$var_validité = false;
					}
				} else {
					$activationCode = $_GET ['activationCode'];
					$requete = '';
					
					if (substr($activationCode, 0, 4) == 'acti'){
						$typeActivation = 'acti%';
						$requete = select_all_activations($mysqli,$id,$activationCode,$typeActivation);
						if (mysqli_num_rows ( $requete ) == 1){
							// on active le compte en le passant au profilId user  == 3
							update_profilId($mysqli,$id);
							updateUserPremium($mysqli,15,$id);
						}
					}
					if (substr($activationCode, 0, 4) == 'reac'){
						$typeActivation = 'reac%';
						$requete = select_all_activations($mysqli,$id,$typeActivation);
						if (mysqli_num_rows ( $requete ) == 1){
							//supression du profilId = 4 pour le user une fois réactivé
							delete_profilId($mysqli,$id);
						}
					}
					if (mysqli_num_rows ( $requete ) == 1) {
						//suppression du code d'activation ou react.. dans la table
						delete_activations($mysqli,$id,$activationCode,$typeActivation);
						if($typeActivation == 'acti%'){
							//maj de la date d'activation
							maj_dateActivation($mysqli,$id);
						}
						
						$mysqli->close ();					
						register_var_session ( $user );
					} 
	
					else {
						
						$err_message .= 'Une erreur est survenue, contactez un administrateur.</br>';
						
						$var_validité = false;
					}
				}
			} else {
				
				$err_message .= 'L\'e-mail, le mot de passe fourni sont incorrects ou le compte est inexistant.</br>';
				
				$var_validité = false;
			}
		}
		/// ici on créé le nouveau mot de passe 
		else{
			$pwd = $_POST ['password1'];
			$activationCode = $_GET ['activationCode'];
			$typeActivation = 'reac%';
			// recherche d'un code de réactivation pour l'id donné et 
			$requete = select_all_activations($mysqli,$id,$activationCode,$typeActivation);
			if (mysqli_num_rows ( $requete ) == 1) {
				
				// création du salt
				$salt = generateRandomString ( 60 );
				// cryptage du pass avec le salt
				$pwd = crypt ( $pwd, $salt);
				
				$mysqli = connect_bdd ();
				//Maj du mot de pass et su salt 
				maj_pwd_salt($mysqli,$id,$pwd,$salt);
				//suppresion du code de réactivation
				delete_activations($mysqli,$id,$activationCode,$typeActivation);
				//suppression de l'état en reactivation
				delete_profilId($mysqli,$id);
				
				$mysqli->close ();
				register_var_session ( $user );
			}
			else{
				$err_message .= 'Ce lien de création de nouveau mot de passe est inconnu.</br>';
				
				$var_validité = false;
			}
		}
	}

	
	if (! $var_validité)
		echo '<div class="alert alert-dismissable alert-warning hidden-print">
				<a href="#" class="close" data-dismiss="alert">&times;</a>
					<h3><b>Erreur de connexion</b></h3>
							<p class="lead">' . $err_message . '</p>
						</center>
				</div>';
}

?>

      

			

			

			</div>

		</div>

	</div>


</body>

<?php include('../footer.php'); ?>

</html>
