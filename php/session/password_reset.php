<?php
include ('../header.php');
if (connected ())
	redirect ( '/index.php' );
?>

<head>
</head>
<body>
	<div class="container ">
		</br> </br> </br>
		<div class="row">
			</br>
			</br>
			</br>
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<?php
				$err_message = '';
				
				$var_validité = true;
				if (isset ( $_POST ['new_pwd'] )) {
					
					if (empty ( $_POST ['email'] )) {
						$err_message .= 'Le champ email est vide.</br>';
						$var_validité = false;
					}
					
					if (! verif_captcha ()) {
						$err_message .= 'Le texte à recopier est invalide.</br>';
						$var_validité = false;
					}
					
					if ($var_validité) {
						// Connexion a la bdd
						$mysqli = connect_bdd ();
						$email = $_POST ['email'];
						// / on vérifie si l'e-mail existe
						if (mysqli_num_rows ( $mysqli->query ( " Select * from users where email='$email'" ) ) != 1) {
							$err_message .= 'Cet e-mail ne correspond à aucun compte.</br>';
							$var_validité = false;
						}
					}
					if ($var_validité) {
						// Creation du nombre aléatoire pour activation email
						$activationCode = "reac" . generateRandomString ( 40 );
						
						$id = insert_bdd_activation ( $activationCode, $email ); // retourne l'id du user par la meme occasion
						
						$lienactivation = racine_serveur().'/php/session/connexion.php?activationCode=' . $activationCode;
						
						$titre = "Création d'un nouveau mot de passe sur Looking for help";
						$message = '<h1 style="color:RGB(111,153,170)">Looking for help </h1>';
						$message .= 'Bonjour, </br>';
						$message .= 'Vous avez demandé la création d\'un nouveau mot de passe.</br>';
						$message .= 'Cliquez <a href=" ' . $lienactivation . ' ">ici</a> afin de réactiver votre compte utilisateur. </br></br>';
						$message .= 'Si le lien ne fonctionne pas, recopier l\'adresse suivante dans votre navigateur web : </br>';
						$message .= '<a href="' . $lienactivation . '">' . $lienactivation . ' </a></br></br>';
						$message .= 'Si vous n\'êtes pas à l\'origine de cette modification, veuillez ignorer ce message </br></br></br></br>';
						$message .= 'Nous vous remercions votre confiance et vous souhaitons une agréable journée, </br></br>';
						$message .= 'L\'équipe L4H. </br></br></br>';
						$message .= '<table style="background-color:RGB(111,153,170)">';
						$message .= '<tr style="text-align: center">';
						$message .= '<td> Cet email à été envoyé à ' . $email . '</td>';
						$message .= '</tr>';
						$message .= '<tr style="text-align: center">';
						$message .= '<td> L4H - Looking for help, rue du ciseau 16 Louvain la neuve, Belgique </td>';
						$message .= '</tr>';
						$message .= '<tr style="text-align: center">';
						$message .= '<td> <a href="mailto:contact@l4h.be"> Contact </a> -- <a href="'.racine_serveur().'/docs/CGU.php"> Mentions légales </a> -- <a href="'.racine_serveur().'/docs/CGU.php"> Conditions d\'utilisation </a></td>';
						$message .= '</tr>';
						$message .= '</table>';
						
						if (envoi_email ( $titre, $message, $email )) {
					?>
							<div class="row">
								<div class="col-md-4"></div>
									<div class="col-md-6">
										<p class="lead text-center">Un e-mail vous a été envoyé.</p>
										
											<script type="text/javascript">
												// initialise le temps
												var cpt = 5;
												 
												timer = setInterval(function(){
												    if(cpt>0) // si on a pas encore atteint la fin
												    {
												        --cpt; // décrémente le compteur
												        var Crono = document.getElementById("Crono"); // récupère l'id
												        var old_contenu = Crono.firstChild; // stock l'ancien contenu
												        Crono.removeChild(old_contenu); // supprime le contenu
												        var texte = document.createTextNode(cpt); // crée le texte
												        Crono.appendChild(texte); // l'affiche
												    }
												    else // sinon brise la boucle
												    {
												    	document.location.replace('/index.php');
												    }
												}, 1000);
			 
											</script>

											<!-- le div ou on affiche le chrono, ne pas le mettre vide -->
											<p class="lead">
												Redirection dans <span id="Crono">5</span> secondes.
											</p>
									</div>
								</div>
						</div>	
									
					<?php 		
							$mysqli->query ( "insert into users_profil (userId,profilId) values ('$id',4)" );
							$mysqli->close ();
						} else
							echo '<div class="alert alert-dismissable alert-warning hidden-print">
									<a href="#" class="close" data-dismiss="alert">&times;</a>
										<p class="lead">Une erreur s\'est produite lors de l\'envoi de l\'email, veuillez contacter un administrateur.</p>
									</center>
								</div>';
					}
					
				}
				if (! isset ( $_POST ['new_pwd'] ) || ! $var_validité) {
					
					?>
				</div>
		</div>
	</div>
	<div class="container ">
		<div class="row">
			<div class="col-sm-5 col-md-6 col-md-offset-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<strong>Création d'un nouveau mot de passe</strong>
					</div>
					<div class="panel-body">
						<form role="form" action="#" method="POST">
							<fieldset>
								<div class="row">
									<div class="center-block">
										<img class="profile-img"
											src="https://lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=120"
											alt="">
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-10  col-md-offset-1 ">
										<div class="input-group">
											<span class="input-group-addon"> <i
												class="glyphicon glyphicon-user"></i>
											</span> <input class="form-control" placeholder="E-mail"
												name="email" type="email" autofocus>
										</div>
										<div class="input-group">

											<!--  Code recaptcha -->
										<?php
					affiche_captcha ();
					?>
										</div>

									</div>
									<div class="col-md-6 col-md-offset-3">
										</br>
										<div class="form-group">
											<input type="submit" class="btn btn-lg btn-primary btn-block"
												value="Valider" name="new_pwd">
										</div>
									</div>
								</div>
							</fieldset>
						</form>
					</div>
				<div class="row">
				
							<?php
				}
				
				if (! $var_validité)
					echo '<div class="alert alert-dismissable alert-warning hidden-print">
									<a href="#" class="close" data-dismiss="alert">&times;</a>
									<h3><b>Erreur</b></h3>
										<p class="lead">' . $err_message . '</p>
									</center>
								</div>';
				?>									
					
			</div>
			</div>
	</div>
	</div>
</body>
<?php include('../footer.php'); ?>
</html>
