<?php
session_start ();
if ($_SERVER ['HTTP_HOST'] == 'localhost') {
	error_reporting ( E_ALL );
	ini_set ( "display_errors", 1 );
}
include ("outils/fonctions.php");
include ("outils/fonctionsCaptcha.php");
include ("outils/fonctionsConnexion.php");
include ("outils/fonctionsImage.php");
include ("outils/fonctionsOffresDemandes.php");
include ("outils/modifierOffreDemande.php");
include ("outils/mesOffresDemandes.php");
include ("outils/fonctionsUtilisateurs.php");
include ("outils/recaptchalib.php");
include ("outils/requetes.php");
include ("outils/fonctionsOffresRelations.php");
include ("outils/fonctionsConversations.php");
include ("outils/fonctionReponseOffreDemande.php");
include ("outils/fonctionListeAttenteOffreDemande.php");
include ("outils/fonctionsNewsletter.php");
include ("outils/fonctionStatistiques.php");
include ("outils/messagesErreurs.php");
include ("outils/fonctionsAnnoncesCreation.php");
include ("outils/fonctionsAsterisk.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<!-- tri des tableaux admin -->
<script src="/jquery/jquery.min.js" type="text/javascript"></script>
<script src="/js/sorttable.js"></script>


<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- pour les mobiles normalement -->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />


	<link rel="stylesheet" type="text/css"
		href="/bootstrap/css/bootstrap.css" />
	<link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet"
		type="text/css">
		<link href="/bootstrap/css/caroussel.css" rel="stylesheet"
			type="text/css">
			<link href="/css/formulaire.css" rel="stylesheet" type="text/css">
				<link href="/css/cssDeBase.css" rel="stylesheet" type="text/css">
					<link href="/css/google_map.css" rel="stylesheet" type="text/css">
						<link href="/css/profil.css" rel="stylesheet" type="text/css">
							<link href="/css/messagerie.css" rel="stylesheet" type="text/css">
								<link rel="shortcut icon" href="/images/site/l4h.ico">
									<link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
									

									<script
										src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDrNzSlMI6dRzhj-EwUhG6cRC3nnKnkbQY&sensor=false">
	</script>
									<!--  google map -->
									<link rel="stylesheet"
										href="/jquery/jquery-ui-1.8.12.custom.css" type="text/css" />

									<!-- Gère le champ date -->
									<script
										src="http://cdn.jsdelivr.net/webshim/1.12.4/extras/modernizr-custom.js"></script>
									<script
										src="http://cdn.jsdelivr.net/webshim/1.12.4/polyfiller.js"></script>

									<!--  permet des bulles descriptives au sur-vol des bouttons -->
									<script src="/js/profil.js"></script>
									<script>
	webshims.setOptions('waitReady', false);
	webshims.setOptions('forms-ext', {types: 'date'});
	webshims.polyfill('forms forms-ext');
</script>


									<title>L4H - Looking for help</title>

</head>
<body>
	<?php $mysqli = connect_bdd ();
		$connected = connected();
		if($connected) {
			$nbNewOffreRelation =  nbNewAnnonceRelation($mysqli,$_SESSION['id'],'offre');
			$nbNewDemandeRelation =  nbNewAnnonceRelation($mysqli,$_SESSION['id'],'demande');
			$nbNewNotifRelationOffre = nbNewNotifRelation($mysqli,$_SESSION['id'],'offre');
			$nbNewNotifRelationDemande = nbNewNotifRelation($mysqli,$_SESSION['id'],'demande');
			
			$nbNotifOffre = $nbNewOffreRelation + $nbNewNotifRelationOffre;
			if($nbNotifOffre == 0 )  $nbNotifOffre = "";
			$nbNotifDemande = $nbNewDemandeRelation + $nbNewNotifRelationDemande;
			if($nbNotifDemande == 0 )  $nbNotifDemande = "";
		}
	?>
	<div class="navbar-wrapper">
		<div class="container">

			<div
				class="navbar navbar-inverse navbar-custom navbar-static-top borderdecontainer"
				role="navigation">
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span> <span
								class="icon-bar"></span> <span class="icon-bar"></span> <span
								class="icon-bar"></span> <span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/index.php">Looking for help - L4H</a>
					</div>
					<div class="collapse navbar-collapse">
						<ul class="nav navbar-nav">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"> Offres <span class="badge"><?php if($connected) echo $nbNotifOffre; ?></span> <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/php/offres/offreCreation.php">Créer</a></li>
									<li><a href="/php/offres/listeOffres.php">Afficher</a></li>
<?php if ($connected){?>			<li class="divider"></li>
									<li><a href="/php/offres/mesOffres.php"><span class="badge pull-right"><?php echo $nbNewOffreRelation; ?></span>Mes offres </a></li>
									<li><a href="/php/offres/reponseoffre.php"><span class="badge pull-right"><?php echo $nbNewNotifRelationOffre; ?></span>Mes relations </a></li>
<?php
}
?>
								</ul></li>
						</ul>

						<ul class="nav navbar-nav">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown">Demandes <span class="badge"><?php if ($connected) echo $nbNotifDemande; ?></span> <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/php/demandes/demande.php">Créer</a></li>
									<li><a href="/php/demandes/listeDemandes.php">Afficher</a></li>
<?php if ($connected){?>			<li class="divider"></li>
									<li><a href="/php/demandes/mesDemandes.php"><span class="badge pull-right"><?php echo $nbNewDemandeRelation; ?></span>Mes Demandes  </a></li>
									<li><a href="/php/demandes/reponsedemande.php"><span class="badge pull-right"><?php echo $nbNewNotifRelationDemande; ?></span>Mes relations </a></li>
<?php }?>
								</ul></li>
						</ul>
						<?php
						if ($connected) {
							if (! userEstPremium ( $mysqli, $_SESSION ['id'] ))
								
								echo '<ul class="nav navbar-nav"><li>
									<a style="font-size:20px;" href="/php/premium/infoPremium.php">
									<img src="/images/site/info.png" WIDTH=22 HEIGHT=22>
									Premium </a></li></ul>';
						}
						
						?>
						<ul class="nav navbar-nav navbar-right">
              <?php if (!$connected){?>
              	<li
								<?php if($_SERVER['REQUEST_URI']=='php/inscription/inscription.php') {echo'class="active"';}  ?>><a
								href="/php/inscription/inscription.php">Inscription</a></li>
							<li
								<?php if($_SERVER['REQUEST_URI']=='/php/session/connexion.php') {echo'class="active"';}  ?>><a
								href="/php/session/connexion.php"><span class="glyphicon glyphicon-log-in"></span> Connexion</a></li>
                <?php
														}
														if ($connected) :
															if (isDesactive ( $_SESSION ['id'] ))
																if ($_SERVER ['REQUEST_URI'] != '/php/profil/profil.php' && $_SERVER ['REQUEST_URI'] != '/php/profil/profil.php?activer=true' && $_SERVER ['REQUEST_URI'] != '/php/profil/modifierProfil.php?desactive' && $_SERVER ['REQUEST_URI'] != '/php/session/logout.php')
																	redirect ( '/php/profil/profil.php' );
															?>
                 <?php									if (nouveauxMessages ($mysqli, $_SESSION ['id'] )>0) {
																echo "<li><a data-toggle='tooltip' data-placement='bottom' title='Nouveau(x) message(s)' href='/php/conversations/messagerie.php'>
			<span class='glyphicon glyphicon-envelope clignotant' style='color:yellow'></span></a></li>";
															}
															if (admin ()) {
																?>
									<li><a href="/php/administration/administration.php">Administration</a></li>
									<?php }?>
                <li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown"><?php echo $_SESSION['prenom'].' '.$_SESSION['nom'];?><span
									class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="/php/profil/profil.php"><span class="glyphicon glyphicon-user"></span> Profil</a></li>
									<li><a href="/php/conversations/messagerie.php"><span class="glyphicon glyphicon-envelope"></span> Messagerie</a></li>
									<?php 
										if(userEstPremium ( $mysqli, $_SESSION ['id'] )){
									?>
										<li><a href="/php/statistiques/stats.php"><span class="glyphicon glyphicon-stats"></span> Statistiques</a></li>
									<?php 
										}
									?>
									<!--  <li><a href="#">Something else here</a></li>
									<li class="divider"></li>-->
									<li class="divider"></li>
									<li><a href="/php/session/logout.php"><span class="glyphicon glyphicon-log-out"></span> Déconnexion</a></li>

								</ul></li>
                
                <?php endif; ?>

              </ul>
					</div>
				</div>
			</div>

		</div>
	</div>
	<?php $mysqli->close();?>
</body>
</html>
