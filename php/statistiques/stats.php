<?php
include ("../../jpgraph/src/jpgraph.php");
include ('../../jpgraph/src/jpgraph_pie.php');
include ('../header.php');

// redirections
// connecté
if (! connected ())
	redirect ( '../index.php' );

// premium
$mysqli = connect_bdd();
if(!(userEstPremium ( $mysqli, $_SESSION ['id'] )))
	redirect('../../index.php');



// variables
$id = $_SESSION['id'];

// suppression des images de graphes anciennes	
	// on lit tout les ID dans la BDD
	$result = get_all_id($mysqli);
	$today = date('Y-m-d H:i:s');
	// on supprime chaque image selon ID si elle existe
	while ($row = $result->fetch_array()) {
		$usrId = $row['id'];
		
		// graphe dem site complet
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieDemSite'.$usrId.'.png';
		if(file_exists($lienGraphe)){
			$datefich1 = filemtime ($lienGraphe);
			if ($datefich1 != $today) {
				unlink($lienGraphe);
			}
		}
		
		// graphe off site complet
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieOffSite'.$usrId.'.png';
		if(file_exists($lienGraphe)){
			$datefich2 = filemtime ($lienGraphe);
			if ($datefich2 != $today) {
				unlink($lienGraphe);
			}
		}
		
		// graphe dem user
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieDemUsr'.$usrId.'.png';
		if(file_exists($lienGraphe)){
			$datefich3 = filemtime ($lienGraphe);
			if ($datefich3 != $today) {
				unlink($lienGraphe);
			}
		}
		
		// graphe off user
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieOffUsr'.$usrId.'.png';
		if(file_exists($lienGraphe)){
			$datefich4 = filemtime ($lienGraphe);
			if ($datefich4 != $today) {
				unlink($lienGraphe);
			}
		}
		
		// graphe dem CP
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieDemCP'.$usrId.'.png';
		if(file_exists($lienGraphe)){
			$datefich5 = filemtime ($lienGraphe);
			if ($datefich5 != $today) {
				unlink($lienGraphe);
			}
		}
		
		// graphe off CP
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieOffCP'.$usrId.'.png';
		if(file_exists($lienGraphe)){
			$datefich6 = filemtime ($lienGraphe);
			if ($datefich6 != $today) {
				unlink($lienGraphe);
			}
		}
		
		//graphe dem Newsl
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieDemNewsl'.$usrId.'.png';
		if(file_exists($lienGraphe)){
			$datefich7 = filemtime($lienGraphe);
			if($datefich7 != $today){
				unlink($lienGraphe);
			}
		}
		
		//graphe off Newsl
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieOffNewsl'.$usrId.'.png';
		if(file_exists($lienGraphe)){
			$datefich7 = filemtime($lienGraphe);
			if($datefich7 != $today){
				unlink($lienGraphe);
			}
		}
	
	}
	
?>
</br></br></br></br></br>


<div class="container">

<!-- ***** ACTIVITE SUR TOUT LE SITE ***** -->

	<h2>Activités sur tout le site par catégorie :</h2>
	<div class="col-xs-6">
	<h3>Demandes</h3>
<?php
	// on calcule le nombre total de demandes
	$result = get_count_total_site($mysqli,"demande");
	$data = $result->fetch_array();
	$total = $data ['nbTagId'];
	
	//si rien à afficher
	if($total !=0){
	
		// on calcule le nombre de demandes pour chaque TAG
		$result = get_count_tag_site ($mysqli,"demande");
	
		// Tableaux de données destinées à JpGraph
		$tableauTagNom = array();
		$tableauNbTagId = array();
	
		// Fetch sur chaque enregistrement
		while ($row = $result->fetch_array()) {
			// Alimentation des tableaux de données
			$tableauTagNom[] = $row['tagNom'];
			$tableauNbTagId[] = calcul_pourcentage($row['nbTagId'], $total);
		}
		
		// On spécifie la largeur et la hauteur du graphique conteneur 
		$graph = new PieGraph(500,400);
		
		// Titre du graphique
		$graph->title->Set("Demandes sur le site (%)");
		
		// Créer un graphique secteur (classe PiePlot)
		$oPie = new PiePlot($tableauNbTagId);
		
		// Légendes qui accompagnent chaque secteur
		$oPie->SetLegends($tableauTagNom);
		
		// position du graphique (légèrement à droite)
		$oPie->SetCenter(0.4); 
		$oPie->SetValueType(PIE_VALUE_ABS);
		
		// Format des valeurs de type entier
		$oPie->value->SetFormat('%d');
		
		// Ajouter au graphique le graphique secteur
		$graph->Add($oPie);
		
		// Provoquer l'affichage
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieDemSite'.$id.'.png';
		if(file_exists($lienGraphe)) unlink($lienGraphe);
		$graph->Stroke($lienGraphe);
		echo "<img src='$lienGraphe' />";
	}
	else{
?>
	<div class="alert alert-info">
        <strong>Info:</strong> Aucune demande à afficher.
    </div>
<?php 
	}
?>
	</div>
	<div class="col-xs-6">
	<h3>Offres</h3>
<?php
	// on calcule le nombre total d'offres
	$result = get_count_total_site($mysqli,"offre");
	$data = $result->fetch_array();
	$total = $data ['nbTagId'];
	
	//si rien à afficher
	if($total !=0){
		
		// on calcule le nombre d'offres pour chaque TAG
		$result = get_count_tag_site ($mysqli,"offre");

		// Tableaux de données destinées à JpGraph
		$tableauTagNom = array();
		$tableauNbTagId = array();
		// Fetch sur chaque enregistrement
		while ($row = $result->fetch_array()) {
			// Alimentation des tableaux de données
			$tableauTagNom[] = $row['tagNom'];
			$tableauNbTagId[] = calcul_pourcentage($row['nbTagId'], $total);
		}
		// On spécifie la largeur et la hauteur du graphique conteneur 
		$graph = new PieGraph(500,400);
		
		// Titre du graphique
		$graph->title->Set("Offres sur le site (%)");
		
		// Créer un graphique secteur (classe PiePlot)
		$oPie = new PiePlot($tableauNbTagId);
		
		// Légendes qui accompagnent chaque secteur
		$oPie->SetLegends($tableauTagNom);
		
		// position du graphique (légèrement à droite)
		$oPie->SetCenter(0.4); 
		$oPie->SetValueType(PIE_VALUE_ABS);
		
		// Format des valeurs de type entier
		$oPie->value->SetFormat('%d');
		
		// Ajouter au graphique le graphique secteur
		$graph->Add($oPie);
		
		// Provoquer l'affichage
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieOffSite'.$id.'.png';
		if(file_exists($lienGraphe)) unlink($lienGraphe);
		$graph->Stroke($lienGraphe);
		echo "<img src='$lienGraphe' />";
	}
	else{
?>		
		<div class="alert alert-info">
        	<strong>Info:</strong> Aucune offre à afficher.
    	</div>
<?php 
	}
?>
	</div>
	<!-- ***** ACTIVITE PAR CP ***** -->
	<br/><br/>
	<h2>Activités par code postal et par catégorie </h2>

	<form action="stats.php#cp" method="post">
		<div class="control-group">
			<label class="control-label" for="CPMin">Code postal minimum</label>
		  	<div class="controls">
		    	<input id="CPMin" name="CPMin" type="text" placeholder="code postal minimum" class="input-xxlarge"
		    		value="<?php if(isset($_POST['CPMin'])) { echo $_POST['CPMin'];}?>">  
		  	</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="CPMax">Code postal maximum</label>
		  	<div class="controls">
		    	<input id="CPMax" name="CPMax" type="text" placeholder="code postal maximum" class="input-xxlarge"
		    		value="<?php if(isset($_POST['CPMax'])) { echo $_POST['CPMax'];}?>">
		  	</div>
		</div>
		
		<div class="control-group">
			<label class="control-label" for="rechercher"></label>
		  	<div class="controls">
		    	<input type="submit" class="btn btn-primary" onclick="#" name="rechercher" value="Rechercher">
		  	</div>
		</div>
		<br/>
		<a href="http://fr.wikipedia.org/wiki/Liste_des_codes_postaux_belges" target=blank>Besoin d'aide? Liste des codes postaux belges</a>
		<br/>
</form>
	<div class="col-xs-6">
	<?php 
	// GERER SI L ENTREE EST BIEN UN NOMBRE ET PAS LETTRE + LECTURE BDD + SECURITE CHAMPS
	$errormsg = " ";
	$validite = true;
	if(isset($_POST['rechercher'])){
	
		if(empty($_POST['CPMin'])){
			$errormsg .= "Veuillez remplir le champ code postal minimum </br>";
			$validite = false;
		}
		elseif(($_POST['CPMin']>9999)or($_POST['CPMin']<1000)or($_POST['CPMax']>9999) or ($_POST['CPMax']<1000)){
			if(($_POST['CPMin']>9999)or($_POST['CPMin']<1000)){
				$errormsg .="Veuillez entrer un code postal minimum compris entre 1000 et 9999 </br>";
				$validite = false;
			}
			if(($_POST['CPMax']>9999) or ($_POST['CPMax']<1000)){
				$errormsg .="Veuillez entrer un code postal maximum compris entre 1000 et 9999 </br>";
				$validite = false;
			}
		}
		else{
			$cpMin = fixtags($_POST['CPMin']);
		}
		if(empty($_POST['CPMax'])){
			$errormsg .= "Veuillez remplir le champ code postal maximum </br>";
			$validite = false;
		}
		else{
			$cpMax = fixtags($_POST['CPMax']);
		}
		echo'</div><div class="col-xs-6"></div>';
		if($validite == true){
		
			// DEMANDES
			echo'<div class="col-xs-6">';
			echo "<h3 id=\"cp\">Demandes pour les codes postaux de " . $cpMin . " à " . $cpMax . "</h3>";
			
			// on calcule le nombre total de demandes pour les CP donnés
			$total = get_count_total_CP($mysqli,$cpMin, $cpMax, "demande");
			
			//si rien à afficher
			if($total !=0){
			
				// on calcule le nombre de demandes pour chaque TAG
				$result = get_count_tag_CP($mysqli,$cpMin, $cpMax, "demande");
			
				// Tableaux de données destinées à JpGraph
				$tableauTagNom = array();
				$tableauNbTagId = array();
			
				// Fetch sur chaque enregistrement
				while ($row = $result->fetch_array()) {
					// Alimentation des tableaux de données
					$tableauTagNom[] = $row['tagNom'];
					$tableauNbTagId[] = calcul_pourcentage($row['nbTag'], $total);
				}
			
				// On spécifie la largeur et la hauteur du graphique conteneur
				$graph = new PieGraph(500,400);
			
				// Titre du graphique
				$graph->title->Set("Demandes (%)");
			
				// Créer un graphique secteur (classe PiePlot)
				$oPie = new PiePlot($tableauNbTagId);
			
				// Légendes qui accompagnent chaque secteur
				$oPie->SetLegends($tableauTagNom);
			
				// position du graphique (légèrement à droite)
				$oPie->SetCenter(0.4);
				$oPie->SetValueType(PIE_VALUE_ABS);
			
				// Format des valeurs de type entier
				$oPie->value->SetFormat('%d');
			
				// Ajouter au graphique le graphique secteur
				$graph->Add($oPie);
			
				// Provoquer l'affichage
				$lienGraphe = "../../images/statistiques/";
				$lienGraphe .= 'graphPieDemCP'.$id.'.png';
				if(file_exists($lienGraphe)) unlink($lienGraphe);
				$graph->Stroke($lienGraphe);
				echo "<img src='$lienGraphe' />";
			}
			else{
?>
				<div class="alert alert-info">
        			<strong>Info:</strong> Aucune demande à afficher.
    			</div>
<?php 
			}
			echo'</div><div class="col-xs-6">';
			// OFFRES
			echo "<h3>Offres pour les codes postaux de " . $cpMin . " à " . $cpMax . "</h3>";
				
			// on calcule le nombre total d'offres pour les CP donnés
			$total = get_count_total_CP($mysqli,$cpMin, $cpMax, "offre");
				
			//si rien à afficher
			if($total !=0){
					
				// on calcule le nombre d'offres pour chaque TAG
				$result = get_count_tag_CP($mysqli,$cpMin, $cpMax, "offre");
					
				// Tableaux de données destinées à JpGraph
				$tableauTagNom = array();
				$tableauNbTagId = array();
					
				// Fetch sur chaque enregistrement
				while ($row = $result->fetch_array()) {
					// Alimentation des tableaux de données
					$tableauTagNom[] = $row['tagNom'];
					$tableauNbTagId[] = calcul_pourcentage($row['nbTag'], $total);
				}
					
				// On spécifie la largeur et la hauteur du graphique conteneur
				$graph = new PieGraph(400,300);
					
				// Titre du graphique
				$graph->title->Set("Offres (%)");
					
				// Créer un graphique secteur (classe PiePlot)
				$oPie = new PiePlot($tableauNbTagId);
					
				// Légendes qui accompagnent chaque secteur
				$oPie->SetLegends($tableauTagNom);
					
				// position du graphique (légèrement à droite)
				$oPie->SetCenter(0.4);
				$oPie->SetValueType(PIE_VALUE_ABS);
					
				// Format des valeurs de type entier
				$oPie->value->SetFormat('%d');
					
				// Ajouter au graphique le graphique secteur
				$graph->Add($oPie);
					
				// Provoquer l'affichage
				$lienGraphe = "../../images/statistiques/";
				$lienGraphe .= 'graphPieOffCP'.$id.'.png';
				if(file_exists($lienGraphe)) unlink($lienGraphe);
				$graph->Stroke($lienGraphe);
				echo "<img src='$lienGraphe' />";
			}
			else{
?>
				<div class="alert alert-info">
        			<strong>Info:</strong> Aucune offre à afficher.
    			</div>
<?php 
			}
		}
	}
	echo'</div>';
	if($validite == false){
?>

		<div class=" alert alert-dismissable alert-warning">
			<div class="row">
				<div class="col-md-2">
					<img src="/images/site/warning.png" alt="attention infos correctes"/>
				</div>
				<big><?php echo $errormsg;?></big>
			</div>
		</div>
<?php 
	}
?>
	
	<!-- ***** ACTIVITE PERSONNELLE ***** -->
	<br/><br/>
	<h2>Mon activité personnelle </h2>
	<div class="col-xs-6">
	<h3>Demandes</h3>
<?php
	// on calcule le nombre total de demandes pour un user
	$result = get_count_total_user($mysqli,"demande", $id);
	$total = $result->fetch_array();
	$total = $total['nbAnn'];

	//si rien à afficher
	if($total !=0){
		// on calcule le nombre de demandes pour chaque TAG
		$result = get_count_tag_user($mysqli,"demande", $id);
		
		// Tableaux de données destinées à JpGraph
		$tableauTagNom = array();
		$tableauNbTagId = array();
		// Fetch sur chaque enregistrement
		while ($row = $result->fetch_array()) {
			// Alimentation des tableaux de données
			$tableauTagNom[] = $row['tagNom'];
			$tableauNbTagId[] = calcul_pourcentage($row['nbTag'], $total);
		}
		
		// On spécifie la largeur et la hauteur du graphique conteneur
		$graph = new PieGraph(500,400);
		
		// Titre du graphique
		$graph->title->Set("Demandes personnelles (%)");
		
		// Créer un graphique secteur (classe PiePlot)
		$oPie = new PiePlot($tableauNbTagId);
		
		// Légendes qui accompagnent chaque secteur
		$oPie->SetLegends($tableauTagNom);
		
		// position du graphique (légèrement à droite)
		$oPie->SetCenter(0.4);
		$oPie->SetValueType(PIE_VALUE_ABS);
		
		// Format des valeurs de type entier
		$oPie->value->SetFormat('%d');
		
		// Ajouter au graphique le graphique secteur
		$graph->Add($oPie);
		
		// Provoquer l'affichage
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieDemUsr'.$id.'.png';
		if(file_exists($lienGraphe)) unlink($lienGraphe);
		$graph->Stroke($lienGraphe);
		echo "<img src='$lienGraphe' />";
	}
	else{
?>
		<div class="alert alert-info">
        	<strong>Info:</strong> Aucune demande à afficher.
    	</div>
<?php 
	}
?>
	</div>
	<div class="col-xs-6">
	<h3>Offres</h3>
<?php
	// on calcule le nombre total de offres pour un user
	$result = get_count_total_user($mysqli,"offre", $id);
	$total = $result->fetch_array();
	$total = $total['nbAnn'];
	
	
	//si rien à afficher
	if($total !=0){
		// on calcule le nombre d'offres pour chaque TAG
		$result = get_count_tag_user($mysqli,"offre", $id);
		
		// Tableaux de données destinées à JpGraph
		$tableauTagNom = array();
		$tableauNbTagId = array();
		
		// Fetch sur chaque enregistrement
		while ($row = $result->fetch_array()) {
			// Alimentation des tableaux de données
			$tableauTagNom[] = $row['tagNom'];
			$tableauNbTagId[] = calcul_pourcentage($row['nbTag'], $total);
		}
		
		// On spécifie la largeur et la hauteur du graphique conteneur
		$graph = new PieGraph(500,400);
		
		// Titre du graphique
		$graph->title->Set("Offres personnelles (%)");
		
		// Créer un graphique secteur (classe PiePlot)
		$oPie = new PiePlot($tableauNbTagId);
		
		// Légendes qui accompagnent chaque secteur
		$oPie->SetLegends($tableauTagNom);
		
		// position du graphique (légèrement à droite)
		$oPie->SetCenter(0.4);
		$oPie->SetValueType(PIE_VALUE_ABS);
		
		// Format des valeurs de type entier
		$oPie->value->SetFormat('%d');
		
		// Ajouter au graphique le graphique secteur
		$graph->Add($oPie);
		
		// Provoquer l'affichage
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieOffUsr'.$id.'.png';
		if(file_exists($lienGraphe)) unlink($lienGraphe);
		$graph->Stroke($lienGraphe);
		echo "<img src='$lienGraphe' />";
	}
	else{
?>
		<div class="alert alert-info">
        	<strong>Info:</strong> Aucune offre à afficher.
    	</div>
<?php 
	}
?>


<!-- ***** PREFERENCES NEWSLETTERS ***** -->
	</div>
	<h2>Préférences pour la newsletter :</h2>
	<div class="col-xs-6">
	<h3>Demandes</h3>
<?php
	// on calcule le nombre total de demandes
	$result = get_count_total_newsletter($mysqli,"demande");
	$data = $result->fetch_array();
	$total = $data ['nbTag'];
	
	//si rien à afficher
	if($total !=0){
	
		// on calcule le nombre de demandes pour chaque TAG
		$result = get_pref_newsl($mysqli,"demande");
	
		// Tableaux de données destinées à JpGraph
		$tableauTagNom = array();
		$tableauNbTagId = array();
	
		// Fetch sur chaque enregistrement
		while ($row = $result->fetch_array()) {
			// Alimentation des tableaux de données
			$tableauTagNom[] = $row['tagNom'];
			$tableauNbTagId[] = calcul_pourcentage($row['nbTagId'], $total);
		}
		
		// On spécifie la largeur et la hauteur du graphique conteneur 
		$graph = new PieGraph(500,400);
		
		// Titre du graphique
		$graph->title->Set("Demandes choisies pour la newsletter (%)");
		
		// Créer un graphique secteur (classe PiePlot)
		$oPie = new PiePlot($tableauNbTagId);
		
		// Légendes qui accompagnent chaque secteur
		$oPie->SetLegends($tableauTagNom);
		
		// position du graphique (légèrement à droite)
		$oPie->SetCenter(0.4); 
		$oPie->SetValueType(PIE_VALUE_ABS);
		
		// Format des valeurs de type entier
		$oPie->value->SetFormat('%d');
		
		// Ajouter au graphique le graphique secteur
		$graph->Add($oPie);
		
		// Provoquer l'affichage
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieDemNewsl'.$id.'.png';
		if(file_exists($lienGraphe)) unlink($lienGraphe);
		$graph->Stroke($lienGraphe);
		echo "<img src='$lienGraphe' />";
	}
	else{
?>
	<div class="alert alert-info">
        <strong>Info:</strong> Aucune demande à afficher.
    </div>
<?php 
	}
?>
	</div>
	<div class="col-xs-6">
	<h3>Offres</h3>
<?php
	// on calcule le nombre total d'offres
	$result = get_count_total_newsletter($mysqli,"offre");
	$data = $result->fetch_array();
	$total = $data ['nbTag'];
	
	//si rien à afficher
	if($total !=0){
	
		// on calcule le nombre d'offres pour chaque TAG
		$result = get_pref_newsl($mysqli,"offre");
	
		// Tableaux de données destinées à JpGraph
		$tableauTagNom = array();
		$tableauNbTagId = array();
	
		// Fetch sur chaque enregistrement
		while ($row = $result->fetch_array()) {
			// Alimentation des tableaux de données
			$tableauTagNom[] = $row['tagNom'];
			$tableauNbTagId[] = calcul_pourcentage($row['nbTagId'], $total);
		}
		
		// On spécifie la largeur et la hauteur du graphique conteneur 
		$graph = new PieGraph(500,400);
		
		// Titre du graphique
		$graph->title->Set("Offres choisies pour la newsletter (%)");
		
		// Créer un graphique secteur (classe PiePlot)
		$oPie = new PiePlot($tableauNbTagId);
		
		// Légendes qui accompagnent chaque secteur
		$oPie->SetLegends($tableauTagNom);
		
		// position du graphique (légèrement à droite)
		$oPie->SetCenter(0.4); 
		$oPie->SetValueType(PIE_VALUE_ABS);
		
		// Format des valeurs de type entier
		$oPie->value->SetFormat('%d');
		
		// Ajouter au graphique le graphique secteur
		$graph->Add($oPie);
		
		// Provoquer l'affichage
		$lienGraphe = "../../images/statistiques/";
		$lienGraphe .= 'graphPieOffNewsl'.$id.'.png';
		if(file_exists($lienGraphe)) unlink($lienGraphe);
		$graph->Stroke($lienGraphe);
		echo "<img src='$lienGraphe' />";
	}
	else{
?>		
		<div class="alert alert-info">
        	<strong>Info:</strong> Aucune offre à afficher.
    	</div>
<?php 
	}
?>

<!-- ***** MEILLEURS UTILISATEURS ***** -->
	</div>
	<br/><br/>
	<h2>Meilleurs utilisateurs</h2>
	<div class="col-xs-6">
	<h3>Demandes</h3>
<?php 
	
	$tableauNom = array();
	$tableauPrenom = array();
	$tableauTotDem = array();
	
	$result = get_best_users($mysqli,"demande");
	
	while ($row = $result->fetch_array()) {
		$tableauNom[] = $row['nom'];
		$tableauPrenom[] = $row['prenom'];
		$tableauTotDem[] = $row['totAnnonces'];
	}
?>
	<TABLE BORDER="1" width="400"> 
  		<TR> 
  			<TH> Place </TH>
 			<TH> Nom </TH> 
 			<TH> Prénom </TH> 
			<TH> Nombres de demandes </TH> 
  		</TR> 
  		<?php 
  		if(sizeof($tableauNom) > 5) $taille = 5;
  		else $taille = sizeof($tableauNom);
  		for($i = 0; $i < $taille; $i++){
 			echo'<TR>'; 
 			echo '<TD>'.($i+1).'</TD>'; 
			echo'<TD>'.$tableauNom[$i].'</TD>'; 
			echo'<TD>'.$tableauPrenom[$i].'</TD>'; 
			echo'<TD>'.$tableauTotDem[$i].'</TD>'; 
  			echo'</TR>'; 
  		}

  		?>
	</TABLE> 
	</div>
	<div class="col-xs-6">
	<h3>Offres</h3>
<?php 
	$tableauNom = array();
	$tableauPrenom = array();
	$tableauTotOff = array();
	
	$result = get_best_users($mysqli,"offre");
	while ($row = $result->fetch_array()) {
		$tableauNom[] = $row['nom'];
		$tableauPrenom[] = $row['prenom'];
		$tableauTotOff[] = $row['totAnnonces'];
	}
?>
	<TABLE BORDER="1" width="400">
	<TR>
		<TH> Place </TH>
		<TH> Nom </TH>
		<TH> Prénom </TH>
		<TH> Nombres d'offres </TH>
  	</TR>
  		<?php
  		if(sizeof($tableauNom) > 5) $taille = 5;
  		else $taille = sizeof($tableauNom);
  		for($i = 0; $i < $taille; $i++){
 			echo'<TR>';
 			echo '<TD>'.($i+1).'</TD>';
			 echo'<TD>'.$tableauNom[$i].'</TD>';
			 echo'<TD>'.$tableauPrenom[$i].'</TD>';
			 echo'<TD>'.$tableauTotOff[$i].'</TD>';
  			echo'</TR>';
		}
	?>
	</TABLE> 
	</div>
	</br></br>
	<!-- ***** POINTS ***** -->
	<h2>Mes points</h2>
	
<?php 
	// actuellement
	$tableauPts = array();
	$result = get_total_points($mysqli,$id);
	while ($row = $result->fetch_array()) {
		$tableauPts[] = $row['totPts'];
	}
	
	// 30 derniers jours
	$dateMin = date('Y-m-d H:i:s', strtotime("-30 days"));
	$dateMax = date('Y-m-d H:i:s');
	$result = get_point_date($mysqli,$id, $dateMin, $dateMax);
	while ($row = $result->fetch_array()) {
		$tableauPts[] = $row['totPts'];
	}
	
	// 60 derniers jours
	$dateMin = date('Y-m-d H:i:s', strtotime("-60 days"));
	$dateMax = date('Y-m-d H:i:s');
	$result = get_point_date($mysqli,$id, $dateMin, $dateMax);
	while ($row = $result->fetch_array()) {
		$tableauPts[] = $row['totPts'];
	}
	
	// 90 derniers jours
	$dateMin = date('Y-m-d H:i:s', strtotime("-90 days"));
	$dateMax = date('Y-m-d H:i:s');
	$result = get_point_date($mysqli,$id, $dateMin, $dateMax);
	while ($row = $result->fetch_array()) {
		$tableauPts[] = $row['totPts'];
	}
	
	if($tableauPts[0] != NULL){
?>
	
	<TABLE BORDER="1" width="400">
		<TR>
			<TH> Dates </TH>
			<TH> Points </TH>
  		</TR>
		<TR>
		 	<TD>Actuellement</TD>
		 	<TD><?php echo $tableauPts[0];?></TD>
  		</TR>
  		<TR>
		 	<TD>au cours des 30 derniers jours</TD>
		 	<TD><?php echo $tableauPts[1];?></TD>
  		</TR>
  		 <TR>
		 	<TD>au cours des 60 derniers jours</TD>
		 	<TD><?php echo $tableauPts[2];?></TD>
  		</TR>
  		<TR>
		 	<TD>au cours des 90 derniers jours</TD>
		 	<TD><?php echo $tableauPts[3];?></TD>
  		</TR>
  		
	</TABLE> 
<?php 
	}
	else{
?>
		<div class="alert alert-info">
        	<strong>Info:</strong> Vous n'avez pas encore de points.
    	</div>
<?php 
	}
	
?>
</div>
<?php
$mysqli->close();
include ('../footer.php');
?>