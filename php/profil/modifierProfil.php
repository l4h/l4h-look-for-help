<?php 
include('../header.php');
if(!connected()) redirect('../index.php');

?>
<html>
<head>
</head>
<body>
	<div class="container">
		</br>
		</br>
		</br>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6"></br></br></br></br>
			<?php 
			$url = racine_serveur().$_SERVER['REQUEST_URI']; 
			//$var enregistre ce qu'il y a apres le ? dans l'url, en l'occurence nom,phone,email,adresse
			$var =  parse_url($url, PHP_URL_QUERY);
			////Partie de la modification pour le nom et prénom
			if($var == 'nom'){?>
				<FORM METHOD="POST" ACTION="#">
					<fieldset>
					<label for="nom">Nom</label><input type="text" name="nom"
							id="nom" tabindex=1 autocorrect="off" spellcheck="false"
							autocapitalize="off" autofocus onblur="verifPseudo(this)" value="<?php echo stripslashes($_SESSION['nom']);?>"/></br>
							
							
						<label for="prenom">Prénom</label><input type="text" name="prenom"
							id="prenom" tabindex=2 autocorrect="off" spellcheck="false"
							autocapitalize="off" autofocus onblur="verifPseudo(this)" value="<?php echo stripslashes($_SESSION['prenom']);?>"/></br></br>
							
							<input id="signup" tabindex=4 type="submit" class="btn btn-primary" value="Enregistrer" name="Enregistrer1">
							<a href="profil.php" class="btn btn-default">Annuler</a>
							</br></br>
					</fieldset>
				</form>
				
			<?php	
			////Partie de la modification du n°de gsm
			}else if($var == 'phone'){?>
				<FORM METHOD="POST" ACTION="#">
					<fieldset>
					<label for="gsm">N° gsm</label><input
							type="tel" 
							pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$"
							 name="gsm" id="gsm" tabindex=5 autocorrect="off"
							spellcheck="false" autocapitalize="off" autofocus value="<?php echo $_SESSION['gsm'];?>"/></br></br>
							
							<input id="signup" tabindex=4 type="submit" class="btn btn-primary" value="Enregistrer" name="Enregistrer2">
							<a href="profil.php" class="btn btn-default">Annuler</a>
							</br></br>
					</fieldset>
				</form>
			
			
			<?php	
			////Partie de la modification de l'email avec vérification du mot de passe
			}else if($var == 'email'){?>
				<FORM METHOD="POST" ACTION="#">
					<fieldset>
					<label for="email1">E-mail</label><input type="text" name="email1"
							id="email1" tabindex=3 autocorrect="off" spellcheck="false"
							autocapitalize="off" autofocus onblur="verifMail(this)" value="<?php if(isset($_POST['email1'])) { echo htmlentities($_POST['email1']);} else echo $_SESSION['email'];?>"/> </br>
							
						<label for="email2">Confirmation de l'e-mail</label><input type="text"
							name="email2" id="email2" tabindex=4 autocorrect="off"
							spellcheck="false" autocapitalize="off" autofocus
							onblur="verifMail(this)" value="<?php if(isset($_POST['email2'])) { echo htmlentities($_POST['email2']);}?>"/> </br>
							
						<label for="password">Mot de passe</label><input type="password"
							name="password" id="password" tabindex=11
							placeholder="mot de passe"></br></br>
							
							<input id="signup" tabindex=4 type="submit" class="btn btn-primary" value="Enregistrer" name="Enregistrer3">
							<a href="profil.php" class="btn btn-default">Annuler</a>
							</br></br>
					</fieldset>
				</form>
			
			
			<?php
			////Partie de la modification de l'adresse
			}else if($var == 'adresse'){ ?>
				<FORM METHOD="POST" ACTION="#">
					<fieldset>
						<label for="rue">Rue/Avenue (optionnel)</label><input type="text" name="rue"
							id="rue" tabindex=6 autocorrect="off" spellcheck="false"
							autocapitalize="off" autofocus value="<?php if(isset($_POST['rue'])) { echo htmlentities($_POST['rue']);} else if(!empty($_SESSION['rue'])) echo stripslashes($_SESSION['rue']);?>"/></br>
							
						<label for="numero">Numéro (optionnel)</label><input type="text" name="numero" id="numero" tabindex=7
							autocorrect="off" spellcheck="false" autocapitalize="off"
							autofocus value="<?php if(isset($_POST['numero'])) { echo htmlentities($_POST['numero']);} else if(!empty($_SESSION['numero'])) echo $_SESSION['numero'];?>"/> </br>
							
						<label for="codepost">Code postal</label><input
							type="number" min="0" name="codePost" id="codepost" tabindex=8
							autocorrect="off" spellcheck="false" autocapitalize="off"
							autofocus onblur="verifcodePost(this)" value="<?php if(isset($_POST['codePost'])) { echo htmlentities($_POST['codePost']);} else echo $_SESSION['codePost'];?>"/> </br>
							
						<label for="ville">Ville</label><input type="text" name="ville" id="ville" tabindex=9
							autocorrect="off" spellcheck="false" autocapitalize="off"
							autofocus value="<?php if(isset($_POST['ville'])) { echo htmlentities($_POST['ville']);}  else echo stripslashes($_SESSION['ville']);?>"/></br>
							
						<label for="pays">Pays </label><input type="text" name="pays" id="pays" tabindex=10 autocorrect="off"
							spellcheck="false" autocapitalize="off" autofocus value="<?php if(isset($_POST['pays'])) { echo htmlentities($_POST['pays']);} else echo stripslashes($_SESSION['pays']);?>"/></br></br>
					
					<input id="signup" tabindex=4 type="submit" class="btn btn-primary" value="Enregistrer" name="Enregistrer4">
					<a href="profil.php" class="btn btn-default">Annuler</a>
					
					</fieldset>
				</form>
				
				
			<?php	
			////Partie de la modification de la visibilité de l'adresse
			}else if($var == 'visibiliteAdresse'){ ?>
			<FORM METHOD="POST" ACTION="#">
				<fieldset>
					<input type="radio" name="adrVisible" value="0" id="onlyMe" <?php if($_SESSION['visibiliteAdresse'] == 0) echo 'checked';?> ><label for="non"> Moi uniquement </label>
					<input type="radio" name="adrVisible" value="1" id="all"  <?php if($_SESSION['visibiliteAdresse'] == 1) echo 'checked';?>  ></input> <label for="oui"> Tout le monde </label>
					<input id="signup" tabindex=4 type="submit" class="btn btn-primary" value="Enregistrer" name="Enregistrer6">
				</fieldset>
			</FORM>		
			<?php 
			}else if($var == 'pwd'){?>
				<FORM METHOD="POST" ACTION="#">
					<fieldset>
					<label for="password">Mot de passe</label><input type="password"
							name="password" id="password" tabindex=11
							placeholder="mot de passe"></br></br>
							
					<label for="pwd1">Nouveau mot de passe</label><input type="password"
							name="pwd1" id="pwd1" tabindex=11
							placeholder="nouveau mot de passe"></br></br>
							
					<label for="pwd2">Confirmation du mot de passe</label><input type="password"
							name="pwd2" id="pwd2" tabindex=11
							placeholder="confirmation"></br></br>
							
							<input id="signup" tabindex=4 type="submit" class="btn btn-primary" value="Enregistrer" name="Enregistrer5">
							<a href="profil.php" class="btn btn-default">Annuler</a>
							</br></br>
					</fieldset>
				</form>
			
			
			
			<?php	
			////désactivation du compte
						}else if($var == 'desactive'){?>
									<div class="alert alert-dismissable alert-danger text-center">
											<h1 class=""> Désactiver le compte </h1>
											<img src="/images/site/warning.png" alt="attention infos correctes"/>
									    	<big>Attention! Une fois le compte désactivé vous serez invisible aux autres utilisateurs.</big>
									    	</br>Réactivation possible à partir du profil.</br>
									   </div>
								 		<form class="text-center" action="#" METHOD="POST">
										 	<input id="signup" type="submit" class="btn btn-danger" value="Désactivation" name="desactiver">
											<a href="profil.php" class="btn btn-default">Annuler</a>
										 </form>
										 </br></br>
									</div>
			<?php	
			////désactivation du compte
						}else if($var == 'newsletter'){?>
									
									<p class="lead"> La newsletter vous pemettra de recevoir l'offre et la demande la plus proche de chez vous.</p>
									<p class="lead"> En sélectionnant des catégories de préférences ci-dessous, vous nous permettez de cibler dans la newsletter ce dont vous voulez être informé.</p>
									
								<form class="text-center" action="#" METHOD="POST">
							                Je m'inscris à la newsletter <input type="checkbox" name="newsletter" <?php if(!empty($_SESSION['newsletter'])) echo 'checked'; ?> /></br>
							               
									                <?php 
									                $mysqli = connect_bdd();?>
							<div class="row">
								<div class="col-xs-6 col-sm-5"> <h2>Offres</h2>
									<?php         
									                $infoNewsletter = selectedCatNewsletter($mysqli,$_SESSION['id'],'offre');
									                $categorieManquante = notSelectCatNewsletter($mysqli,$_SESSION['id'],'offre');

									     if(!empty($infoNewsletter)){
									   //  echo'<h3 class="text-left">Vos préférences</h3>';
									                for ($i=0;$i<sizeof($infoNewsletter);$i++)
									                	 echo ' <p class="text-left"><input type="checkbox" name=" o'.$infoNewsletter[$i]['tagId'].'" checked/> '.$infoNewsletter[$i]['tagNom'].'</p>';
									     }
									     if(!empty($categorieManquante)){
									    //echo'<h3 class="text-left">Catégories qui pourraient vous intéresser</h3>';
									               for ($i=0; $i<sizeof($categorieManquante);$i++)
									                	echo ' <p class="text-left"><input type="checkbox" name=" o'.$categorieManquante[$i]['tagId'].'" /> '.$categorieManquante[$i]['tagNom'].'</p>';	                
									     } ?>
									 </div>
									 <div class="col-xs-6 col-sm-5"> <h2>Demandes</h2>
									<?php         
									                $infoNewsletter = selectedCatNewsletter($mysqli,$_SESSION['id'],'demande');
									                $categorieManquante = notSelectCatNewsletter($mysqli,$_SESSION['id'],'demande');
									      if(!empty($infoNewsletter)){         
									    // echo'<h3 class="text-left">Vos préférences</h3>';
									                for ($i=0;$i<sizeof($infoNewsletter);$i++)
									                	 echo ' <p class="text-left"><input type="checkbox" name=" d'.$infoNewsletter[$i]['tagId'].'" checked/> '.$infoNewsletter[$i]['tagNom'].'</p>';
									      }
									    if(!empty($categorieManquante)){
									   // echo'<h3 class="text-left">Catégories qui pourraient vous intéresser</h3>';
									               for ($i=0; $i<sizeof($categorieManquante);$i++)
									                	echo ' <p class="text-left"><input type="checkbox" name=" d'.$categorieManquante[$i]['tagId'].'" /> '.$categorieManquante[$i]['tagNom'].'</p>';	                
										}?>
									 </div>
									</div>
									      <?php 
									                	$mysqli->close();
									         ?> 
				                  			</p>
										 	<input id="signup" type="submit" class="btn btn-primary" value="Valider" name="enregistrer7">
											<a href="profil.php" class="btn btn-default">Annuler</a>
										
								 </form>	
			<?php	
			////modifier l'avatar
						}else if($var == 'avatar'){
							
						
						$lien = racine_serveur().'/images/avatars/';
						$emplacement = '../../images/avatars/';
					if (!empty($_POST['submit'])){
						if(upload("avatar",$emplacement,array("png","jpg","gif"), 0) == true) {
							redirect('profil.php');
						}
					}
			
					echo  '<div class="col-md-3 col-lg-3 " align="center">'.afficher_avatar($_SESSION ['id'],$lien,$emplacement,100,100).'</div>';
					?>
	
			 </br></br>
				<form enctype="multipart/form-data" action="#" method="post">
		         <fieldset>
		 			<legend><span class="glyphicon glyphicon-camera"></span> Modifier votre avatar </legend>
		  		   <label for="avatar">Choisissez un avatar</label><input type="file" class="btn btn-default" name="avatar" id="avatar"/></br></br>
		    	 <input type="submit" class="btn btn-primary" value="Envoyer" name="submit"/> <a href="profil.php" class="btn btn-default">Annuler</a>
		         </fieldset>
    			 </form>
						
			<?php	
			}else redirect('profil.php');
			?>
			<label>
			 <?php
			 	$var_validité = true;   // On part du principe que les données encodées sont bonnes,
			 							// elles sont vérifiées par la suite et on tourne la valeur à false si les données sont mauvaises ou incomplètes
			 							
			 	$err_message = '';		// Variable servant à enregistrer tous les messages d'erreur d'encodage et sont affichés par la suite
			 	
			 	$email = $_SESSION['email'];
			 	$id = $_SESSION['id'];
			 	
			/////Modification du nom et prénom
				if (isset ( $_POST ['Enregistrer1'])) {
				$modif = false;
					if (!empty($_POST['nom'])){
						$nom = $_POST['nom'];
						// cérification que le nom est valide
						

						if (strlen ($nom) < 4) {
							$err_message .=  'Nom trop court! (minimum 4 caractères)';
							$var_validité = false;
						}
						$mysqli = connect_bdd();
						$nom = fixtags($nom);
						update_nom($mysqli,$id,$nom);
						$mysqli->close();
						$_SESSION['nom'] = $nom;
						$modif = true;
					}
					if (!empty($_POST['prenom'])){
						$prenom = $_POST['prenom'];
						/*
						if (preg_match ( "/([^A-Za-z0-9])/", $prenom ) > 0) {
							$err_message .= 'Caractère(s) invalides dans le prenom';
							$var_validité = false;
						}*/
						if (strlen ($prenom) < 4) {
							$err_message .=  'Prenom trop court! (minimum 4 caractères)';
							$var_validité = false;
						}
						$mysqli = connect_bdd();
						$prenom = fixtags($prenom);
						
						//maj du prenom dans la bdd
						update_prenom($mysqli,$id,$prenom);
						
						$mysqli->close();
						$_SESSION['prenom'] = $prenom;
						$modif = true;
					}
					if($modif) redirect('profil.php');
					else echo'Les deux champs sont vides, aucune modification n\'a été faite.'; 
					
				}
		// Modification du numéro de gsm
				if (isset ( $_POST ['Enregistrer2']))
					if (!empty ( $_POST ['gsm'] )){
						$mysqli = connect_bdd();
						$gsm = fixtags($_POST['gsm']);
						
						// maj du numéro de gsm
						update_gsm($mysqli,$id,$gsm);
						
						$mysqli->close();
						$_SESSION['gsm'] = $gsm;
						redirect('profil.php');
						
				}

				
				/////////////////Modification de l'email avec vérification email 2 + mot de passe car le mail est un moyen de connexion
				if (isset ( $_POST ['Enregistrer3'])){
					
					$email1 = $_POST['email1'];
					$email2 = $_POST['email2'];
					
					if (! (filter_var ( $email1, FILTER_VALIDATE_EMAIL ))) {
						$err_message .= 'Le mail inscrit n\'est pas valide. Réessayez!</br>';
						$var_validité = false;
					}
					else{
						$mysqli = connect_bdd();
						$query = $mysqli->query( "SELECT id FROM users WHERE email = '$email1'" );
						if (mysqli_num_rows($query)==1) {
							$err_message .= 'Un compte utilise déjà cet e-mail';
							$var_validité = false;
						}
					}
					if ($email1 != $email2) {
						$err_message .= 'La vérification de l\'email de correspond pas.</br>';
						$var_validité = false;
					}
					if (empty($_POST['password'])){
						$err_message .= 'Le mot de passe n\'a pas été complété.</br>';
						$var_validité = false;
					}else{
						$mysqli = connect_bdd();
						$user = mysqli_fetch_array($mysqli->query(" Select * from users where email='$email'" ) );
						if(!decrypter_pwd_salt($_POST['password'],$user)){
							$err_message .= 'Le mot de passe est incorrect.</br>';
							$var_validité = false;
						}
					}
					if(!$var_validité) echo $err_message;  // On vérifie si la variable est à false, on affiche les erreurs sinon on fait la modif dans la bdd.
					else{
					
						// modif du mail dans la bdd
						
						
						$mysqli = connect_bdd();
						//maj du mail
						update_email($mysqli,$id,$email1);
						$mysqli->close();
						$_SESSION['email'] = $email1;
						redirect('profil.php');
					}
						
					
				}
				
				////Modification des données de l'adresse
						
				if (isset ( $_POST ['Enregistrer4'])){
					$mysqli = connect_bdd();
					if (!empty ( $_POST ['rue'] )){
						$rue = $_POST['rue'];
						update_rue($mysqli,$id,$rue);
						$_SESSION['rue'] = $rue;
					}
					if (empty ( $_POST ['rue'] )){
						update_rue($mysqli,$id,NULL);
						$_SESSION['rue'] = null;
					}
					if (empty ( $_POST ['numero'] )){
						update_numero($mysqli,$id,NULL);
						$_SESSION['numero'] = null;
					}
					if (!empty ( $_POST ['numero'] )){
						$numero = $_POST['numero'];
						update_numero($mysqli,$id,$numero);
						$_SESSION['numero'] = $numero;
					}
					
					if (!empty ( $_POST ['codePost'] )){
						$codePost = $_POST['codePost'];
						update_codePost($mysqli,$id,$codePost);
						$_SESSION['codePost'] = $codePost;
					}
					
					if (!empty ( $_POST ['ville'] )){
						$ville = $_POST['ville'];
						update_ville($mysqli,$id,$ville);
						$_SESSION['ville'] = $ville;
					}
						
					if (!empty ( $_POST ['pays'] )){
						$pays= $_POST['pays'];
						update_pays($mysqli,$id,$pays);		
						$_SESSION['pays'] = $pays;
					}
					$mysqli->close();
					redirect('profil.php');

				}
				////Visibilité de l'adresse
				
				if (isset ( $_POST ['Enregistrer6'])){
					$mysqli = connect_bdd();
					update_visibiliteAdresse($mysqli,$id,$_POST['adrVisible']);
					$mysqli->close();
					$_SESSION['visibiliteAdresse'] = $_POST['adrVisible'];
					redirect('profil.php');
				}
				
				
				////////////////Modification du mot de passe 
				if (isset ( $_POST ['Enregistrer5'])){
					$pwd = $_POST['password'];
					$pwd1 = $_POST['pwd1'];
					$pwd2 = $_POST['pwd2'];

					if (empty($_POST['pwd1'])||empty($_POST['pwd2'])||empty($_POST['password'])) {
						$err_message .= 'Un champ était incomplet</br>';
						$var_validité = false;
					}else if ($pwd1 != $pwd2) {
						$err_message .= 'Le nouveau mot de passe et la confirmation ne correspondait pas.</br>';
						$var_validité = false;
					}else{
						$mysqli = connect_bdd();
						$user = mysqli_fetch_array(select_all_from_users($mysqli,$email) );
						if(!decrypter_pwd_salt($_POST['password'],$user)){
							$err_message .= 'Le mot de passe d\'origine était incorrect.</br>';
							$var_validité = false;
						}
					}
					if(!$var_validité) echo $err_message;  // On vérifie si la variable est à false, on affiche les erreurs sinon on fait la modif dans la bdd.
					else{
						
						// création du salt
						$salt = generateRandomString ( 60 );
						// cryptage du pass avec le salt
						$pwd1 = crypt ( $pwd1, $salt);
						
						$mysqli = connect_bdd ();
						//Maj du mot de pass et su salt
						maj_pwd_salt($mysqli,$id,$pwd1,$salt);
						
						$mysqli->close();
						redirect('profil.php');
					}
				}
				// Modification du numéro de gsm
				if (isset ( $_POST ['desactiver'])){
					$mysqli = connect_bdd();
					// désactivation , on envoit 1 en param
					activer_desactiver($mysqli, $_SESSION['id'],1);
					$mysqli->close();
					redirect('profil.php');
				
				}
				
				////Newsletter
				
				if (isset ( $_POST ['enregistrer7'])){
					$mysqli = connect_bdd();
					if(empty($_POST['newsletter'])) maj_newsletter($mysqli,$_SESSION['id'],0);
					else maj_newsletter($mysqli,$_SESSION['id'],1);	

					
					$nbCat = nbCategorie($mysqli);
					for($i=1;$i<$nbCat+1;$i++){
						if(!empty($_POST['o'.$i])) insertCatNewsletter($mysqli,$i,$_SESSION['id'],'offre');
						else  deleteCatNewsletter($mysqli,$i,$_SESSION['id'],'offre');
						
						if(!empty($_POST['d'.$i])) insertCatNewsletter($mysqli,$i,$_SESSION['id'],'demande');
						else deleteCatNewsletter($mysqli,$i,$_SESSION['id'],'demande');
					
					}
					$mysqli->close();
					redirect('modifierProfil.php?newsletter');
				}
				
				
					
				
			?>
			</label>
			</div>
		</div>
	</div>

</body>
<?php include('../footer.php'); ?>
</html>
