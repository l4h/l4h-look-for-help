<?php
include ('../header.php');
if (! connected ())
	redirect ( '../index.php' );

?>

<body>
<?php
$mysqli = connect_bdd ();
$id = $_SESSION ['id'];
$prenom = $_SESSION ['prenom'];
$nom = $_SESSION ['nom'];
$email = $_SESSION ['email'];
$user = mysqli_fetch_array ( $mysqli->query ( " Select * from users where email='$email'" ) );
$prenom = $user ['prenom'];
$email = $user ['email'];
$nom = $user ['nom'];
$rue = $user ['rue'];
$numero = $user ['numero'];
$codePost = $user ['codePost'];
$ville = $user ['ville'];
$pays = $user ['pays'];
$gsm = $user ['gsm'];
$dateInscription = $user ['dateInscription'];
$desactive = $user ['desactive'];
$visibiliteAdresse = $user ['visibiliteAdresse'];
$dateNaiss = new DateTime ( $user ['dateNaissance'] );

$modifNom = 'modifierProfil.php?nom';
$modifAdresse = 'modifierProfil.php?adresse';
$modifEmail = 'modifierProfil.php?email';
$modifGsm = 'modifierProfil.php?phone';
$modifPwd = 'modifierProfil.php?pwd';
$modifDesactive = 'modifierProfil.php?desactive';
$modifAvatar = 'modifierProfil.php?avatar';
$modifVisibiliteAdresse = 'modifierProfil.php?visibiliteAdresse';
$modifNewsletter = 'modifierProfil.php?newsletter';
?>
</br>
	</br>
	</br>
	</br>
	</br>
	<div class="container">
		<div class="row">
      <?php
						
						if (isDesactive ( $_SESSION ['id'] )) {
							?>
  		<div class="alert alert-dismissable alert-danger text-center">
  		
				<img src="/images/site/warning.png" alt="attention infos correctes"/>
				<big>Veuillez réactiver votre compte pour accéder au site.</big></br></br>
				Vous êtes pour le moment invisble des autres utilisateurs
		 </div>
  <?php }?>
        <div
				class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">


				<div class="panel panel-info">
					<div class="panel-heading">
						<h3 class="panel-title"><?php echo $prenom.' '.$nom.' <small><a href="'.$modifNom.'">Modifier</a></small>'; ?></h3>
					</div>
					<div class="panel-body">
						<div class="row">
                		<?php
																		$lien = racine_serveur () . '/images/avatars/';
																		$emplacement = '../../images/avatars/';
																		if (! empty ( $_POST ['submit'] )) {
																			if (upload ( "avatar", $emplacement, array (
																					"png",
																					"jpg",
																					"gif" 
																			), 0 ) == true) {
																				redirect ( 'profil.php' );
																			}
																		}
																		
																		echo '<div class="col-md-3 col-lg-3 " align="center">' . afficher_avatar ( $_SESSION ['id'], $lien, $emplacement, 100, 100 ) . '</br><small><a href="' . $modifAvatar . '">Modifier</a></small></div>';
																		?>
	
                <div class=" col-md-9 col-lg-9 ">
								<table class="table table-user-information">
									<tbody>
										<tr>
											<td>Date de naissance</td>
											<td><?php echo $dateNaiss->format( 'd-m-Y' );?></td>
										</tr>
										<tr>
											<td>Adresse</td>
											<td><?php
											if (! empty ( $rue ))
												echo $rue;
											if (! empty ( $rue ) && ! empty ( $numero ))
												echo ',';
											if (! empty ( $numero ))
												echo ' n°' . $numero . '</br>';
											if (! empty ( $rue ) && empty ( $numero ))
												echo '</br>';
											echo $codePost . ' ' . $ville . '</br>';
											echo $pays . ' <small><a href="' . $modifAdresse . '">Modifier</a></small>';
											?>
						</td>
										</tr>
										<tr>
											<td>Visibilité de l'adresse</td>
											<td>
                        	<?php
																									
																									if ($visibiliteAdresse)
																										echo 'Tout le monde';
																									else
																										echo 'Moi uniquement</br>';
																									?>
                        	<?php echo '<small><a href="'.$modifVisibiliteAdresse.'">Modifier</a></small>';?>
                        </td>
										</tr>
										<tr>
											<td>Email</td>
											<td><?php echo'<a href="mailto:'.$email.'">'.$email.'</a> <small><a href="'.$modifEmail.'">Modifier</a></small>'; ?></td>
										</tr>
										<tr>
											<td>Numéro de gsm</td>
											<td>
                        	<?php echo $gsm.' <small><a href="'.$modifGsm.'">Modifier</a></small>';?>
                        </td>
										</tr>
										<tr>
											<td>Date d'inscription</td>
											<td><?php $dateInscription = new DateTime ( $dateInscription ); echo $dateInscription->format ( 'd-m-Y H:i:s' );?></td>
										</tr>
										<tr>
											<td>Mot de passe</td>
											<td><?php echo '<small><a href="'.$modifPwd.'">Modifier </a></small>';?></td>
										</tr>
                      <?php if(userEstPremium($mysqli, $_SESSION['id'])){ ?>
		                      <tr>
											<td>Temps premium restant</td>
											<td>
		                      		<?php echo afficheNbJoursPremiumRestant($mysqli);?>   		
		                      		<small><a href="/php/premium/infoPremium.php">Prolonger</a></small>
											</td>
										</tr>
		                      <?php }else{?>
		                      <tr>
											<td></td>
											<td><a href="/php/premium/infoPremium.php"> <img
													src="/images/site/info.png" WIDTH=22 HEIGHT=22> Devenir
													Premium
											</a></td>
										</tr>
		                      
                      <?php }?><tr>
                      <td>Identifiant SIP :</td>
					<td><?php echo $_SESSION['id'];?></td>
                    </tr>
                    <tr>
                     <td>Serveur Asterisk :</td>
					<td>l4h.ddns.net</td>
                     </tr> 
                    </tbody>
								</table>
							</div>
						</div>
					</div>

					<div class="panel-footer">
						<span class="pull-right">		
						<?php if(!$desactive){?>
   							<a href="<?php echo $modifDesactive; ?>"
							data-original-title="Désactiver le compte" data-toggle="tooltip"
							type="button" class="btn btn-sm btn-warning"><i
								class="glyphicon glyphicon-remove"></i></a>
                    	<?php }else{?>
                    		<a href="profil.php?activer=true"
							data-original-title="Activer le compte" data-toggle="tooltip"
							type="button" class="btn btn-sm btn-success"><i
								class="glyphicon glyphicon-ok"></i></a>
                    	<?php
						}
						if (isset ( $_GET ['activer'] )) {
							activer_desactiver ( $mysqli, $_SESSION ['id'], 0 );
							redirect ( 'profil.php' );
						}
						
						?>
                  </span> <a href="<?php echo $modifNewsletter;?>"
							data-original-title="Options de la newsletter"
							data-toggle="tooltip" type="button"
							class="btn btn-sm btn-primary"><i
							class="glyphicon glyphicon-envelope"></i></a>
					</div>
				</div>
			</div>
		</div>

		<h3 class="text-center">Votre réputation</h3> 
    
    <?php
				if (! ($stmt = $mysqli->prepare ( "SELECT userIdDonnant, points, datePoints, a.annonceId, type
																					FROM users_points up 
																					join annonces a on a.annonceId = up.annonceId
																					WHERE userIdRecevant = (?)" )))
					echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
				if (! $stmt->bind_param ( "i", $_SESSION ['id'] ))
					echo "Echec lors du liage des paramètres : (" . $stmt->errno . ") " . $stmt->error;
				if (! $stmt->execute ())
					echo "Echec lors de l'exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
				$stmt->bind_result ( $col1, $col2, $col3, $col4, $col5 );
				$result = array ();
				while ( $stmt->fetch () )
					array_push ( $result, array (
							$col1,
							$col2,
							$col3,
							$col4,
							$col5 
					) );
				$stmt->close ();
				
				$nbRep = count ( $result );
				if ($nbRep > 0) {
					echo "<div class='container'><table class='sortable table table-bordered table-hover table-condensed'>
				<thead><tr class='info'><td></td>
				<th>Prénom/Nom</th>
				<th>Point reçu</th>
				<th>Date</th>
				<th>[O]ffre/[D]emande - Titre</th>
		</tr></thead><tbody>";
					$nbDemandes = 0;
					$nbOffres = 0;
					
					for($i = 0; $i < $nbRep; $i ++) {
						$userDonnant = $result [$i] [0];
						$nbPoints = $result [$i] [1];
						$datePoints = $result [$i] [2];
						$annonceId = $result [$i] [3];
						$type = $result [$i] [4];
						
						if ($nbPoints < 0)
							echo "<tr class='danger'>";
						else if ($nbPoints > 0)
							echo "<tr class='success'>";
						echo "<td></td>";
						echo "<td>" . getPrenomNom ( $mysqli, $userDonnant ) . "</td>";
						echo "<td>" . $nbPoints . "</td>";
						echo "<td>" . $datePoints . "</td>";
						if ($type == 'offre') {
							echo "<td>[O] - " . getTitre ( $mysqli, $annonceId, $type) . "</td>";
							$nbOffres++;
						} else if ($type == 'demande') {
							echo "<td>[D] - " . getTitre ( $mysqli, $annonceId, $type ) . "</td>";
							$nbDemandes++;
						}
						echo "</tr>";
					}
					echo "</tbody>";
					echo "<tfoot>";
					echo "<tr class='warning'><td>TOTAL</td>";
					echo "<td>" . $nbRep . " personne(s)</td>";
					echo "<td>" . nbPointsUser ( $mysqli, $_SESSION ['id'] ) . " point(s)</td>";
					echo "<td>X</td>";
					echo "<td>".$nbOffres." offre(s) et ".$nbDemandes." demande(s)</td>";
					echo "</tr></tfoot>";
					echo "</table>";
				} else
					echo "<div class='text-center'>Vous n'avez pas encore reçu de points !</div>";
				echo "</div>"?>
    
    </div>
	<div class="container ">
	
		<?php
		$rue = str_replace ( " ", "+", $rue );
		$rue = str_replace ( "&#039;", "+", $rue );
		
		$ville = str_replace ( " ", "+", $ville );
		$ville = str_replace ( "&#039;", "+", $ville );
		
		$pays = str_replace ( "&#039;", "+", $pays );
		$pays = str_replace ( "&#039;", "+", $pays );
		
		$adresse = $numero . '+' . $rue . '+' . $codePost . '+' . $ville . '+' . $pays;
		
		$url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $adresse . "&key=AIzaSyDrNzSlMI6dRzhj-EwUhG6cRC3nnKnkbQY";
		$response = file_get_contents ( $url );
		$response = json_decode ( $response, true );
		
		if ((! empty ( $response ['results'] ))) {
			$lat = $response ['results'] [0] ['geometry'] ['location'] ['lat'];
			$long = $response ['results'] [0] ['geometry'] ['location'] ['lng'];
			
			?>

		<script>
		var latitude = '<?=$lat;?>';
		var longitude = '<?=$long;?>';
		var myCenter=new google.maps.LatLng(latitude,longitude);

		function initialize()
		{
		var mapProp = {
		  center:myCenter,
		  zoom:15,
		  mapTypeId:google.maps.MapTypeId.ROADMAP
		  };

		var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

		var marker=new google.maps.Marker({
		  position:myCenter,
		  });

		marker.setMap(map);
		
		var infowindow = new google.maps.InfoWindow({
		  content:"Vous êtes ici."
		  });

		infowindow.open(map,marker);
		}

		google.maps.event.addDomListener(window, 'load', initialize);
	
	</script>
	<?php }else echo'Votre localisation est impossible, veuillez vous assurer que votre adresse est bien complétée et correcte.'; ?>
	
	<h3 class="text-center">Votre position</h3>
		<div id="googleMap" class="googleMap mapProfil"></div>


	</div>
</body>
<?php
$mysqli->close ();
include ('../footer.php');
?>

<?php
function getTitre($mysqli, $idType, $type) {
		$titre = mysqli_fetch_array ( $mysqli->query ( "SELECT titre FROM annonces WHERE annonceId = " . $idType ." and type = '".$type."' ") );
		return $titre ['titre'];
}
?>
