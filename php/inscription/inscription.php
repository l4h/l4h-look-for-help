<?php
include ('../header.php');
if (connected ())
	redirect ( '/index.php' );
?>

<body>
	<div class="container">
		</br> </br> </br></br></br>
		<div class="row">
		<div class="col-md-6 col-md-offset-4">
		
						<div class=" alert alert-dismissable alert-warning">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
							<div class="row">
								<div class="col-md-2">
									<img src="/images/site/warning.png" alt="attention infos correctes"/>
								</div> 
		    					<big>Vous devez avoir 18 ans pour vous inscrire.</big>
		    				</div>
						</div>
				<FORM METHOD="POST" ACTION="#">
					<fieldset>
						<h2>Données de connexion</h2>	

						<label for="nom">Nom</label><input type="text" name="nom" id="nom"
							tabindex=1 autocorrect="off" spellcheck="false"
							autocapitalize="off" autofocus onblur="verifPseudo(this)"
							value="<?php if(isset($_POST['nom'])) { echo htmlentities($_POST['nom']);}?>" /></br>


						<label for="prenom">Prénom</label><input type="text" name="prenom"
							id="prenom" tabindex=2 autocorrect="off" spellcheck="false"
							autocapitalize="off" autofocus onblur="verifPseudo(this)"
							value="<?php if(isset($_POST['prenom'])) { echo htmlentities($_POST['prenom']);}?>" /></br>
						<?php 
							$date_limit = date('Y-m-d H:i:s', strtotime("-18 years"));	
						?>	
						
						
							
						<label for="dateNaiss">Date de naissance</label><input type="date" name="dateNaiss" value="<?php if(isset($_POST['dateNaiss'])) { echo htmlentities($_POST['dateNaiss']);}?>" max=<?php echo $date_limit; ?> /></br></br>
						
						
						
						
						<label for="email">E-mail</label><input type="text" name="email"
							id="email" tabindex=3 autocorrect="off" spellcheck="false"
							autocapitalize="off" autofocus onblur="verifMail(this)"
							value="<?php if(isset($_POST['email'])) { echo htmlentities($_POST['email']);}?>" />
						</br> <label for="email">Confirmation de l'e-mail</label><input
							type="text" name="email2" id="email2" tabindex=4
							autocorrect="off" spellcheck="false" autocapitalize="off"
							autofocus onblur="verifMail(this)"
							value="<?php if(isset($_POST['email2'])) { echo htmlentities($_POST['email2']);}?>" />
						</br> <label for="gsm">N° gsm</label><input type="tel" 
							pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$"
							 name="gsm"
							id="gsm" tabindex=5 autocorrect="off" spellcheck="false"
							autocapitalize="off" autofocus
							value="<?php if(isset($_POST['gsm'])) { echo htmlentities($_POST['gsm']);}?>" /></br>
						<h2>Adresse</h2>

						<label for="rue">Rue/Avenue (optionnel)</label><input type="text" name="rue"
							id="rue" tabindex=6 autocorrect="off" spellcheck="false"
							autocapitalize="off" autofocus
							value="<?php if(isset($_POST['rue'])) { echo htmlentities($_POST['rue']);}?>" /></br>

						<label for="numero">Numéro (optionnel)</label><input type="text" name="numero"
							id="numero" tabindex=7 autocorrect="off" spellcheck="false"
							autocapitalize="off" autofocus
							value="<?php if(isset($_POST['numero'])) { echo htmlentities($_POST['numero']);}?>" />
						</br> <label for="codepost">Code postal</label><input  type="number" min="0"
							name="codePost" id="codepost" tabindex=8 autocorrect="off"
							spellcheck="false" autocapitalize="off" autofocus
							onblur="verifcodePost(this)"
							value="<?php if(isset($_POST['codePost'])) { echo htmlentities($_POST['codePost']);}?>" />
						</br> <label for="ville">Ville</label><input type="text"
							name="ville" id="ville" tabindex=9 autocorrect="off"
							spellcheck="false" autocapitalize="off" autofocus
							value="<?php if(isset($_POST['ville'])) { echo htmlentities($_POST['ville']);}?>" /></br>

						<label for="pays">Pays </label><input type="text" name="pays"
							id="pays" tabindex=10 autocorrect="off" spellcheck="false"
							autocapitalize="off" autofocus
							value="<?php if(isset($_POST['pays'])) { echo htmlentities($_POST['pays']);}?>" /></br>
						<label for="adrVisible">Visibilité de l'adresse</label>
						
							<input type="radio" name="adrVisible" value="false" id="onlyMe" checked><label for="non"> Moi uniquement </label></br>
						<label></label>	<input type="radio" name="adrVisible" value="true" id="all" ></input> <label for="oui"> Tout le monde </label>

						<h2>Mot de passe</h2>

						<label for="password">Mot de passe</label><input type="password"
							name="password" id="password" tabindex=11
							placeholder="mot de passe"></br> <label for="password2">Confirmation
							du mot de passe</label><input type="password" name="password2"
							id="password2" tabindex=12 placeholder="mot de passe"></br>

						<h2>Veuillez recopier le texte</h2>
						<!--  Code recaptcha -->
						<?php
						affiche_captcha ();
						?>
						</br>
						<h4>Je m'inscris à la newsletter <input name="newsletter" type="checkbox" checked></h4><h5> (paramétrable dans le profil)</h5></br>
						<h4>J'accepte les <a href="/docs/CGU.php" target=_blank>conditions générales d'utilisation </a> <input name="CGU" type="checkbox" <?php if(!empty($_POST['CGU'])) echo 'checked';?>></h4>
						<div class="alert alert-dismissable alert-warning">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
							<div class="row">
								<div class="col-md-2">
									<img src="/images/site/warning.png" alt="attention infos correctes"/>
								</div> 
		    					<big>Dans un souci d'utilisation efficace du site, nous vous demandons de veiller à introduire des informations correctes.</big>
		    				</div>
						</div>
						<input id="signup" tabindex=4 type="submit" class="btn btn-primary" value="Créer" name="Enregistrement"></br>
						</br>
					</fieldset>
				</FORM>
				
				
    
    <?php
				$err_message = '';
				$var_validité = true;
				if (isset ( $_POST ['Enregistrement'] )) {
					if (! verif_captcha ()) {
						$err_message .= 'Le texte à recopier est invalide.</br>';
						$var_validité = false;
					}
					
					// / On vérifie que les champs sont bien remplis
					if (empty ( $_POST ['nom'] )) {
						$err_message .= ' Le champ nom est vide.</br>';
						$var_validité = false;
					}
					if (empty ( $_POST ['prenom'] )) {
						$err_message .= 'Le champ prenom est vide.</br>';
						$var_validité = false;
					}
					if (empty ( $_POST ['dateNaiss'] )){
						$err_message .=  'Vous n\'avez pas complété votre date de naissance</br>';
						$var_validité = false;
					}
					if (empty ( $_POST ['password'] )) {
						$err_message .= 'Le champ Mot de passe est vide.</br>';
						$var_validité = false;
					}
					if (empty ( $_POST ['password2'] )) {
						$err_message .= 'Le champ Confirmation de mot de passe est vide.</br>';
						$var_validité = false;
					}
					if (empty ( $_POST ['email'] )) {
						$err_message .= 'Le champ E-mail est vide.</br>';
						$var_validité = false;
					}
					if (empty ( $_POST ['email2'] )) {
						$err_message .= 'Le champ Confirmation de l\'e-mail est vide.</br>';
						$var_validité = false;
					}
					if (empty ( $_POST ['gsm'] )) {
						$err_message .= 'Le numéro de gsm est vide.</br>';
						$var_validité = false;
					}
					if (empty ( $_POST ['codePost'] )) {
						$err_message .= 'Le champ code postal est vide.</br>';
						$var_validité = false;
					}
					if (empty ( $_POST ['ville'] )) {
						$err_message .= 'Le champ ville est vide.</br>';
						$var_validité = false;
					}
					if (empty ( $_POST ['pays'] )) {
						$err_message .= 'Le champ pays est vide.</br>';
						$var_validité = false;
					}
					if (empty ( $_POST ['CGU'] )) {
						$err_message .= 'Vous devez accepter les conditions générales d\'utilisation.</br>';
						$var_validité = false;
					}
					
					// / enregistrement des post dans des variables
					
					$nom = fixtags ( $_POST ['nom'] );
					$prenom = fixtags ( $_POST ['prenom'] );
					if(!empty($_POST['rue'])) $rue = fixtags ( $_POST ['rue'] );
					else $rue = NULL;
					if(!empty($_POST['numero'])) $numero = fixtags ( $_POST ['numero'] );
					else $numero = NULL;
					$codePost = fixtags ( $_POST ['codePost'] );
					$ville = fixtags ( $_POST ['ville'] );
					$gsm = fixtags ( $_POST ['gsm'] );
					$pays = fixtags ( $_POST ['pays'] );
					$pass1 = $_POST ['password'];
					$pass2 = $_POST ['password2'];
					$pwd = $_POST ['password'];
					$email = $_POST ['email'];
					$email2 = $_POST ['email2'];
					$dateNaiss = $_POST['dateNaiss'];
					$visibiliteAdresse = $_POST['adrVisible'];
					if(empty($_POST['newsletter'])) $newsletter = false;
					else $newsletter = true;
					
					// / On vérifie que les champs on des caractères valides
					/*
					 * if (preg_match ( "/([^A-Za-z0-9])/", $_POST ['nom'] ) > 0) {
					 * $err_message .= 'Caractère(s) invalides dans le nom.</br>';
					 * $var_validité = false;
					 * }
					 */
					if (! (filter_var ( $email, FILTER_VALIDATE_EMAIL ))) {
						$err_message .= 'Le mail inscrit n\'est pas valide. Réessayez!</br>';
						$var_validité = false;
					}
					if ($email != $email2) {
						$err_message .= 'La vérification de l\'email de correspond pas. </br>';
						$var_validité = false;
					}
					if ($pass1 != $pass2) {
						$err_message .= 'La vérification de mot de passe ne correspond pas.</br>';
						$var_validité = false;
					}
					if (strlen ( $nom ) < 4) {
						$err_message .= 'Nom trop court! (minimum 4 caractères)</br>';
						$var_validité = false;
					}
					if (strlen ( $nom ) > 50) {
						$err_message .= 'Nom trop long! (maximum 50 caractères)</br>';
						$var_validité = false;
					}
					if (strlen ( $prenom ) > 50) {
						$err_message .= 'Prénom trop long! (maximum 50 caractères)</br>';
						$var_validité = false;
					}
					if (strlen ( $prenom ) < 4) {
						$err_message .= 'Prénom trop court! (minimum 4 caractères)</br>';
						$var_validité = false;
					}
					if (strlen ( $pwd ) < 4) {
						$err_message .= 'Mot de passe trop court! (minimum 4 caractères)</br>';
						$var_validité = false;
					}
					// Connexion bdd
					$mysqli = connect_bdd ();
					// recherche d'un user utilisant déjà ce mail
					$query = select_id_users($mysqli,$email);
					
					
					if (mysqli_num_rows ( $query ) == 1) {
						$err_message .= 'Erreur ... Un compte utilise déjà cet e-mail.</br>';
						$var_validité = false;
					}
					if ($var_validité) {
						
						// Creation du nombre aléatoire pour activation email
						$activationCode = "acti".generateRandomString (40);
						// création du salt
						$salt = generateRandomString ( 60 );
						// cryptage du pass avec le salt
						$pwd = crypt ( $pwd, $salt);

						creation_new_user($mysqli,$nom,$prenom,$dateNaiss,$rue,$numero,$codePost,$ville,$pays,$visibiliteAdresse,$newsletter,$email,$gsm,$pwd,$salt,$activationCode);
						
						// données pour l'envoi du mail
						$lienactivation = racine_serveur().'/php/session/connexion.php?activationCode=' . $activationCode;
						
						$titre = 'Votre activation sur looking for help';
						$message = '<h1 style="color:RGB(111,153,170)">Bienvenue sur Looking for help, </h1>';
						$message .= 'A l\'activation vous obtenez 15 jours de compte premium gratuitement. </br></br>';
						$message .= 'Cliquez <a href="'.$lienactivation.'">ici</a> afin d\'activer votre compte utilisateur. </br></br>';
						$message .= 'Si le lien ne fonctionne pas, recopier l\'adresse suivante dans votre navigateur web : </br>';
						$message .= '<a href="'.$lienactivation.'">' . $lienactivation . ' </a></br></br></br></br>';
						$message .= 'Nous vous remercions votre confiance et vous souhaitons une agréable journée, </br></br>';
						$message .= 'L\'équipe Looking for help. </br></br></br>';
						
						$message .='<table style="background-color:RGB(111,153,170)">';
						$message .='<tr style="text-align: center">';
						$message .='<td> Cet email à été envoyé à ' . $email .'</td>';
						$message .='</tr>';
						$message .='<tr style="text-align: center">';
						$message .='<td> L4H - Looking for help, rue du ciseau 16 Louvain la neuve, Belgique </td>';
						$message .='</tr>';
						$message .='<tr style="text-align: center">';
						$message .='<td> <a href="mailto:contact@l4h.be"> Contact </a> -- <a href="'.racine_serveur().'/docs/CGU.php"> Mentions légales </a> -- <a href="'.racine_serveur().'/docs/CGU.php"> Conditions d\'utilisation </a></td>';
						$message .='</tr>';
						$message .='</table>';
						if (envoi_email ( $titre, $message, $email )) {
							$id = getMaxUserId($mysqli);
							$mysqli->close ();
							ajoutAsterisk($mysqli,$id,$prenom,$nom,$_POST ['password'],$email);
							redirect('inscription_ok.php');
						} else
							echo '<div class="alert alert-dismissable alert-warning hidden-print">
									<a href="#" class="close" data-dismiss="alert">&times;</a>
										<p class="lead">>Une erreur s\'est produite lors de l\'envoi de l\'e-mail.</p>
									</center>
								</div>';
					}else{
						if (! $var_validité)
							echo '<div class="alert alert-dismissable alert-warning hidden-print">
									<a href="#" class="close" data-dismiss="alert">&times;</a>
									<h3><b>Erreur d\'inscription</b></h3>
										<p class="lead">' . $err_message . '</p>
									</center>
								</div>';
						
					}
					$mysqli->close ();
					
				}
				
				?>
				
     </div>
			<div class="col-md-2"></div>
		</div>
	</div>
	</div>

<?php include('../footer.php'); ?>  

</body>
</html>
