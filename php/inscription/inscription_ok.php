<?php
include ('../header.php');
if (connected ())
	redirect ( '../index.php' );
?>
<html>
<head>
</head>
<body>
	<div class="container">
		</br> </br> </br>
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-6">
				<?php echo '<p class="lead"></br></br></br>Inscription réussie avec succès !</br>Un e-mail d\'activation a été envoyé.</p></br></br></br>'; ?>

			<script type="text/javascript">
				// initialise le temps
				var cpt = 5;
				 
				timer = setInterval(function(){
				    if(cpt>0) // si on a pas encore atteint la fin
				    {
				        --cpt; // décrémente le compteur
				        var Crono = document.getElementById("Crono"); // récupère l'id
				        var old_contenu = Crono.firstChild; // stock l'ancien contenu
				        Crono.removeChild(old_contenu); // supprime le contenu
				        var texte = document.createTextNode(cpt); // crée le texte
				        Crono.appendChild(texte); // l'affiche
				    }
				    else // sinon brise la boucle
				    {
				    	document.location.replace('/index.php');
				    }
				}, 1000);
			 
			</script>

				<!-- le div ou on affiche le chrono, ne pas le mettre vide -->
				<p class="lead">
					Redirection dans <span id="Crono">5</span> secondes.
				</p>
			</div>
		</div>
	</div>
	<?php include('../footer.php'); ?>  
</body>
</html>