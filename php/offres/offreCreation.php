<?php include('../header.php'); 
	if(!connected()) redirect('../session/connexion.php');
?>
<!--  Cette page permet de remplir les services proposés -->
<body>
<div class="container fluid">
		</br>
		</br>
		</br>
		</br>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
	<?php	
	$err_message = array();
	$var_validite = true;
	$id = $_SESSION['id'];
	// on enregistre la date du jour
	$today = date("Y-m-d H:i:s");
	$date_limit = date('Y-m-d H:i:s', strtotime("+14 days"));
	$plusieurs_demandes = true;

		if(isset($_POST['offres'])){
    		
			$var_validite *= verifEmptyPostAnnonceCreation('offre',$_POST,$err_message);
	
			if($var_validite) $dateExpiration = $_POST['expiration'].' '.$_POST['heure'].':00';
			else{
				if(!isset($_POST['expiration']) && !isset($_POST['heure']))$dateExpiration  =  $date_limit;
				$dateExpiration = $_POST['expiration'].' '.$_POST['heure'].':00';
			}

			// connexion BDD
			$mysqli = connect_bdd();
			
			$var_validite *= compareDateAnnonceCreation($mysqli,$today,$dateExpiration,$date_limit,$err_message);
			
			
			// si validite OK
			if ($var_validite){
				// enregistrement des post dans des variables
				$titre = $_POST ['titre'];
				$tagId = $_POST ['tag'];
				$description = $_POST ['description'];

				
				$var_validite *= verifPossibiliteCreationAnnonce($mysqli,$id,'offre');
				
				if($var_validite){
					insertNewAnnonce($mysqli,'offre',$description,$titre,$today,$dateExpiration,$tagId);

					$messageSucces = "Votre offre a bien été créée.";
					$redirection = "/php/offres/mesOffres.php";
					echo '</br>'.redirectionTime($messageSucces,$redirection).'</br>';
				}
				$mysqli->close();
			}
		}
		if (!$var_validite) {

			echo'<div class="alert alert-dismissable alert-warning hidden-print">
				<a href="#" class="close" data-dismiss="alert">&times;</a>';
			echo '<h3><b>Erreur</b></h3><center><p class="lead">';
			echo '';
			foreach ($err_message as $msg) echo $tab_messErreurs[$msg].'</br>';
			echo'</p></center>
		</div>';

		}
		if (!isset ( $_POST ['offres'] ) || !$var_validite) {
			$mysqli = connect_bdd();
			if(!userEstPremium($mysqli,$id)){
				$nboffre = nbAnnonceEnCours($mysqli,'offre',$_SESSION['id']);
				if($nboffre > 0){
					$plusieurs_demandes = false;
					?>
					</br>
					<div class="alert alert-dismissable alert-warning">
						<div class="row">
							<div class="col-md-2">
								<img src="/images/site/warning.png" alt="attention"/>
							</div>
							<big>Vous n'êtes pas Premium, vous ne pouvez pas créer une seconde offre tant que la première est en cours.</br></big>
						</div>
					</div>
					<?php 
				}	
			}
		if($plusieurs_demandes){	
	?>
	<!--  FORMULAIRE HTML -->
	<h1>Proposer ses services</h1>

	<FORM METHOD="POST" ACTION="#">
		<!-- Titre de la proposition -->
		<label for="titre">Titre</label> 
		<input type="text" name="titre" id="titre" value="<?php if(isset($_POST['titre'])) { echo htmlentities($_POST['titre']);}?>" /></br>
		<label for="titre">Date d'expiration</label>				
			<?php
				if(!userEstPremium($mysqli,$id)){ ?>
				<input type="date" name="expiration"  value=<?php if(isset($_POST['expiration'])){ 
								$date = new DateTime ($_POST['expiration'] ); echo $date->format ( 'Y-m-d H:i:s');} 
								else echo $date_limit; ?>" min=<?php echo $today;?>  max=<?php echo $date_limit; ?> />
				</br></br></br>
				
				<div class="alert alert-dismissable alert-warning">
					En tant que non premium, la date d'expiration est limitée à 14 jours
				</div>
					
					
<?php 		}else{?> <input type="date" name="expiration" value=<?php if(isset($_POST['expiration'])){ 
								$date = new DateTime ($_POST['expiration'] ); echo $date->format ( 'Y-m-d H:i:s');} 
								else echo $date_limit; ?>" min=<?php echo $today; ?>></br></br>
				<?php }
				?>
		<label for="heure">Heure d'expiration</label> <input type="time" name="heure" value="<?php if(isset($_POST['heure'])) { echo $_POST['heure'];} else echo '00:00';?>"/></br></br>
		<!-- Type de services proposés -->
		<label for="type">Type</label> 
		<SELECT name="tag" size="1">
				
		<?php
			$query = "SELECT * from tags";
			if ($result = $mysqli->query ( $query )) {
			
				/* Récupération un tableau associatif */
				while ( $row = $result->fetch_assoc () ) {
					$tagId = $row['tagId'];
					echo "<OPTION value='$tagId'> $row[tagNom]";
				}
				$result->free ();
			}
			$mysqli->close ();
		?>
		</SELECT></br>

		<!-- Description de la proposition -->
		<legend>Description</legend>
		<textarea cols="80" rows="8" id="description" name="description"><?php if(isset($_POST['description'])) {echo htmlentities($_POST['description']);}?></textarea>
		</br></br>


		<!-- Bouton envoi -->
		<input id="offres" type="submit"
			value="Envoyer l'offre" name="offres" class="btn btn-primary"></br>
		</br>
			<?php
		} 
		}

				?>
			</div>
		</div>
	</div>
	</br></br>
</body>
<?php include('../footer.php'); ?> 
