<?php
if (! connected () || ! admin ())
	redirect ( '../index.php' );

if (is_numeric ( $_GET ['id'] ))
	$idUser = mysqli_real_escape_string ( $mysqli, $_GET ['id'] );
else
	redirect ( "administration.php" );

if ($_SERVER ['HTTP_HOST'] == 'localhost')
	$lien = '/images/avatars/';
else
	$lien = 'http://l4h.vv.si/images/avatars/';

$emplacement = 'images/avatars/';
if (file_exists ( $emplacement . $idUser . '.jpg' ))
	$avatar = '<img class="featurette-image img-responsive"  src="' . $lien . $idUser . '.jpg" alt="avatar">';
else if (file_exists ( $emplacement . $idUser . '.jpeg' ))
	$avatar = '<img class="featurette-image img-responsive"  src="' . $lien . $idUser . '.jpeg" alt="avatar">';
else if (file_exists ( $emplacement . $idUser . '.png' ))
	$avatar = '<img class="featurette-image img-responsive"  src="' . $lien . $idUser . '.png" alt="avatar">';
else if (file_exists ( $emplacement . $idUser . '.gif' ))
	$avatar = '<img  class="featurette-image img-responsive" src="' . $lien . $idUser . '.gif" alt="avatar">';
else
	$avatar = '<img src="' . $lien . 'inconnu.jpg">';

if (! ($stmt = $mysqli->prepare ( "SELECT id,nom,prenom,email,pwd,salt,rue,numero,codePost,ville,pays,gsm,dateInscription,dateActivation,dateDebutPremium,nbJoursPremium,
										GROUP_CONCAT(profils.label SEPARATOR ' / ') AS label
										FROM users
										JOIN users_profil ON users.id = users_profil.userId
										JOIN profils ON users_profil.profilId = profils.profilId
										WHERE id = (?)" )))
	echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
if (! $stmt->bind_param ( "i", $idUser ))
	echo "Echec lors du liage des paramètres : (" . $stmt->errno . ") " . $stmt->error;
if (! $stmt->execute ())
	echo "Echec lors de l'exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
$user = fetch ( $stmt );
$stmt->close ();

echo '<h4><span class="label label-primary">Modification de l\'utilisateur - ID#' . $user [0] ["id"] . '</span></h4>';
echo '<hr/> <h4><span class="label label-info">Avatar</span></h4>';
echo '<div class="form-horizontal alert alert-info breadcrumb">
			<div class="form-group">';

$av = 'images/avatars/' . $user [0] ['id'];
if (file_exists ( $av . '.jpg' ) || file_exists ( $av . '.jpeg' ) || file_exists ( $av . '.png' ) || file_exists ( $av . '.gif' )) {
	echo '	<label class="col-sm-3 control-label">Réprésentation de l\'utilisateur</label>
			<div class="col-xs-6 col-md-3">	<span class="thumbnail">
						' . $avatar . '
						</span>
				<form  enctype="multipart/form-data" method="post">
			    		<label>
							<button name="supprimerAvatar" type="submit" class="btn btn-danger">Supprimer cet avatar</button>
						</label></div></form>';
} else {
	echo '			<label class="col-sm-3 control-label">Aucun avatar</label>
				<form  enctype="multipart/form-data" method="post">
							<input type="file"
							name="avatar" id="avatar" /> <input type="submit" value="Envoyer"
							name="submit" />
				</form>';
}
if (isset ( $_POST ['submit'] ) && ! empty ( $_POST ['submit'] )) {
	$emplacement = 'images/avatars/';
	if (upload ( "avatar", $emplacement, array (
			"png",
			"jpg",
			"gif" 
	), $user [0] ['id'] ) == true) {
	}
	redirect ( "administration.php?id=" . $user [0] ['id'] );
}

if (isset ( $_POST ['supprimerAvatar'] )) {
	if (file_exists ( $av . '.jpg' ) && unlink ( 'images/avatars/' . $user [0] ['id'] . '.jpg' ))
		redirect ( "administration.php?id=" . $user [0] ['id'] );
	if (file_exists ( $av . '.png' ) && unlink ( 'images/avatars/' . $user [0] ['id'] . '.png' ))
		redirect ( "administration.php?id=" . $user [0] ['id'] );
	if (file_exists ( $av . '.gif' ) && unlink ( 'images/avatars/' . $user [0] ['id'] . '.gif' ))
		redirect ( "administration.php?id=" . $user [0] ['id'] );
	if (file_exists ( $av . '.jpeg' ) && unlink ( 'images/avatars/' . $user [0] ['id'] . '.jpeg' ))
		redirect ( "administration.php?id=" . $user [0] ['id'] );
}

echo '			</div></div>';

echo '<form class="form-horizontal" role="form" method="post" accept-charset="UTF-8">
			<hr/>
			<h4><span class="label label-info">Identité</span></h4>
			<div class="alert alert-info breadcrumb">
			<div class="form-group">
			<label class="col-sm-3 control-label">Nom</label>
				<div class="col-sm-5">
						Nom actuel : <strong>' . $user [0] ["nom"] . '</strong>
					<input type="text" class="form-control" name="nom_ch" placeholder="Nouveau nom">
				</div>
			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Prénom</label>
				<div class="col-sm-5">
						Prénom actuel : <strong>' . $user [0] ["prenom"] . '</strong>
					<input type="text" class="form-control" name="prenom_ch" placeholder="Nouveau prénom">
				</div>
			</div>
				
			<div class="form-group">
				<label for="inputEmail3" class="col-sm-3 control-label">Email</label>
				<div class="col-sm-5">
					Email actuel : <strong><a href="mailto:' . $user [0] ["email"] . '">' . $user [0] ["email"] . '</a></strong>
					<input name="email_ch1" type="email" class="form-control" placeholder="Nouvelle adresse Email">
					<input name="email_ch2" type="email" class="form-control" placeholder="Confirmez l\'adresse Email">
				</div>
			</div>
			
			<div class="form-group">
				<label for="inputPassword3" class="col-sm-3 control-label">Mot de passe</label>
				<div class="col-sm-5">
					<input name="pwd_ch1" type="password" class="form-control" id="inputPassword3" placeholder="Nouveau mot de passe">
					<input name="pwd_ch2" type="password" class="form-control" id="inputPassword3" placeholder="Confirmez le  mot de passe">
				</div>
			</div>
			</div>

			<hr/>
			<h4><span class="label label-info">Coordonneés</span></h4>

			<div class="breadcrumb alert alert-info">
			<div class="form-group">
			<label class="col-sm-3 control-label">Rue</label>
				<div class="col-sm-5">
						Rue actuelle : <strong>' . $user [0] ["rue"] . '</strong>
					<input name="rue_ch" type="text" class="form-control" name="inputNom" placeholder="Nouvelle rue">
				</div>
			</div>
				
			<div class="form-group">
			<label class="col-sm-3 control-label">Numéro</label>
				<div class="col-sm-5">
						Numéro actuel : <strong>' . $user [0] ["numero"] . '</strong>
					<input name="numero_ch" type="text" class="form-control" name="inputNom" placeholder="Nouveau numéro">
				</div>
			</div>

			<div class="form-group">
			<label class="col-sm-3 control-label">Code postal</label>
				<div class="col-sm-5">
						Code postal actuel : <strong>' . $user [0] ["codePost"] . '</strong>
					<input name="codePost_ch" type="text" class="form-control" name="inputNom" placeholder="Nouveau code postal">
				</div>
			</div>
			
			<div class="form-group">
			<label class="col-sm-3 control-label">Ville</label>
				<div class="col-sm-5">
						Ville actuelle : <strong>' . $user [0] ["ville"] . '</strong>
					<input name="ville_ch" type="text" class="form-control" name="inputNom" placeholder="Nouvelle ville">
				</div>
			</div>
			
			<div class="form-group">
			<label class="col-sm-3 control-label">Pays</label>
				<div class="col-sm-5">
						Pays actuel : <strong>' . $user [0] ["pays"] . '</strong>
					<input name="pays_ch" type="text" class="form-control" name="inputNom" placeholder="Nouveau pays">
				</div>
			</div>
			
			<div class="form-group">
			<label class="col-sm-3 control-label">GSM</label>
				<div class="col-sm-5">
						GSM actuel : <strong>' . $user [0] ["gsm"] . '</strong>
					<input name="gsm_ch" type="text" class="form-control" name="inputNom" placeholder="Nouveau GSM">
				</div>
			</div>
			</div>
			
			<hr/>
			<h4><span class="label label-info">Dates</span></h4>
			<div class="breadcrumb alert alert-info">
			<div class="form-group">
			<label class="col-sm-3 control-label">Date d\'inscription</label>
				<div class="col-sm-5">
						<input class="form-control" id="disabledInput" type="text" placeholder="' . $user [0] ["dateInscription"] . '" disabled>
				</div>
			</div>
			
			<div class="form-group">
			<label class="col-sm-3 control-label">Date d\'activation</label>
				<div class="col-sm-5">
						<input class="form-control" id="disabledInput" type="text" placeholder="' . $user [0] ["dateActivation"] . '" disabled>
				</div>
			</div>
			
			<div class="form-group">
			<label class="col-sm-3 control-label">Date début premium</label>
				<div class="col-sm-5">
						<input class="form-control" id="disabledInput" type="text" placeholder="' . $user [0] ["dateDebutPremium"] . '" disabled>
				</div>
			</div>
			</div>
			
			<hr/>
			<h4><span class="label label-info">Divers</span></h4>
		
			<div class="breadcrumb alert alert-info">
				<div class="form-group">
					<label class="col-sm-3 control-label">Compte premium</label>
					<div class="radio">
						<label>';
if (userEstPremium ( $mysqli, $user [0] ['id'] )) {
	echo '<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
						Activé, ';
	echo 'encore ' . nbJoursRestants ( $mysqli, $user [0] ['id'] ) . ' jour(s)';
	echo '</label>
					</div>
					<div class="radio">
							<label class="col-sm-3 control-label"></label>
							<label>
							<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
							Désactiver le premium
							</label>
					</div>
				</div>
			</div>';
} else {
	echo '<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
						Activer le premium pour [nombre de jour(s)]<input name="activerPour" type="number" step="1" value="0" min="0" max="1000">
					</label>
						</label>
					</div>
					<div class="radio">
							<label class="col-sm-3 control-label"></label>
							<label>
							<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" checked>
							Le premium est désactivé
							</label>
					</div>
				</div>
			</div>';
}

echo '<div class="breadcrumb alert alert-info">
			<div class="form-group">
			<label class="col-sm-3 control-label">Points</label>
					<label class="col-sm-5">
						<input name="points_ch" type="number" step="1" value="' . nbPointsUser ( $mysqli, $user [0] ['id'] ) . '" min="0">
					</label>
			</div>
			</div>
		
			<div class="breadcrumb alert alert-info">
			<div class="form-group">
			<label class="col-sm-3 control-label">Label</label>
					';
$changerLabel = false;
if (getValueUser ( $mysqli, $idUser ) == 1) {
	$changerLabel = true;
	$monLabel = getMonLabel ( $mysqli, $idUser );
	echo '<div class="col-sm-5">
			<select name="label">';
	if (strpos ( $monLabel, 'premium' ))
		$profils = $mysqli->query ( 'SELECT label FROM profils WHERE valeur IN (15,20);' );
	else
		$profils = $mysqli->query ( 'SELECT label FROM profils WHERE valeur IN (10,30);' );
	
	while ( $r = $profils->fetch_row () ) {
		if ($monLabel == $r [0])
			echo '<option value="' . $r [0] . '"  selected>' . $r [0] . '</option>';
		else
			echo '<option value="' . $r [0] . '">' . $r [0] . '</option>';
	}
	echo '</select></div>';
} else {
	echo ' <div class="col-sm-5">
						<input style="width:100%;"class="form-control" id="disabledInput" type="text" placeholder="' . $user [0] ["label"] . '" disabled>
				</div>';
}

echo '		
			</div>
			</div>';

echo '<div class="form-group"><hr/>
			<div class="col-sm-offset-3 col-sm-10">
			<button name="changer" type="submit" class="btn btn-success">Sauvegarder les modifications</button>&nbsp&nbsp';
if (userBanni ( $mysqli, $user [0] ['id'] ))
	echo '<button name="debannir" type="submit" class="btn btn-warning">Débannir</button>';
else
	echo '<button name="bannir" type="submit" class="btn btn-danger">Bannir cet utilisateur</button>';
echo '		</div>
			</div>
			</form>';
//Ban
if (isset ( $_POST ['bannir'] )) {
	$mysqli->query ( 'INSERT INTO users_profil VALUES (' . $user [0] ['id'] . ', 6);' );
	redirect ( "administration.php?id=" . $user [0] ['id'] );
}
//Deban
if (isset ( $_POST ['debannir'] )) {
	$mysqli->query ( 'DELETE FROM users_profil WHERE userId = ' . $user [0] ['id'] . ' AND profilId = 6;' );
	redirect ( "administration.php?id=" . $user [0] ['id'] );
}
//Changement des donnees de l'utilisateur
if (isset ( $_POST ['changer'] ) && ! empty ( $_POST )) {
	if ($changerLabel)
		$mysqli->query ( 'UPDATE users_profil SET profilId = (SELECT profilId FROM profils WHERE label = "' . $_POST ['label'] . '") WHERE userId = ' . $idUser . ';' );
	
	$request = "UPDATE users
	SET nom=(?),prenom=(?),email=(?),pwd=(?),rue=(?),numero=(?),codePost=(?),ville=(?),pays=(?),gsm=(?),dateDebutPremium=(?),nbJoursPremium=(?)
	WHERE id=(?)";
	
	if (! ($stmt = $mysqli->prepare ( $request )))
		echo "Echec lors de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
	
	$nom = (! empty ( $_POST ['nom_ch'] )) ? $_POST ['nom_ch'] : $user [0] ['nom'];
	$prenom = (! empty ( $_POST ['prenom_ch'] )) ? $_POST ['prenom_ch'] : $user [0] ['prenom'];
	$email = (! empty ( $_POST ['email_ch1'] ) && ! empty ( $_POST ['email_ch2'] ) && ($_POST ['email_ch1']) === $_POST ['email_ch2'] && emailExiste ( $mysqli, $_POST ['email_ch1'] )) ? $_POST ['email_ch1'] : $user [0] ['email'];
	
	if ($email != $user [0] ['email']) {
		$mysqli->query ( "UPDATE users_profil SET profilId=4 WHERE userId =" . $user [0] ['id'] );
		$reactivationCode = 'reac' . generateRandomString ( 40 );
		$mysqli->query ( "INSERT INTO activations VALUES (" . $user [0] ['id'] . ",'" . $reactivationCode . "')" );
		$lienactivation = 'http://l4h.vv.si/connexion.php?reactivationCode=' . $reactivationCode;
		$titre = 'Looking For Help : EMAIL de réactivation';
		$message = 'Votre compte doit être réactivé, veuillez cliquer sur ce lien : </br> <a href="' . $lienactivation . '">' . $lienactivation . ' </a></br>';
		$message .= 'Merci de la confiance que vous nous accordez! </br></br>';
		$message .= 'Nous vous souhaitons une bonne journée.';
		if (! envoi_email ( $titre, $message, $email ))
			echo '<p class="lead"></br>Une erreur c\'est produite lors de l\'envoi de l\'e-mail.</p>';
	}
	
	if (! empty ( $_POST ['pwd_ch1'] ) && ! empty ( $_POST ['pwd_ch2'] ) && ($_POST ['pwd_ch1'] == $_POST ['pwd_ch2']))
		$pwd = $_POST ['pwd_ch1'];
	$rue = (! empty ( $_POST ['rue_ch'] )) ? $_POST ['rue_ch'] : $user [0] ['rue'];
	$numero = (! empty ( $_POST ['numero_ch'] )) ? $_POST ['numero_ch'] : $user [0] ['numero'];
	$codePost = (! empty ( $_POST ['codePost_ch'] )) ? $_POST ['codePost_ch'] : $user [0] ['codePost'];
	$ville = (! empty ( $_POST ['ville_ch'] )) ? $_POST ['ville_ch'] : $user [0] ['ville'];
	$pays = (! empty ( $_POST ['pays_ch'] )) ? $_POST ['pays_ch'] : $user [0] ['pays'];
	$gsm = (! empty ( $_POST ['gsm_ch'] )) ? $_POST ['gsm_ch'] : $user [0] ['gsm'];
	
	// Activation premium
	$dateDebutPremium = (isset ( $_POST ['optionsRadios'] ) && isset ( $_POST ['activerPour'] ) && $_POST ['activerPour'] > 0) ? date ( "Y-m-d H:i:s" ) : $user [0] ['dateDebutPremium'];
	$nbJoursPremium = (isset ( $_POST ['optionsRadios'] ) && isset ( $_POST ['activerPour'] ) && $_POST ['activerPour'] > 0) ? $_POST ['activerPour'] : $user [0] ['nbJoursPremium'];
	if (isset ( $_POST ['activerPour'] ) && $_POST ['activerPour'] > 0){
		if(userAdmin( $mysqli, $idUser ))
		$mysqli->query ( "UPDATE users_profil SET profilId=7 WHERE userId =" . $user [0] ['id'] . ";" );
			else 
		$mysqli->query ( "UPDATE users_profil SET profilId=2 WHERE userId =" . $user [0] ['id'] . ";" );
	}
		// Desactivation premium
	if (isset ( $_POST ['optionsRadios'] ) && $_POST ['optionsRadios'] == 'option2') {
		$dateDebutPremium = null;
		$nbJoursPremium = 0;
		if(userAdmin( $mysqli, $idUser ))
		$mysqli->query ( "UPDATE users_profil SET profilId=1 WHERE userId =" . $user [0] ['id'] . ";" );
			else
		$mysqli->query ( "UPDATE users_profil SET profilId=3 WHERE userId =" . $user [0] ['id'] . ";" );
	}
	
	$pts = nbPointsUser ( $mysqli, $user [0] ['id'] );
	if ($_POST ['points_ch'] != $pts)
		$mysqli->query ( "INSERT INTO users_points (userIdRecevant,points,userIdDonnant,datePoints,annonceId) VALUES (" . $user [0] ['id'] . ", " . ($_POST ['points_ch'] - $pts) . ", " . $_SESSION ['id'] . ", NOW(), null);" );
	
	$salt = (mysqli_fetch_array ( $mysqli->query ( 'SELECT salt FROM users WHERE id=' . $user [0] ['id'] ) ));
	
	if (! empty ( $pwd ))
		$new_pwd = crypt ( $pwd, $salt ['salt'] );
	else
		$new_pwd = getPwdUser ( $mysqli, $user [0] ['id'] );
		
		// Filtre balises HTML
	$nom = strip_tags ( $nom );
	$prenom = strip_tags ( $prenom );
	$email = strip_tags ( $email );
	$rue = strip_tags ( $rue );
	$numero = strip_tags ( $numero );
	$codePost = strip_tags ( $codePost );
	$ville = strip_tags ( $ville );
	$pays = strip_tags ( $pays );
	$gsm = strip_tags ( $gsm );
	// $dateDebutPremium = strip_tags ( $dateDebutPremium );
	
	if (! $stmt->bind_param ( "ssssssissssii", $nom, $prenom, $email, $new_pwd, $rue, $numero, $codePost, $ville, $pays, $gsm, $dateDebutPremium, $nbJoursPremium, $user [0] ['id'] )) {
		echo "Echec lors du liage des paramètres : (" . $stmt->errno . ") " . $stmt->error;
	}
	if (! $stmt->execute ())
		echo "Echec lors de l'exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
	
	$stmt->close ();
	redirect ( "administration.php?id=" . $user [0] ['id'] );
}
?>
