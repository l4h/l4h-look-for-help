<?php
include ('../header.php');
if (! connected () || ! admin ())
	redirect('/index.php'); ?>

<body>
	<br />
	<br />
	<br />
	<br />
	<br />
	<div class="container">
		<div class="row row-centered">
			<div class="col-md-3">
				<form method="post" accept-charset='UTF-8'>
					<button name="lister" type="submit"
						class="btn btn-primary btn-lg btn-block">Lister les utilisateurs</button>
					<button name="offres" type="submit"
						class="btn btn-primary btn-lg btn-block">Liste des offres</button>
					<button name="demandes" type="submit"
						class="btn btn-primary btn-lg btn-block">Liste des demandes</button>
					<button name="newsletter" type="submit"
						class="btn btn-primary btn-lg btn-block">Newsletter</button>
				</form>
			</div>
			<div class="col-md-9">
		<?php
		$mysqli = connect_bdd ();
		if (empty ( $_POST ) && empty ( $_GET ))
			echo "<h2>Page de l'administrateur</h2>";
		
		if (isset ( $_POST ['lister'] ))
			redirect ( "administration.php?list" );
		
		if (isset ( $_POST ['lister'] ) && ! empty ( $_GET ['id'] ))
			redirect ( "administration.php?list" );
		
		if (isset ( $_POST ['offres'] ) || isset ( $_POST ['demandes'])){
			if(isset($_POST ['offres'])){$annonce='offre';}
			if(isset($_POST ['demandes'])) $annonce='demande';
			redirect ( "administration.php?annonces=$annonce" );
		}
		if (isset ( $_POST ['newsletter'] ) || isset ( $_POST ['newsletter']))
			redirect ( "administration.php?newsletter=newsletter" );
			
			
		if (isset ( $_POST ['lister'] ) || isset ( $_GET ['list'] )) {
			echo "<h3><span class=\"label label-primary\">Liste des utilisateurs</span></h3>";
			$query = "SELECT id,nom,prenom,email,GROUP_CONCAT(profils.label SEPARATOR '<br/>') AS label 
										FROM users
										JOIN users_profil ON users.id = users_profil.userId
										JOIN profils ON users_profil.profilId = profils.profilId
                                        GROUP BY users.id";
			
			$users = $mysqli->query ( $query );
			
			echo "<hr/>
					<table class='sortable table table-bordered table-hover table-condensed'>";
			echo "<tr class='info'><th>ID</th>
					<th>Nom</th>
					<th>Prénom</th>
					<th>Email</th>
					<th>Points</th>
					<th>Premium</th>
					<th>Label</th><th></th></tr>";
			
			while ( $row = mysqli_fetch_array ( $users ) ) {
				echo "<tr>";
				echo "<td>" . $row ['id'] . "</td> 
						<td>" .  $row ['nom']  . "</td> 
						<td>" .  $row ['prenom']  . "</td> 
						<td>" . $row ['email'] . "</td> 
						<td>" . nbPointsUser ( $mysqli, $row ['id'] ) . "</td>";
				if (userEstPremium ( $mysqli, $row ['id'] ))
					echo "<td class='btn-success'><span class='glyphicon glyphicon-plus-sign'></span></td>";
				else
					echo "<td class='btn-warning'><span class='glyphicon glyphicon-minus-sign'></span></td>";
				echo "<td>" .  $row ['label']  . "</td> <td>
												<a href='administration.php?id=" . $row ['id'] . "' data-original-title='Modifier' data-toggle='tooltip' ><span class='glyphicon glyphicon-cog'></span></a></td>";
				echo "</tr>";
			}
			echo "</table>";
			
			mysqli_free_result ( $users );
		}
		
		/**
		 * Gestion d'un utilisateur.
		 */
		if (! isset ( $_POST ['lister'] ) && ! empty ( $_GET ['id'] ) && idExiste ( $mysqli, $_GET ['id'] ))
			include ("gestionUsers.php");
		else if (! empty ( $_GET ['id'] ) && ! idExiste ( $mysqli, $_GET ['id'] ))
			redirect ( "administration.php?list" );
		/**
		 * Gestion ini
		 */
		if (isset ( $_POST ['configurerIni'] ))
			redirect ( "administration.php?ini" );
		if (isset ( $_GET ['ini'] )) {
			include ("configIni.php");
		}
		/**
		 * Gestion des offres
		 */
		$today = date('Y-m-d H:m:s');
		if (isset ( $_GET ['annonces'] ) && ( $_GET ['annonces'] == 'offre' || $_GET ['annonces'] == 'demande') ) {
			if(!empty($_GET['annonceId'])&&(!empty($_GET['bool']))){
				cloture_decloture($mysqli,$_GET['annonceId'], $_GET ['annonces'],$_GET['bool']);
			}
			echo "<h3><span class=\"label label-primary\">Liste des ".$_GET['annonces']."s</span></h3>";
			$row= liste_offres_demandes($mysqli,$_GET['annonces'],0,true,0);
			if(empty($row)) echo "Il n'y a aucune ".$_GET['annonces']." est cloturée ou expirée.";
			else{
				$type = $_GET['annonces'];	
				echo "<hr/>
						<table class='sortable table table-bordered table-hover table-condensed'>";
				echo "<tr class='info' style='width:100%;'>
						<th style='width:10%;'>Créateur</th>
						<th style='width:10%;'>Catégorie</th>
						<th style='width:14%;'>Titre</th>
						<th style='width:42%;'>Description</th>
						<th style='width:12%;'>Date de création</th>
						<th style='width:12%;'>Date d'expiration</th>
						<th></th>
					</tr>";
					
				for ($i = 0;$i<sizeof( $row);$i++  ) {
					$annonceId = $row[$i] ['annonceId'] ;
					echo "<tr>";
					echo "<td style='width:10%;'>". $row[$i] ['prenom'] .' '. $row[$i] ['nom'] . "</td>
			 				<td style='width:10%;'>" . $row[$i] ['tagNom']  . "</td>
							<td style='width:14%;'>" . $row[$i] ['titre']  . "</td>
							<td style='width:42%;'>" . $row[$i] ['annonce']  . "</td>
							<td style='width:12%;'>" . $row[$i] ['dateCreation'] . "</td>
							<td style='width:12%;'>" .  $row[$i] ['dateExpiration']  . "</td>";
					if(estCloturee($mysqli,$annonceId , $type) > 0 )
						echo"<td><a href='administration.php?annonces=".$type."&annonceId=".$annonceId."&bool=2' data-original-title='Déloturer' data-toggle='tooltip' ><span class='glyphicon glyphicon-lock'></a></td>";
					else echo "<td><a href='administration.php?annonces=".$type."&annonceId=".$annonceId."&bool=1' data-original-title='Cloturer' data-toggle='tooltip'><i class='icon-unlock'></i></a></td>";
					echo "</tr>";
				}
				echo "</table>";
			}
			
			
			echo "<h3><span class=\"label label-primary\">Liste des ".$_GET['annonces']."s cloturées ou expirées</span></h3>";
			$tab['expiration'] = 1;
			$row= liste_offres_demandes($mysqli,$_GET['annonces'],$tab,true,0);
			if(empty($row)) echo "Il n'y a aucune ".$_GET['annonces']." est cloturée ou expirée.";
			else{
				$type = $_GET['annonces'];
				echo "<hr/>
						<table class='sortable table table-bordered table-hover table-condensed'>";
				echo "<tr class='info'>
						<th>Créateur</th>
						<th>Catégorie</th>
						<th>Titre</th>
						<th>Description</th>
						<th>Date de création</th>
						<th>Date d'expiration</th>
						<th></th>
					</tr>";
				
				for ($i = 0;$i<sizeof( $row);$i++  ) {
					$annonceId = $row[$i] ['annonceId'] ;
					echo "<tr>";
					echo "<td>". $row[$i] ['prenom'] .' '. $row[$i] ['nom'] . "</td>
			 				<td>" . $row[$i] ['tagNom']  . "</td>
							<td>" . $row[$i] ['titre']  . "</td>
							<td>" . $row[$i] ['annonce']  . "</td>
							<td>" . $row[$i] ['dateCreation'] . "</td>
							<td>" .  $row[$i] ['dateExpiration']  . "</td>";
					if(estCloturee($mysqli,$annonceId , $type) > 0 ){
						$niveauCloture = niveau_cloture($mysqli, $row[$i] ['annonceId'],$type);
						$valeurUser = valeurUser($mysqli, $_SESSION['id']);
						if(($niveauCloture >= $valeurUser || $niveauCloture >= 15)&& strtotime($row[$i] ['dateExpiration'])>strtotime($today))
							if((!userEstPremium($mysqli, $row[$i]['id']) && nbOffreDemandeEnCours($mysqli,$row[$i]['id'],$type)< 1) || userEstPremium($mysqli, $row[$i]['id']))
								echo"<td><a href='administration.php?annonces=".$type."&annonceId=".$annonceId."&bool=2' data-original-title='Déloturer' data-toggle='tooltip'><span class='glyphicon glyphicon-lock'></a></td>";
					}else{
						if(strtotime($row[$i] ['dateExpiration'])<strtotime($today)){
							cloture_decloture($mysqli,$annonceId, $_GET ['annonces'],1);
						}
						else{	
							echo "<td><a href='administration.php?annonces=".$type."&annonceId=".$annonceId."&bool=2'><span class='glyphicon glyphicon-lock'></a></td>";
						}
					}
					echo "</tr>";
				}
				echo "</table>";
			}
		}
		$mysqli->close ();
		if (! isset ( $_POST ['offres'] ) && ! empty ( $_GET ['offre'] ))
			include('gestionOffreDemande.php');
		
		if (! isset ( $_POST ['newsletter'] ) && ! empty ( $_GET ['newsletter'] ))
			include('../../docs/newsletter.php');
		?>
		</div>
		</div>
	</div>
</body>
<?php
include ('../footer.php');
?>