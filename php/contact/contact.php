<?php
include ('../header.php');
?>

</br>
</br>
</br>
</br>
</br>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="well well-sm">
				<form class="form-horizontal" action="" method="post">
					<fieldset>
						<legend class="text-center">Contacter un administrateur</legend>

						<!-- Name input-->
						<div class="form-group">
							<?php if(!connected()) echo '<label class="col-md-3 control-label" for="name">Nom</label>';?>
							<div class="col-md-9">
								<input id="name" name="name" 
									<?php  if (connected()) echo'type="hidden"'; else echo'type="text"'; ?>
									placeholder="Votre nom"
									class="form-control"
									value="<?php 
									if(connected()) echo $_SESSION['nom'] .' '. $_SESSION['prenom'];
									elseif(isset($_POST['name'])) { echo $_POST['name'];}?>">
							</div>
						</div>

						<!-- Email input-->
						<div class="form-group">
							<?php if(!connected()) echo '<label class="col-md-3 control-label" for="email">Email</label>';?>
							<div class="col-md-9">
								<input id="email" name="email" 
									<?php  if (connected()) echo'type="hidden"'; else echo'type="text"'; ?>
									placeholder="Votre adresse email" class="form-control"
									value="<?php 
									if(connected()) echo $_SESSION['email'];
									elseif(isset($_POST['email'])) { echo $_POST['email'];}?>">
							</div>
						</div>
						
						<!-- Sujet input-->
						<div class="form-group">
							<label class="col-md-3 control-label" for="sujet">Sujet</label>
							<div class="col-md-9">
								<input id="sujet" name="sujet" type="text"
									placeholder="Le sujet de votre requête" class="form-control"
									value="<?php if(isset($_POST['sujet'])) { echo $_POST['sujet'];}?>">
							</div>
						</div>

						<!-- Message body -->
						<div class="form-group">
							<label class="col-md-3 control-label" for="message">Votre message</label>
							<div class="col-md-9">
								<textarea class="form-control" id="message" name="message"
									rows="5"><?php if(isset($_POST['message'])) echo $_POST['message'];?></textarea>
							</div>
						</div>

						<!-- Form actions -->
						<div class="form-group">
							<div class="col-md-12 text-right">
								<button type="submit" class="btn btn-primary btn-lg"
									name="envoyer">Envoyer</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

	<?php
	$var_validité = true;
	$err_message = "";
	if (isset ( $_POST ['envoyer'] )) {
		if (empty ( $_POST ['name'] )) {
			$err_message .= ' Le champ nom est vide.</br>';
			$var_validité = false;
		}
		if (empty ( $_POST ['email'] )) {
			$err_message .= ' Le champ nom est vide.</br>';
			$var_validité = false;
		}
		if (empty ( $_POST ['sujet'] )) {
			$err_message .= ' Le champ sujet est vide.</br>';
			$var_validité = false;
		}
		if (empty ( $_POST ['message'] )) {
			$err_message .= ' Le champ message est vide.</br>';
			$var_validité = false;
		}
		
		$nom = fixtags ( $_POST ['name'] );
		$email = fixtags ( $_POST ['email'] );
		$sujet = fixtags ($_POST['sujet']);
		$message = fixtags ( $_POST ['message'] );
		
		if (! (filter_var ( $email, FILTER_VALIDATE_EMAIL ))) {
			$err_message .= ' L\'email inscrit est invalide.</br>';
			$var_validité = false;
		}
		
		if ($var_validité) {
			if(connected()) $isUser = 'oui'; else $isUser = 'non';
			
			$titre = 'Demande de contact administrateur';
			$messageMail = '<h1 style="color:RGB(111,153,170)">Quelqu\'un a une question pour vous: </h1>';
			$messageMail .= '<p>Nom-prénom : ' . $nom.'</p>';
			$messageMail .= '<p>Adresse email : ' . $email.'</p>';
			$messageMail .= '<p>Connecté lors de la requête : '.$isUser;
			$messageMail .= '<p>Sujet : '.$sujet .'</p>';
			$messageMail .= '<p>Demande : '.$message.'</p>';
			
			
			if (envoi_email_admin($titre, $messageMail, $email)) {
				redirect ( 'contactOk.php' );
			} 
			else{
				?>
				<div class="alert alert-dismissable alert-warning">
					<div class="row">
						<div class="col-md-2">
							<img src="/images/site/warning.png" alt="attention infos incorrectes" />
						</div>
						<big>Une erreur c'est malheureusement produite lors de l'envoi de la requête.</big>
					</div>
				</div>
			<?php 
			}
		} 
		else{
			?>
			<div class="alert alert-dismissable alert-warning">
				<div class="row">
					<div class="col-md-2">
						<img src="/images/site/warning.png" alt="attention infos incorrectes" />
					</div>
					<big>Veuillez corriger l'(les) erreur(s) suivante(s): </br></big>
					<?php echo $err_message;?>
				</div>
			</div>
		<?php
		} 
	} 
?>

</div>
<?php
include ('../footer.php');
?>