<?php
/**
 * Permet de connaitre le nombre de demandes pour chaque tag
 * @return tableau contenant le tag et le nombre de fois présent
 */
function get_count_tag_site($mysqli,$type){
	$requete = "SELECT at.tagId, count(at.tagId) as nbTagId, t.tagNom FROM annonce_tags at join tags t on t.tagId = at.tagId join annonces a on a.annonceId = at.annonceId where a.type = '".$type."' group by at.tagId";
	$result = $mysqli->query($requete);
	return $result;
}

/**
 * Cette fonction renvoie le nombre total de demande/offre
 * @return unknown
 */
function get_count_total_site($mysqli,$type){
	$requete = "SELECT count(tagId) as nbTagId FROM annonce_tags at join annonces a on a.annonceId = at.annonceId and a.type = '".$type."' ";
	$result = $mysqli->query($requete);
	return $result;
}

/**
 * Permet de calculer un pourcentage et le formater avec 2 décimales
 * @param $nb le premier nombre
 * @param $total le second nombre
 * @return un nombre avec 2 décimale pourcentage du premier nombre par le second
 */
function calcul_pourcentage($nb, $total){
	$tmp = ($nb / $total)*100;
	$pourcentage = number_format ( $tmp, 2, ',', ' ' );
	return $pourcentage;
}

/**
 * Cette fonction renvoie le nombre total de demande/offre pour un utilisateur donné
 * @return unknown
 */
function get_count_total_user($mysqli,$type, $id){
	$requete = "SELECT count(a.annonceId) as nbAnn FROM users_annonce ua join annonces a on a.annonceId = ua.annonceId  WHERE type='".$type."' and userId = $id";
	$result = $mysqli->query($requete);
	return $result;
}

/**
 * Permet de connaitre le nombre de demandes pour chaque tag pour un user
 * @return tableau contenant le tag et le nombre de fois présent
 */
function get_count_tag_user($mysqli,$type, $id){
	$requete = "select userId, tagNom, count(t.tagNom) as nbTag from users u 
					join users_annonce ua on ua.userId = u.id
					join annonces a on ua.annonceId = a.annonceId
					join annonce_tags at on at.annonceId = a.annonceId
					join tags t on t.tagId = at.tagId
					where u.id = $id and a.type = '".$type."' 
					group by t.tagNom
					order by nbTag desc";
	$result = $mysqli->query($requete);
	return $result;
}

/**
 * Permet de retrouver la liste des utilisateurs triés par nombre d'offres/demandes
 * @param $type : offres ou demandes selon cas
 */
function get_best_users($mysqli,$type){
		$requete = "SELECT u.nom, u.prenom, count(ua.userId) as totAnnonces FROM users_annonce ua
					join users u on u.id = ua.userId
					join annonces a on a.annonceId = ua.annonceId
					where a.type = '".$type."' 
					group by userId
					order by count(ua.userId) desc";
	$result = $mysqli->query($requete);
	return $result;
}

/**
 * Permet de savoir le nombres de points reçu par un utilisateur entre deux dates
 * @param $id : l'id de l'utilisateur
 * @param $dateMin : date de début du calcul de points reçus
 * @param $dateMax : date de fin du calcul de points reçus
 */
function get_point_date($mysqli,$id, $dateMin, $dateMax){
	$requete = "SELECT sum(points) as totPts FROM users_points 
				WHERE datePoints BETWEEN '$dateMin' AND '$dateMax' 
				AND userIdRecevant = $id";
	$result = $mysqli->query($requete);
	return $result;
	
}

/**
 * Permet de connaitre le total des points d'un user
 * @param $id : id de l'utilisateur
 */
function get_total_points($mysqli,$id){
	$requete = "SELECT sum(points) as totPts FROM users_points WHERE  userIdRecevant = $id";
	$result = $mysqli->query($requete);
	return $result;
}

/**
 * Permet de connaitre le total pour chaque tags selon un range de code postaux
 * @param $cpMin : code postal min de recherche
 * @param $cpMax : code postal max de recherche
 * @param  $type : type (offres ou demandes)
 * @return tableau contenant le tag + le total du tag
 */
function get_count_tag_CP($mysqli,$cpMin, $cpMax, $type){
	$requete = "select tagNom, count(t.tagNom) as nbTag from users u 
					join users_annonce ua on ua.userId = u.id
					join annonces a on ua.annonceId = a.annonceId
					join annonce_tags at on at.annonceId = a.annonceId
					join tags t on t.tagId = at.tagId
					where u.codePost BETWEEN $cpMin AND $cpMax AND type = '".$type."' 
					group by t.tagNom
					order by nbTag desc";
	$result = $mysqli->query($requete);
	return $result;
	
}

/**
 * Permet de connaitre le nombre total d'offres/demandes selon le CP
 * @param $cpMin CP min de la recherche
 * @param $cpMax CP max de la recherche
 * @param $type offres ou demandes
 * @return le total des offres/demandes selon les CP donnés
 */
function get_count_total_CP($mysqli,$cpMin, $cpMax, $type){
	$result = get_count_tag_CP($mysqli,$cpMin, $cpMax, $type);
	$total = 0;
	while ($row = $result->fetch_array()) {
		// calcul du nombre total
		$total += $row['nbTag'];
	}
	return $total;
}

/**
 * Permet d'obtenir une liste de tout les id des users du site
 */
function get_all_id($mysqli){
	$requete = "SELECT id FROM users";
	$result = $mysqli->query($requete);
	return $result;
}

/**
 * Permet de connaitre le nombre total d'utilisateurs du site
 * @return un tableau contenant le nombre total de users du site
 */
function get_nb_usr($mysqli){
	$requete = "SELECT count(id) as tot FROM users";
	$result = $mysqli->query($requete);
	return $result;
}

/**
 * Permet de connaitre les préférences pour la newsletters
 * @param $type demandes ou offres
 * @return un tableau contenant les données de préférences pour la newsletter
 */
function get_pref_newsl($mysqli,$type){
		$requete = "SELECT t.tagNom, count(un.tagId) as nbTagId
					FROM users_newsletters un
					join tags t on t.tagId = un.tagId 
					WHERE un.typeAnnonce = '".$type."'
					group by un.tagId";
	$result = $mysqli->query($requete);
	return $result;
}

/**
 * Permet de connaitre le nombre total d'offre/demandes choisies pour la newsletter
 * @param $type offres/demandes
 * @return un tableau contenant le total des préférences
 */
function get_count_total_newsletter($mysqli,$type){
	$requete = "SELECT count(tagId) as nbTag from users_newsletters where typeAnnonce = '".$type."'";	
	$result = $mysqli->query($requete);
	return $result;
}
?>