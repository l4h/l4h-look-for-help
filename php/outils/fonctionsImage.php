<?php
/**
 * redimensionnement d'une image
 * @param unknown $index
 * @param unknown $fichier
 * @param int $longueur 
 * @param int $largeur
 * @return boolean
 */
function redimensionner_image($index, $fichier, $longueur, $largeur) {
	$taille = getimagesize ( $fichier );
	if ($_FILES [$index] ['type'] == 'image/png')
		$img_big = imagecreatefrompng ( $fichier );
	else if ($_FILES [$index] ['type'] == 'image/jpeg')
		$img_big = imagecreatefromjpeg ( $fichier );
	else if ($_FILES [$index] ['type'] == 'image/gif')
		$img_big = imagecreatefromgif ( $fichier );
	else {
		echo 'Erreur de format,l\'image n\a pas pu être lue';
		return false;
	}

	$img_new = imagecreate ( $longueur, $largeur );

	// Création de l'image réduite
	$img_petite = imagecreatetruecolor ( $longueur, $largeur ) or $img_petite = imagecreate ( $longueur, $largeur );

	// COPIE DE L'IMAGE REDIMENSIONNEE
	imagecopyresized ( $img_petite, $img_big, 0, 0, 0, 0, $longueur, $largeur, $taille [0], $taille [1] );
	if ($_FILES [$index] ['type'] == 'image/png')
		imagepng ( $img_petite, $fichier );
	if ($_FILES [$index] ['type'] == 'image/jpeg')
		imagejpeg ( $img_petite, $fichier );
	if ($_FILES [$index] ['type'] == 'image/gif')
		imagegif ( $img_petite, $fichier );
}

/**
 * upload d'une image
 * @param unknown $index string représentant le nom de l'upload (pas important)
 * @param unknown $destination desitination de l'image apres upload
 * @param string $extension  extension autorisées de l'image (ex jpg,png,..)
 * @param unknown $id  = 0 
 * @return boolean   true si l'upload a réussit 
 */
function upload($index, $destination, $extension = false, $id) {
	if(empty($id) || $id=0) $id = $_SESSION['id'];
	if (empty ( $_FILES [$index] ) || $_FILES [$index] ['error'] > 0) {
		echo 'Une erreur est survenue lors de l\'upload';
		return false;
	}
	$ext = strtolower ( substr ( strrchr ( $_FILES [$index] ['name'], "." ), 1 ) );

	$largeur = 250;
	$hauteur = 250;

	if ($extension != false && ! in_array ( $ext, $extension ))
		echo 'L\'extension ne correspond pas au type souhaité (jpeg,gif,png)';

	if (file_exists ( $destination . $id . '.jpg' ))
		unlink ($destination . $id . '.jpg' );
	if (file_exists ( $destination . $id . '.jpeg' ))
		unlink ( $destination . $id . '.jpeg' );
	if (file_exists ( $destination . $id . '.png' ))
		unlink ( $destination . $id . '.png' );
	if (file_exists ( $destination . $id . '.gif' ))
		unlink ( $destination . $id . '.gif' );
	if (file_exists($destination))
	
	move_uploaded_file ( $_FILES [$index] ['tmp_name'], $destination . $_FILES [$index] ['name'] );

	redimensionner_image ( "avatar", $destination . $_FILES [$index] ['name'], $largeur, $hauteur );
	return rename ( $destination . $_FILES [$index] ['name'], $destination . $id . '.' . $ext );
}

/**
 * permet dd'affiche l'avatar d'un utilisateur donné
 * @param unknown $id de l'utilisateur
 * @param unknown $lien  de l'avatar sur le serveur
 * @param unknown $emplacement emplacement de l'avateur sur le serveur
 * @param unknown $width  largeur de l'image
 * @param unknown $height hauteur  de l'image
 * @return string return un string , il faut faire un echo de la fonction
 */
function afficher_avatar($id,$lien,$emplacement,$width,$height){
	$avatar = '<img  class="media-object" width="'.$width.'"  height="'.$height.'"  src="'.$lien;
	if (file_exists($emplacement.$id.'.jpg')) $avatar .= $id.'.jpg"';
	else if (file_exists($emplacement.$id.'.jpeg')) $avatar .= $id.'.jpeg"';
	else if (file_exists($emplacement.$id.'.png')) $avatar .= $id.'.png"';
	else if (file_exists($emplacement.$id.'.gif')) $avatar .= $id.'.gif"';
	else $avatar .= 'inconnu.jpg"';
	$avatar .="alt='avatar'>";
	return $avatar;
}
?>