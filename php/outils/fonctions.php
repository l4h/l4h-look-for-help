<?php
/**
 * Fonction allant retourner la racine du serveur.
 * @return un string contenant la racine du serveur
 */
function racine_serveur() {
	return 'http://' . $_SERVER ['HTTP_HOST'];
}
/**
 * Fonction utilisee pour envoyer un meil.
 *
 * @param string $titre
 *        	le titre du mail
 * @param string $message
 *        	le message du mail
 * @param string $destinataire
 *        	le destinataire du mail
 * @return true si le message est correctment envoye, false sinon
 */
function envoi_email($titre, $message, $destinataire) {
	$titre = $titre;
	$headers = "From: \"L4H-Looking for help\"<no-reply@l4h.be>\n";
	$headers .= "Reply-To: no-reply@l4h.be\n";
	$headers .= "Content-Type: text/html; charset=\"UTF-8\"\r\n";
	if (mail ( $destinataire, $titre, $message, $headers ))
		return true;
	else
		return false;
}
/**
 * Fonction allant generer une chaine de caracteres aleatoire composee de lettres et de chiffres.
 *
 * @param int $nbLetters
 *        	la taille de la chaine souhaitee.
 * @return string est le resultat obtenu
 */
function generateRandomString($nbLetters) {
	$randString = '';
	$charUniverse = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	for($i = 0; $i < $nbLetters; $i ++) {
		$randInt = rand ( 0, 61 );
		$randChar = $charUniverse [$randInt];
		$randString .= $randChar;
	}
	return $randString;
}
/**
 * Fonction allant securiser le texte recu en parametre pour qu'il ne contienne pas de caracteres nefastes .
 *
 * @param string $text
 *        	est la chaine de caractere a securiser
 * @return le string securise
 */
function fixtags($text) {
	$text = htmlspecialchars ( $text );
	$text = preg_replace ( "/=/", "=\"\"", $text );
	$text = addslashes ( $text );
	$text = htmlspecialchars ( $text, ENT_QUOTES );
	$text = preg_replace ( "/&quot;/", "&quot;\"", $text );
	$tags = "/&lt;(\/|)(\w*)(\ |)(\w*)([\\\=]*)(?|(\")\"&quot;\"|)(?|(.*)?&quot;(\")|)([\ ]?)(\/|)&gt;/i";
	$replacement = "<$1$2$3$4$5$6$7$8$9$10>";
	$text = preg_replace ( $tags, $replacement, $text );
	$text = preg_replace ( "/=\"\"/", "=", $text );
	return $text;
}
/**
 * Fonction allant forcer la redirection vers l'URL passee en parametre
 * 
 * @param string $url
 *        	est la destination choisie
 */
function redirect($url) {
	if (! headers_sent ()) {
		header ( 'Location: ' . $url );
		exit ();
	} else {
		echo '<script type="text/javascript">';
		echo 'window.location.href="' . $url . '";';
		echo '</script>';
		echo '<noscript>';
		echo '<meta http-equiv="refresh" content="0;url=' . $url . '" />';
		echo '</noscript>';
		exit ();
	}
}
/**
 * Fonction allant chercher le nom du tag a partir d'une annonce donnee.
 * 
 * @param object $mysqli        	
 * @param string $liste
 *        	le type de l'annonce
 * @param int $idListe
 *        	l'id de l'annonce
 * @return string contenant le nom du tag
 */
function recherche_tag_nom($mysqli, $liste, $idListe) {
	$recherche_tag_nom = 'SELECT * FROM tags WHERE tagId = (SELECT tagId from ' . $liste . '_tags where ' . $liste . 'Id=' . $idListe . ')';
	$tag_nom = mysqli_fetch_array ( $mysqli->query ( $recherche_tag_nom ) );
	return $tag_nom ['tagNom'];
}
/**
 * Fonction allant rediriger vers une autre page avec compteur.
 * 
 * @param string $message
 *        	le message a afficher avant de rediriger vers uen autre page
 * @param string $redirection
 *        	l'URL de la destination
 */
function redirectionTime($message, $redirection) {
	echo '<center></br><p class="lead">' . $message . '</p></br></br>';
	?>

<script type="text/javascript">
					// initialise le temps
					var cpt = 3;
					 
					timer = setInterval(function(){
					    if(cpt>0) // si on a pas encore atteint la fin
					    {
					        --cpt; // décrémente le compteur
					        var Crono = document.getElementById("Crono"); // récupère l'id
					        var old_contenu = Crono.firstChild; // stock l'ancien contenu
					        Crono.removeChild(old_contenu); // supprime le contenu
					        var texte = document.createTextNode(cpt); // crée le texte
					        Crono.appendChild(texte); // l'affiche
					    }
					    else // sinon brise la boucle
					    {
					    	document.location.replace('<?php echo $redirection; ?>');
					    }
					}, 1000);
				 
				</script>

<!-- le div ou on affiche le chrono, ne pas le mettre vide -->
<p class="lead">
	Redirection dans <span id="Crono">3</span> secondes.
</p>
</center>

<?php
}

/**
 * Permet d'envoyer un email de contact à l'administrateur
 *
 * @param $titre :
 *        	le titre du mail
 * @param $message :
 *        	le contenu du mail
 * @return boolean : true si l'email est bien envoyé false le cas inverse
 */
function envoi_email_admin($titre, $message, $demandeur) {
	$titre = $titre;
	$destinataire = "admin@l4h.be";
	$headers = "From: \"L4H-Looking for help\"<no-reply@l4h.be>\n";
	$headers .= "Reply-To:" . $demandeur . "\n";
	$headers .= "Content-Type: text/html; charset=\"UTF-8\"\r\n";
	if (mail ( $destinataire, $titre, $message, $headers ))
		return true;
	else
		return false;
}

/**
 * Verification de l'IPN.
 * 
 * @param object $mysqli        	
 * @param int $userId
 *        	l'id de l'utilisateur qui a paye
 * @param string $receiver_email
 *        	l'email de la reception
 * @param string $payment_status
 *        	le statut du payement
 * @param string $prix
 *        	le montant depense
 * @param string $payment_currency
 *        	la devise du payement
 * @return true si l'operation s'est correctement deroulee, false sinon
 */
function verifIPN($mysqli, $userId, $receiver_email, $payment_status, $prix, $payment_currency) {
	$value = true;
	if (! idExiste ( $mysqli, $userId ))
		$value = false;
	if ($receiver_email != 'vendeur@l4h.be')
		$value = false;
	if ($payment_status != 'Completed')
		$value = false;
	if ($payment_currency != 'EUR')
		$value = false;
	if (! ($prix == '8.00' || $prix == '22.00' || $prix == '40.00' || $prix == '75.00'))
		$value = false;
	return $value;
}
/**
 * Fonction allant creer un lien avec l'image de l'index.
 * 
 * @param int $tagId
 *        	l'id du tag
 * @return string contenant le lien de l'emplacement de l'image
 */
function creeLienImageIndex($tagId) {
	$lien = "src=/images/categories/";
	$lien .= $tagId;
	$lien .= '.jpg';
	return $lien;
}
/**
 * Affichage de la date sous un format donne.
 * @param string $date
 */
function afficheDate($date) {
	$date = date_create ( $date );
	return date_format ( $date, 'G\hi \l\e d/m/Y' );
}

?>