<?php
/**
 * Fonction servant a se connecter a la base de donnees. 
 * @return mysqli
 */
function connect_bdd() {
	if ($_SERVER ['HTTP_HOST'] == 'localhost')
		$mysqli = new mysqli ( 'localhost', 'yim2lhvu', 'projet3t', 'lhbeduarle_l4h' );
	else if ($_SERVER ['HTTP_HOST'] == '192.168.0.10')
		$mysqli = new mysqli ( 'localhost', 'yim2lhvu', 'projet3t', 'lhbeduarle_l4h' );
	else if (racine_serveur () == 'http://l4h.zz.vc')
		$mysqli = new mysqli ( 'mysql.hostinger.fr', 'u179363380_l4h', 'projet3t', 'u179363380_l4h' );
	else if (racine_serveur () == 'http://l4h.be' || racine_serveur () == 'http://www.l4h.be')
		$mysqli = new mysqli ( 'mysql51-142.perso', 'lhbeduarle_l4h', 'ZHZB6rEYfSrS', 'lhbeduarle_l4h' );
	else
		$mysqli = new mysqli ( 'mysql.hostinger.fr', 'u712348832_l4h', 'projet3t', 'u712348832_l4h' );
	
	if ($mysqli->connect_error) {
		die ( 'Erreur de connexion (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error );
	}
	$mysqli->set_charset ( "utf8" );
	return $mysqli;
}

/**
 * A utiliser pour mettre en var de session la table user de la base de données
 * 
 * @param unknown $user
 *        	tableau des données users
 */
function fill_var_session($user) {
	$_SESSION ['id'] = $user ['id'];
	$_SESSION ['prenom'] = $user ['prenom'];
	$_SESSION ['nom'] = $user ['nom'];
	$_SESSION ['email'] = $user ['email'];
	$_SESSION ['gsm'] = $user ['gsm'];
	if (! empty ( $user ['rue'] ))
		$_SESSION ['rue'] = $user ['rue'];
	if (! empty ( $user ['numero'] ))
		$_SESSION ['numero'] = $user ['numero'];
	$_SESSION ['ville'] = $user ['ville'];
	$_SESSION ['pays'] = $user ['pays'];
	$_SESSION ['visibiliteAdresse'] = $user ['visibiliteAdresse'];
	$_SESSION ['newsletter'] = $user ['newsletter'];
	$_SESSION ['codePost'] = $user ['codePost'];
	$_SESSION ['dateInscription'] = $user ['dateInscription'];
	$_SESSION ['salt'] = $user ['salt'];
	$_SESSION ['profilId'] = $user ['profilId'];
	$_SESSION ['afficherMesAnnonces'] = null;
}
/**
 * Enregistrement des informations d'un utilisateur.
 * 
 * @param table $user
 *        	le tableau associatif
 */
function register_var_session($user) {
	fill_var_session ( $user );
	redirect ( '/index.php' );
}
/**
 * enregistre les variable de session de l'user
 * 
 * @param
 *        	user c'est un tableau qui contient les données de l'utilisateur
 */
function read_user($user) {
	$id = $user ['id'];
	$prenom = $user ['prenom'];
	$email = $user ['email'];
	$nom = $user ['nom'];
	$rue = $user ['rue'];
	$numero = $user ['numero'];
	$codePost = $user ['codePost'];
	$ville = $user ['ville'];
	$pays = $user ['pays'];
	$gsm = $user ['gsm'];
	$dateInscription = $user ['dateInscription'];
}
?>
