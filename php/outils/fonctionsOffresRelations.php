<?php
function ownAnnonce($mysqli,$annonceId,$userId,$type){
	$requete = "SELECT annonceId from users_annonce where annonceId = $annonceId and userId = $userId";
	return mysqli_num_rows($mysqli->query($requete));
}

function isInListeAttente($mysqli, $annonceId, $type) {
	$stmt = $mysqli->prepare ( "SELECT annonceId FROM annonce_liste_attente WHERE annonceId = (?);" );
	$stmt->bind_param ( "i", $annonceId );
	$stmt->execute ();
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	return $result;
}
function cancelRelation($mysqli, $annonceId, $type) {
	if(!attribue ( $mysqli, $annonceId, $_SESSION['id'], $type )){
		$stmt = $mysqli->prepare ( "DELETE FROM annonce_liste_attente WHERE annonceId = (?) AND userId = " . $_SESSION ['id'] . ";" );
		$stmt->bind_param ( "i", $annonceId );
		$stmt->execute ();
		$stmt->close ();
	}
}
function getTitreAnnonce($mysqli, $annonceId, $type) {
	$stmt = $mysqli->prepare ( "SELECT titre FROM annonces WHERE annonceId = (?);" );
	$stmt->bind_param ( "i", $annonceId );
	$stmt->execute ();
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	return $result;
}
function getMesRelations($mysqli, $type) {
	$stmt = $mysqli->prepare ( "SELECT ala.annonceId, dateEntreeListe,vuCreateur,dateExpiration FROM annonce_liste_attente ala join annonces a on a.annonceId = ala.annonceId WHERE userId = (?) and a.type = (?) and (a.dateCloture is null && dateExpiration > NOW() ) ORDER BY dateEntreeListe desc" );
	$stmt->bind_param ( "is", $_SESSION ['id'],$type );
	$stmt->execute ();
	$stmt->bind_result ( $col1, $col2,$col3,$col4 );
	$result = array ();
	while ( $stmt->fetch () ) {
		array_push ( $result, array (
				$col1,
				$col2,
				$col3,
				$col4 
		) );
	}
	$stmt->close ();
	return $result;
}
function getMesRelationsExpirées($mysqli, $type){
	$stmt = $mysqli->prepare ( "SELECT ala.annonceId, dateEntreeListe,vuCreateur,dateExpiration FROM annonce_liste_attente ala join annonces a on a.annonceId = ala.annonceId WHERE userId = (?) and a.type = (?) and (a.dateCloture is not null || dateExpiration < NOW() ) ORDER BY dateEntreeListe desc" );
	$stmt->bind_param ( "is", $_SESSION ['id'],$type );
	$stmt->execute ();
	$stmt->bind_result ( $col1, $col2,$col3,$col4 );
	$result = array ();
	while ( $stmt->fetch () ) {
		array_push ( $result, array (
		$col1,
		$col2,
		$col3,
		$col4
		) );
	}
	$stmt->close ();
	return $result;
}
function enAttente($mysqli, $annonceId, $userId, $type) {
	$stmt = $mysqli->prepare ( "SELECT COUNT(*) AS attente FROM annonce_liste_attente WHERE annonceId = (?) AND userId = (?) " );
	$stmt->bind_param ( "ii", $annonceId, $userId );
	$stmt->execute ();
	$stmt->bind_result ( $attente );
	$stmt->fetch ();
	$stmt->close ();
	return $attente;
}
function attribue($mysqli, $annonceId, $userId, $type) {
	$stmt = $mysqli->prepare ( "SELECT COUNT(*) AS attribution FROM annonces_attributions WHERE annonceId = (?) AND userId = (?)" );
	$stmt->bind_param ( "ii", $annonceId, $userId );
	$stmt->execute ();
	$stmt->bind_result ( $attribution );
	$stmt->fetch ();
	$stmt->close ();
	return $attribution;
}
function annonceAttribue($mysqli, $annonceId, $type) {
	$stmt = $mysqli->prepare ( "SELECT COUNT(*) AS annonceAttribuee FROM annonces_attributions WHERE annonceId = (?);" );
	$stmt->bind_param ( "i", $annonceId );
	$stmt->execute ();
	$stmt->bind_result ( $annonceAttribuee );
	$stmt->fetch ();
	return $annonceAttribuee;
}
function getAnnonceCreateur($mysqli, $annonceId, $type) {
	$stmt = $mysqli->prepare ( "SELECT CONCAT(users.prenom,' ',UPPER(users.nom)) AS createur FROM users
	JOIN users_annonce ON users.id = users_annonce.userId
	WHERE users_annonce.annonceId = (?);" );
	$stmt->bind_param ( "i", $annonceId );
	$stmt->execute ();
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	return $result;
}
function calculJoursRestants($mysqli, $annonceId, $type) {
	$stmt = $mysqli->prepare ( "SELECT dateExpiration FROM annonces WHERE annonceId=(?);" );
	$stmt->bind_param ( "i", $annonceId );
	$stmt->execute ();
	$stmt->bind_result ( $exp );
	$stmt->fetch ();
	$stmt->close ();
	$rest = (mysqli_fetch_array ( $mysqli->query ( "SELECT  TIMESTAMPDIFF(DAY,NOW(),'" . $exp . "') AS DAY,
        TIMESTAMPDIFF(HOUR,NOW(),'" . $exp . "')-TIMESTAMPDIFF(DAY,NOW(),'" . $exp . "')*24 AS HOUR,
        TIMESTAMPDIFF(MINUTE,NOW(),'" . $exp . "')-TIMESTAMPDIFF(HOUR,NOW(),'" . $exp . "')*60 AS MINUTE;" ) ));
	return $rest ['DAY'] . " jour(s), " . $rest ['HOUR'] . " heure(s) et " . $rest ['MINUTE'] . " minute(s)";
}
function nbReponsesOffresDemandes($mysqli, $userId, $annonceId, $type) {
	$stmt = $mysqli->prepare ( "SELECT COUNT(*) AS nb FROM annonce_liste_attente WHERE annonceId IN(
			SELECT annonceId FROM users_annonce WHERE userId = (?) AND annonceId = (?));" );
	$stmt->bind_param ( "ii", $userId, $annonceId );
	$stmt->execute ();
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	return $result;
}
function getMaxOffreId($mysqli) {
	$max = mysqli_fetch_array ( $mysqli->query ( "SELECT MAX(offreId) AS max FROM users_offre;" ) );
	return $max ['max'];
}


function acceptRelation($mysqli,$type,$annonceId,$choix){
	$mysqli->query("UPDATE annonce_liste_attente SET vuCreateur = false where annonceId = $annonceId ");
	$stmt = $mysqli->prepare ( "INSERT INTO annonces_attributions (annonceId, userId, dateAttribution) VALUES((?),(?),NOW())" );
	$stmt->bind_param ( "ii", $annonceId, $choix );
	$stmt->execute ();
	$stmt->close ();
}
?>