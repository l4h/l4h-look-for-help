<!DOCTYPE html> 
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Calcul d'itinéraire Google Map Api v3</title>
    <link rel="stylesheet" href="jquery/jquery-ui-1.8.12.custom.css" type="text/css" /> 
  </head>
  <style type="text/css">
    body{font-family:Arial;background:#000000;margin:0px;padding:0px;}
    #container{position:relative;width:990px;margin:auto;background:#FFFFFF;padding:20px 0px 20px 0px;}
    #container h1{margin:0px 0px 10px 20px;}
    #container #map{width:700px;height:500px;margin:auto;}
    #container #panel{width:700px;margin:auto;}
    #container #destinationForm{margin:0px 0px 20px 0px;background:#EEEEEE;padding:10px 20px;border:solid 1px #C0C0C0;}
    #container #destinationForm input[type=text]{border:solid 1px #C0C0C0;}
  </style>
  <body>
    <div id="container">
    <?php    
       $adresse = 'avenue+victor+tahon+1160+bruxelles';
		$url = "http://maps.google.com/maps/api/geocode/json?address=".$adresse."&sensor=false";
		$response = file_get_contents($url);
		$response = json_decode($response, true);
	 	if((!empty($response['results']))){
			$lat = $response['results'][0]['geometry']['location']['lat'];
			$long = $response['results'][0]['geometry']['location']['lng'];
	 
			echo "latitude: " . $lat . " longitude: " . $long;
	 	}
		$adresse2='avenue victor tahon';
	?>
	  <?php
        if (!empty($_POST['destination'])) {
              
    	    function getDistance($adresse3,$adresse4){
    	    	$base_url = 'http://maps.googleapis.com/maps/api/directions/xml?sensor=false';
    			$xml = simplexml_load_file("$base_url&origin=$adresse3&destination=$adresse4");
    			$distance = (string)$xml->route->leg->distance->text;
    			$duration = (string)$xml->route->leg->duration->text;
    	    	return $distance;
    	    } 
    	    
    	    	echo 'la distance est de ...'.getDistance($adresse,$_POST['destination']);
    	    
        } 
    	    ?>
	
        <h1>Itinéraire Offreur/Demandeur</h1>
        <div id="destinationForm">
            <form action="#" method="POST" name="direction" id="direction">
                <label>Point de départ :</label>
                <input type="hidden" name="origin" id="origin" value="<?php echo $adresse2;?>">
                <label>Destination :</label>
                <input type="text" name="destination" id="destination">
                <input type="button"  value="Calculer l'itinéraire" onclick="javascript:calculate()" >
            </form>
        </div>
      

        <div id="panel"></div>
        <div id="map">
            <p>Veuillez patienter pendant le chargement de la carte...</p>
        </div>
    </div>
    
    <!-- Include Javascript -->
    <script type="text/javascript" src="jquery/jquery.min.js"></script>
    <script type="text/javascript" src="jquery/jquery-ui-1.8.12.custom.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=fr"></script>
    <script>

    var map;
    var panel;
    var initialize;
    var calculate;
    var direction;

    var lat = "<?php echo $lat;?>";
    var lng =  "<?php echo $long; ?>";
    
    initialize = function(lat,lng){
      var latLng = new google.maps.LatLng(lat, lng); // Correspond au coordonnées de Lille
      var myOptions = {
        zoom      : 14, // Zoom par défaut
        center    : latLng, // Coordonnées de départ de la carte de type latLng 
        mapTypeId : google.maps.MapTypeId.TERRAIN, // Type de carte, différentes valeurs possible HYBRID, ROADMAP, SATELLITE, TERRAIN
        maxZoom   : 20
      };
      
      map      = new google.maps.Map(document.getElementById('map'), myOptions);
      panel    = document.getElementById('panel');
      
      var marker = new google.maps.Marker({
        position : latLng,
        map      : map,
        title    : "Titre de la carte"
        //icon     : "marker_lille.gif" // Chemin de l'image du marqueur pour surcharger celui par défaut
      });
      
      var contentMarker = [
          '<div id="containerTabs">',
          '<div id="tabs">',
          '<ul>',
            '<li><a href="#tab-1"><span>Lorem</span></a></li>',
            '<li><a href="#tab-2"><span>Ipsum</span></a></li>',
            '<li><a href="#tab-3"><span>Dolor</span></a></li>',
          '</ul>',
          '<div id="tab-1">',
            '<h3>Lille</h3><p>Suspendisse quis magna dapibus orci porta varius sed sit amet purus. Ut eu justo dictum elit malesuada facilisis. Proin ipsum ligula, feugiat sed faucibus a, <a href="http://www.google.fr">google</a> sit amet mauris. In sit amet nisi mauris. Aliquam vestibulum quam et ligula pretium suscipit ullamcorper metus accumsan.</p>',
          '</div>',
          '<div id="tab-2">',
           '<h3>Aliquam vestibulum</h3><p>Aliquam vestibulum quam et ligula pretium suscipit ullamcorper metus accumsan.</p>',
          '</div>',
          '<div id="tab-3">',
            '<h3>Pretium suscipit</h3><ul><li>Lorem</li><li>Ipsum</li><li>Dolor</li><li>Amectus</li></ul>',
          '</div>',
          '</div>',
          '</div>'
      ].join('');

      var infoWindow = new google.maps.InfoWindow({
        content  : contentMarker,
        position : latLng
      });
      
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.open(map,marker);
      });
      
      google.maps.event.addListener(infoWindow, 'domready', function(){ // infoWindow est biensûr notre info-bulle
        jQuery("#tabs").tabs();
      });
      
      
      direction = new google.maps.DirectionsRenderer({
        map   : map,
        panel : panel // Dom element pour afficher les instructions d'itinéraire
      });

    };

    calculate = function(){
        origin      = document.getElementById('origin').value; // Le point départ
        destination = document.getElementById('destination').value; // Le point d'arrivé
        if(origin && destination){
            var request = {
                origin      : origin,
                destination : destination,
                travelMode  : google.maps.DirectionsTravelMode.DRIVING // Mode de conduite
            }
            var directionsService = new google.maps.DirectionsService(); // Service de calcul d'itinéraire
            directionsService.route(request, function(response, status){ // Envoie de la requête pour calculer le parcours
                if(status == google.maps.DirectionsStatus.OK){
                    direction.setDirections(response); // Trace l'itinéraire sur la carte et les différentes étapes du parcours
                }
            });
        }
    };
   
    initialize(lat,lng);



    </script>

    
   
  </body>
</html>
