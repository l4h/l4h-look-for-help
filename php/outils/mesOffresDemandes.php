<?php
/**
 * Fonction affichant les annonces de l'utilisateur courant.
 * @param string $type le type d'annonce
 */
function mesOffresDemandes($type) {
	$mysqli = connect_bdd ();
	$id = $_SESSION ['id'];
	$row = liste_mes_offresDemandes ( $mysqli, $type, $id, 1 );
	$today = date ( 'Y-m-d H:m:s' );
	echo "<h3><span class=\"label label-primary\">Liste de mes " . $type . "s en cours</span></h3></br>";
	?>
<table class='sortable table table-bordered table-hover table-condensed'>
		<?php
	if (empty ( $row ))
		echo "Vous n'avez aucune $type en cours.";
	else {
		echo "<tr class='info' style='width:100%;'>
					<th style='width:10%;'>Catégorie</th>
					<th style='width:11%;'>Titre</th>
					<th style='width:28%;'>Description</th>
					<th style='width:13%;'>Date de création</th>
					<th style='width:13%;'>Date d'expiration</th>
					<th style='width:15%;'>Nombre de réponses</th>
					<th></th>
					<th></th>
				</tr>";
		
		for($i = 0; $i < sizeof ( $row ); $i ++) {
			$dateCreation = new DateTime ( $row [$i] ['dateCreation'] );
			$dateExpiration = new DateTime ( $row [$i] ['dateExpiration'] );
			echo "<tr>";
			echo "	<td>" . $row [$i] ['tagNom'] . "</td>
						<td>" . $row [$i] ['titre'] . "</td>
						<td>" . $row [$i] ['annonce'] . "</td>
						<td>" . $dateCreation->format ( 'd-m-Y H:i:s' ) . "</td>
						<td>" . $dateExpiration->format ( 'd-m-Y H:i:s' ) . "</td>";
			$nbRep = nbReponsesOffresDemandes ( $mysqli, $_SESSION ['id'], $row [$i] ['annonceId'], $type );
			if ($nbRep > 0) {
				echo "<td class='alert-success'>" . $nbRep . " ";
				echo '<a href="listeAttente' . $type . '.php?ido=' . $row [$i] ['annonceId'] . '"   data-original-title="Consulter la liste d attente" " data-toggle="tooltip"><span class="glyphicon glyphicon-list"></span></a>';
			} else
				echo "<td class='alert alert-warning'>" . $nbRep . "</td>";
			
			if (annonceAttribue ( $mysqli, $row [$i] ['annonceId'], $type )) {
				echo " <span class='glyphicon glyphicon-check'></span>";
			}
			
			echo "<td><a href='/php/" . $type . "s/modifier$type.php?id=" . $row [$i] ['annonceId'] . "&bool=1' data-original-title='Cloturer' data-toggle='tooltip'><i class='icon-unlock'></i></a></td>";
			echo "<td><a href='/php/" . $type . "s/modifier$type.php?id=" . $row [$i] ['annonceId'] . "' data-original-title='Modifier' data-toggle='tooltip'><span class='glyphicon glyphicon-cog'></span></a></td>";
			echo "</tr>";
			if($row [$i] ['vuReceveur'] < 1) vuAnnonceRelationReceveur($mysqli,$row [$i] ['annonceId'],$_SESSION['id'],$type);
		}
		echo "</table>";
	}
	$historique = liste_mes_offresDemandes ( $mysqli, $type, $id, 0 );
	echo "<h3><span class=\"label label-primary\">Liste de mes " . $type . "s cloturées ou expirées</span></h3>";
	?>
            <table
		class='sortable table table-bordered table-hover table-condensed'>
		<?php
	if (empty ( $historique ))
		echo "</br>Vous n'avez aucune $type cloturée ou expirée.";
	else {
		echo "<tr class='info' style='width:100%;'>
					<th style='width:10%;'>Catégorie</th>
					<th style='width:20%;'>Titre</th>
					<th style='width:44%;'>Description</th>
					<th style='width:13%;'>Date de création</th>
					<th style='width:13%;'>Date d'expiration</th>
					<td></td>
				</tr>";
		
		for($i = 0; $i < sizeof ( $historique ); $i ++) {
			$dateCreation = new DateTime ( $historique [$i] ['dateCreation'] );
			$dateExpiration = new DateTime ( $historique [$i] ['dateExpiration'] );
			echo "<tr>";
			echo "	<td>" . $historique [$i] ['tagNom'] . "</td>
					  	<td>" . $historique [$i] ['titre'] . "</td>
						<td>" . $historique [$i] ['annonce'] . "</td>
						<td>" . $dateCreation->format ( 'd-m-Y H:i:s' ) . "</td>
						<td>" . $dateExpiration->format ( 'd-m-Y H:i:s' ) . "</td>";
			if (estCloturee ( $mysqli, $historique [$i] ['annonceId'], $type ) > 0) {
				if (niveau_cloture ( $mysqli, $historique [$i] ['annonceId'], $type ) >= valeurUser ( $mysqli, $_SESSION ['id'] ) && strtotime ( $historique [$i] ['dateExpiration'] ) > strtotime ( $today )){
					if ((! userEstPremium ( $mysqli, $_SESSION ['id'] ) && nbOffreDemandeEnCours ( $mysqli, $_SESSION ['id'], $type ) < 1) || userEstPremium ( $mysqli, $_SESSION ['id'] ))
						echo "<td><a href='/php/" . $type . "s/modifier$type.php?id=" . $historique [$i] ['annonceId'] . "&bool=2' data-original-title='Décloturer' data-toggle='tooltip'><span class='glyphicon glyphicon-lock'></span></td>";
				}
			} else {
				if (strtotime ( $historique [$i] ['dateExpiration'] ) < strtotime ( $today ))
					cloture_decloture ( $mysqli, $historique [$i] ['annonceId'], $type, 1 );
				else
					echo "<td><a href='/php/" . $type . "s/modifier$type.php?id=" . $historique [$i] ['annonceId'] . "&bool=2' data-original-title='Décloturer' data-toggle='tooltip'><span class='glyphicon glyphicon-lock'></span></a></td>";
			}
			if($historique [$i] ['vuReceveur'] < 1) vuAnnonceRelationReceveur($mysqli,$historique [$i] ['annonceId'],$_SESSION['id'],$type);
			echo "</tr>";
		}
		echo "</table>";
		
		
		// Noter une personne
		
		echo "<h3><span class=\"label label-primary\">Utilisateur(s) à noter</span></h3></br>";
		
		$nbAnnonces = count ( $historique );
		if($nbAnnonces > 0){
			echo "<table class='sortable table table-bordered table-hover table-condensed'>
				<tr class='info' style='width:100%;'>";
			
			echo "<tr><th>Titre de votre " . $type . "</th>";
			echo "<th>Accomplie par</th>";
			echo "<td>Note</td>
			</tr>";
			
			echo "<tr>";
		}
		for($i = 0; $i < $nbAnnonces; $i ++) {
			$annonceId = $historique [$i] ['annonceId'];
			$anonceTitre = $historique [$i] ['titre'];
			
			$annonceRealisee = annonceRealisee ( $mysqli, $annonceId, $type );
			$annonceRealiseePar = annonceRealiseePar ( $mysqli, $type, $annonceId );
			$userNote = userNote ( $mysqli, $annonceId, $type, $annonceRealiseePar );
			
			if ($annonceRealisee && ! $userNote) {
				if($type == 'demande') $placeholder = "Merci de laisser un bref commentaire sur le prestataire.";
				if($type == 'offre') $placeholder = "Merci de laisser un bref commentaire sur l'utilisateur presté.";
				echo "<td>" . $anonceTitre . "</td>";
				$nomPrenom = getprenomNom ( $mysqli, $annonceRealiseePar );
				echo "<td>" . $nomPrenom . "</td>";?>
				<td><button class="btn btn-warning btn-xs"  data-toggle="modal" data-target="#modalCommenter<?php echo $annonceRealiseePar; ?>">Donner votre avis sur le prestataire</button>
				<?php 			
								//
				//Modal des commentaires sur l'user
				//
				echo'<div class="modal fade" id="modalCommenter' . $annonceRealiseePar . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-envelope"></span> Commenter '.$nomPrenom.'</h4>
					      </div>
					      <div class="modal-body">
							
								<form id="formTexte" class="form-horizontal" role="form" method="post" accept-charset="UTF-8" >	
					        	Titre:  '.$anonceTitre .'</br>	
								<textarea style="resize:vertical" rows = "10" cols = "65" name = "texte' . $annonceRealiseePar . '"  placeholder="'.$placeholder.'" ></textarea>
									
					      </div>
				      <div class="modal-footer">
				        <form class="form-horizontal" role="form" method="post" accept-charset="UTF-8">
							<button name="' . $annonceId . 'positif" type="submit" data-toggle="modal" data-target="#modalCommentaire".$annonceRealiseePar." class="btn btn-success btn-xs">Positif</button>
							<button name="' . $annonceId . 'neutre" type="submit" class="btn btn-default btn-xs">Neutre</button>
							<button name="' . $annonceId . 'negatif" type="submit" class="btn btn-danger btn-xs">Negatif</button>
							</form>
				      </div>
				    </div>
				  </div>
				</div>';
				//
				//fin du modal
				//
					
					
					
					
					
							echo"</td></tr>";
			}
			
		}
		if($nbAnnonces > 0 ) echo "</table>";
		else echo "Il n'y a pas d'utilisateur à noter pour le moment.";
		
		for($j = 0; $j < $nbAnnonces; $j ++) {
			if (! empty ( $_POST )) {
				foreach ( $_POST as $key => $entry ) {
					if (strstr ( $key, 'positif' ) || strstr ( $key, 'neutre' ) || strstr ( $key, 'negatif' )) {
						$annonceId = intval ( filter_var ( $key, FILTER_SANITIZE_NUMBER_INT ) );
						$note = preg_replace ( '/[0-9 ]*+/', '', $key );
					}
				}
				switch ($note) {
					case 'positif' :
						$note_num = 1;
						break;
					case 'neutre' :
						$note_num = 0;
						break;
					case 'negatif' :
						$note_num = - 1;
						break;
				}
				$annonceRealiseePar = annonceRealiseePar ( $mysqli, $type, $annonceId );
				$commentaire = null;
				if(!empty($_POST['texte'.$annonceRealiseePar])){
					$commentaire = $_POST['texte'.$annonceRealiseePar];
					commenterUser($mysqli, $_SESSION['id'],  $annonceRealiseePar,$annonceId, $type, $commentaire,$note_num);
				}
				noterUser ( $mysqli, $annonceId, $type, $_SESSION ['id'], $annonceRealiseePar, $note_num );
				if($type == 'offre') $url = "mesOffres.php";
				else $url = "mesDemandes.php";
				redirect ( $url );
			}
		}
	}
	?>       
        
<?php
	
	$mysqli->close ();
}
?>