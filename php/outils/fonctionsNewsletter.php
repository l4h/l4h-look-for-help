<?php

/**
 * On cherche le nombre de  tagId de préférence d'un utilisateur pour un type d'annonce donné 
 * @param unknown $mysqli
 * @param int $userId id de l'user concerné
 * @param string $type string offre ou demande
 * @return un nombre
 */
function nbCatSelected($mysqli,$userId,$type){
	return mysqli_num_rows($mysqli->query("SELECT tagId from users_newsletters where userId = $userId and typeAnnonce = '".$type."'"));
}

/**
 * on renvoie l'ensemble des tagId que l'utilisateur à pour préférence dans un type d'annonce donné (offre ou demande)
 * @param unknown $mysqli
 * @param int $userId
 * @param string $type = offre ou demande
 * @return tableau des tagId
 */
function randCategorieSelected($mysqli,$userId,$type){
	$requete = "SELECT tagId from users_newsletters where userId = $userId and typeAnnonce = '".$type."'";
	$tab_cat = fetch($mysqli->query($requete));
	if(empty($tab_cat)) return 0;
	else return $tab_cat;
}

/**
 * On recherche les tagNom avec les tagId dont l'utilisateur à pour préférence en fonction du type demandé
 * @param unknown $mysqli
 * @param int $userId id de l'user concerné
 * @param string $type string offre ou demande
 * @return tableau contenant le tagId,tagNom
 */
function selectedCatNewsletter($mysqli,$userId,$type){
	$requete = "SELECT un.tagId,tagNom from users_newsletters un join tags t on t.tagId=un.tagId where un.userId= ".$userId." and un.typeAnnonce = '".$type."'";
	return fetch($mysqli->query($requete));
}

/**
 * on recherche les catégorie non préférées d'un user selon le type donné
 * @param unknown $mysqli
 * @param int $userId id de l'user concerné
 * @param string $type string offre ou demande
 * @return tableau
 */
function notSelectCatNewsletter($mysqli,$userId,$type){
	$requete = "SELECT * from tags where  tagId NOT IN (SELECT un.tagId from users_newsletters un join tags t on t.tagId=un.tagId where un.userId= ".$userId." and un.typeAnnonce = '".$type."' )";
	return fetch($mysqli->query($requete));
}

/**
 * On veut conaitre le nombre de catégorie qui existe
 * @param unknown $mysqli
 * @return int nombre
 */
function nbCategorie($mysqli){
	$requete = "SELECT count(*)as nbCategorie FROM tags";
	$nb = mysqli_fetch_array($mysqli->query($requete));
	return $nb['nbCategorie'];
}

/**
 * on regarde si le tagId donné existe dans la table des users_newsletters pour un utilisateur donné et un type donné
 * @param unknown $mysqli
 * @param int $tagId
 * @param int $userId user donné
 * @param int nombre
 */
function existeCatNewsletter($mysqli,$tagId,$userId,$type){
	$requete = "SELECT * from users_newsletters where userId = $userId and tagId = '$tagId' and typeAnnonce = '".$type."' ";
	return mysqli_num_rows($mysqli->query($requete));
}

/**
 * on insère une nouvelle catégorie de préférence dans la newsletter
 * @param unknown $mysqli
 * @param int $tagId de la catégorie à insérer
 * @param int $userId de l'user concerné
 * @param string $type pour une offre ou une demande
 */
function insertCatNewsletter($mysqli,$tagId,$userId,$type){
	if(!existeCatNewsletter($mysqli,$tagId,$userId,$type)){
		$requete = "INSERT INTO users_newsletters (userId,tagId,typeAnnonce) VALUES ('$userId','$tagId','$type')";
		$mysqli->query($requete);
	}	
}

/**
 * suppression d'une catégorie de préférence 
 * @param unknown $mysqli
 * @param int $tagId de la catégorie concernée
 * @param int $userId de l'user concerné
 * @param string $type pour une offre ou une demande
 */
function deleteCatNewsletter($mysqli,$tagId,$userId,$type){
	if(existeCatNewsletter($mysqli,$tagId,$userId,$type)){
		$requete = "DELETE FROM users_newsletters where userId = $userId and tagId='$tagId' and typeAnnonce = '".$type."'";
		$mysqli->query($requete);
	}
}

/**
 * mise à jour du choix de recevoir une newsletter ou non
 * @param unknown $mysqli
 * @param int $id de l'user concerné
 * @param boolean $bool 1 = oui 0 = non 
 */
function maj_newsletter($mysqli,$id,$bool){
	$mysqli->query( " UPDATE users SET newsletter = '$bool' WHERE id='$id'");
	$_SESSION['newsletter'] = $bool;
}

/**
 * Mise à jour de la dernière date d'envoi de la newsletter
 * @param unknown $mysqli
 * @param int $nbmail le nombre de newsletters envoyées
 */
function updateDateNewsletter($mysqli,$nbmail){
	$requete = "INSERT INTO dates_newsletters (date,nbNewsletters) VALUES (NOW(),".$nbmail.")";
	$mysqli->query($requete);
}

/**
 * affiche la date de la dernière newsletter
 * @param unknown $mysqli
 */
function lastNewsletter($mysqli){
	$requete = "SELECT * from dates_newsletters where date =(SELECT MAX(date) as date  from dates_newsletters)";
	return mysqli_fetch_array($mysqli->query($requete));
}



?>