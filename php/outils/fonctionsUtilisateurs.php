<?php
/**
 * Fonction retournant vrai si une session est ouverte.
 * @return boolean l'etat de la connexion
 */
function connected() {
	return (! empty ( $_SESSION ['id'] ) && ! empty ( $_SESSION ['nom'] ) && ! empty ( $_SESSION ['prenom'] )  && ! empty ( $_SESSION ['gsm'] ) && ! empty ( $_SESSION ['email'] ) && ! empty ( $_SESSION ['ville'] ) && ! empty ( $_SESSION ['pays'] ) && ! empty ( $_SESSION ['codePost'] )  && ! empty ( $_SESSION ['dateInscription'] ));
}
/**
 * Fonction allant retourner la valeur en fonction de l'id d'un utilisateur donne.
 * @param object $mysqli
 * @param int $userId l'id de l'utilisateur
 * @return la valeur entiere d'un utilisateur donnee
 */
function valeurUser($mysqli,$userId){
	$requete = "SELECT valeur from profils where profilId = (SELECT profilId from users_profil where userId = $userId)";
	$valeur = mysqli_fetch_array($mysqli->query($requete));
	return $valeur['valeur'];
}

/**
 * permet d'insérer un code d'activation dans la bbd + retourner l'id de l'utilisateur qui a été insérer dans activations
 * @param string $codeActivation
 * @param string $email
 * @return int $id l'id de l'utilisateur cree
 */
function insert_bdd_activation($codeActivation, $email) {
	$mysqli = connect_bdd ();
	$user = mysqli_fetch_array ( $mysqli->query ( "SELECT * from users where email='$email'" ) );
	$id = $user ['id'];
	$mysqli->query ( "INSERT INTO activations (userId,activationCode) VALUES ('$id','$codeActivation')" );
	$mysqli->close ();
	return $id;
}
/**
 * Fonction allant determiner si la session en cours est celle d'un administrateur.
 * @return true si la session en cours est celle d'un admin, false sinon
 */
function admin() {
	if(connected()) return ($_SESSION ['profilId'] == 1) || ($_SESSION ['profilId'] == 7);
	else return 0;
}
/**
 * Fonction allant verifier si le mot de passe entre par l'utilisateur correspond a son mot de passe crypte. 
 * @param string $pwd le mot de passe ne clair
 * @param table $user le tableau associatif contenant les informations d'un utilisateur
 * @return true si le mot de passe correspond, false sinon
 */
function decrypter_pwd_salt($pwd, $user) {
	return ($user ['pwd'] == crypt ( $pwd, $user ['salt'] ));
}
/**
 * Fonction allant determiner si un utilisateur est premium.
 * @param object $mysqli
 * @param int $id l'id de l'utilisateur
 * @return true si l'utilisateur est premium, false sinon
 */
function userEstPremium($mysqli, $id) {
	$user = (mysqli_fetch_array ( $mysqli->query ( 'SELECT NOW() < dateDebutPremium + INTERVAL (SELECT nbJoursPremium FROM users WHERE id=' . $id . ') DAY AS premium FROM users WHERE id=' . $id . ';' ) ));
	return $user ['premium'];
}
/**
 * Fonction allant verifier pour un utilisateur donne si ce dernier est administrateur.
 * @param object $mysqli
 * @param int $idUser l'id de l'utilisateur
 * @return true si l'utilisateur est un administrateur, false sinon
 */
function userAdmin($mysqli, $idUser) {
	$admin = (mysqli_fetch_array ( $mysqli->query ( 'SELECT SUM(profilId IN (1,7))AS admin FROM users_profil WHERE userId='.$idUser)));
	return $admin ['admin'];
}
/**
 * Fonction allant determiner si un utilisateur donne est banni du site.
 * @param object $mysqli
 * @param int $idUser l'id de l'utilisateur
 * @return true si l'utilisateur donne est banni, false sinon
 */
function userBanni($mysqli, $idUser) {
	$banni = (mysqli_fetch_array ( $mysqli->query ( 'SELECT SUM(profilId=6)AS ban FROM users_profil WHERE userId='.$idUser)));
	return $banni ['ban'];
}
/**
 * Fonction allant determiner le nombre de jours restant pour qu'un utilisateur ne soit plus premium.
 * @param object $mysqli
 * @param int $id l'id de l'utilisateur
 * @return le nombre de jours restant
 */
function nbJoursRestants($mysqli, $id) {
	$jours = (mysqli_fetch_array ( $mysqli->query ( 'SELECT DATEDIFF((SELECT dateDebutPremium + INTERVAL (SELECT nbJoursPremium FROM users WHERE id=' . $id . ') DAY FROM users WHERE id=' . $id . '), (SELECT NOW())) AS jours' ) ));
	return $jours ['jours'];
}
/**
 * Fonction allant verifier si un id existe dans la base de donnees. 
 * @param object $mysqli
 * @param int $id l'id a verifier
 * @return true si l'id existe, false sinon
 */
function idExiste($mysqli, $id) {
	if (! ($stmt = $mysqli->prepare ( "SELECT COUNT(id) FROM users WHERE id = (?)" )))
		echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
	if (! $stmt->bind_param ( "i", $id ))
		echo "Echec lors du liage des paramètres : (" . $stmt->errno . ") " . $stmt->error;
	if (! $stmt->execute ())
		echo "Echec lors de l'exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	return $result;
}
/**
 * Fonction allant retourner le label pour un utilisateur donne.
 * @param object $mysqli
 * @param int $id l'id de l'utilisateur 
 * @return le string contenant le label
 */
function getMonLabel($mysqli, $id) {
	$label = (mysqli_fetch_array ( $mysqli->query ( 'SELECT label FROM users_profil JOIN profils ON users_profil.profilId = profils.profilId WHERE userId=' . $id ) ));
	return $label ['label'];
}
/**
 * Fonction retournant la valeur du profil d'un utilisateur.
 * @param object $mysqli
 * @param int $idUser l'id de l'utilisateur
 * @return la valeur de l'utilisateur
 */
function getValueUser($mysqli, $idUser) {
	$value = mysqli_fetch_array ( $mysqli->query ( "SELECT COUNT(valeur<30) AS valeur FROM profils JOIN users_profil ON profils.profilId = users_profil.profilId WHERE users_profil.userId = ".$idUser ) );
	return $value['valeur'];
}
/**
 * Fonction allant verifier si un mail existe dans la base de donnees. 
 * @param object $mysqli
 * @param string $email 
 * @return true si l'email existe, false sinon
 */
function emailExiste($mysqli, $email) {
	if (! ($stmt = $mysqli->prepare ( "SELECT COUNT(id) FROM users WHERE email = ('?')" )))
		echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
	if (! $stmt->bind_param ( "s", $email ))
		echo "Echec lors du liage des paramètres : (" . $stmt->errno . ") " . $stmt->error;
	if (! $stmt->execute ())
		echo "Echec lors de l'exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	return $result;
}
/**
 * Fonction allant retourner le nombre de points d'un utilisateur.
 * @param object $mysqli
 * @param int $idUser
 * @return le nombre de points d'un utilisateur donne
 */
function nbPointsUser($mysqli, $idUser) {
	$points = mysqli_fetch_array ( $mysqli->query ( "SELECT SUM(points) AS points FROM users_points WHERE userIdRecevant=" . $idUser ) );
	if(empty($points['points'])) $points['points']=0;
	return $points ['points'];
}
/**
 * Fonction permettant de recuperer un tableau associatif a parti d'un resultat d'une requete preparee.
 * @param object $result le resultat de la requete ayant plusieurs colonne
 * @return tableau associatif dont le nom des champs correspond a ceux de la requete preparee
 */
function fetch($result) {
	$array = array ();
	if ($result instanceof mysqli_stmt) {
		$result->store_result ();
		$variables = array ();
		$data = array ();
		$meta = $result->result_metadata ();

		while ( $field = $meta->fetch_field () )
			$variables [] = &$data [$field->name];
		call_user_func_array ( array (
		$result,
		'bind_result'
				), $variables );

		$i = 0;
		while ( $result->fetch () ) {
			$array [$i] = array ();
			foreach ( $data as $k => $v )
				$array [$i] [$k] = $v;
			$i ++;
		}
	} elseif ($result instanceof mysqli_result) {
		while ( $row = $result->fetch_assoc () )
			$array [] = $row;
	}
	return $array;
}
/**
 * Fonction allant retourner le mot de passe d'un utilisateur donne.
 * @param object $mysqli
 * @param int $id l'id de l'utilisateur 
 * @return le string contenant le mot de passe crypte
 */
function getPwdUser($mysqli, $id) {
	$pwd = mysqli_fetch_array ( $mysqli->query ( "SELECT pwd FROM users WHERE id=" . $id ) );
	return $pwd ['pwd'];
}
/**
 * Fonction allant retourner la concatenantion entre le prenom et le nom d'un utilisateur donne.
 * @param object $mysqli
 * @param int $id l'id d'un utilisateur donne 
 * @return le string contenant le nom et le prenom d'un utilisateur
 */
function getPrenomNom($mysqli, $id) {
	$np = mysqli_fetch_array ( $mysqli->query ( "SELECT CONCAT(prenom,' ', UPPER(nom)) as res FROM users WHERE id=$id ") );
	return $np ['res'];
}
/**
 * Fonction allant retourner le nombre de jours en tant que premium que l'utilisateur a commande.
 * @param object $mysqli
 * @param int $id
 * @return le nombre de jours commande par l'utilisateur
 */
function getNbJoursPremium($mysqli, $id) {
	$nbJours = mysqli_fetch_array ( $mysqli->query ( "SELECT nbJoursPremium FROM users WHERE id=" . $id ) );
	return $nbJours['nbJoursPremium'];
}
/**
 * Fonction allant calculer le temps restant pour un compte premium.
 * @param object $mysqli
 * @return string contenant le temps restant
 */
function afficheNbJoursPremiumRestant($mysqli){
	$user = (mysqli_fetch_array ( $mysqli->query("SELECT dateDebutPremium,nbJoursPremium from users where id = ".$_SESSION['id'])));
	$date = $user['dateDebutPremium'];
	$nbJours = $user['nbJoursPremium'];
	
	$date = date('Y-m-d H:i:s', strtotime($date."+".$user['nbJoursPremium']." days"));
	
	
	$rest = (mysqli_fetch_array ( $mysqli->query ( "SELECT  TIMESTAMPDIFF(DAY,NOW(),'" . $date . "') AS DAY,
        TIMESTAMPDIFF(HOUR,NOW(),'" . $date . "')-TIMESTAMPDIFF(DAY,NOW(),'" . $date . "')*24 AS HOUR,
        TIMESTAMPDIFF(MINUTE,NOW(),'" . $date . "')-TIMESTAMPDIFF(HOUR,NOW(),'" . $date . "')*60 AS MINUTE;" ) ));
	return $rest ['DAY'] . " jour(s), " . $rest ['HOUR'] . " heure(s) et " . $rest ['MINUTE'] . " minute(s)";
}
?>
