<?php
const TITRE_VIDE = 1;
const DESCRIPTION_VIDE = 2;
const CATEGORIE_DEMANDE = 3;
const CATEGORIE_OFFRE = 4;
const DATE_EXPIRATION_VIDE = 5;
const HEURE_EXPIRATION_VIDE = 6;
const DATE_EXPIRATION_NON_VALIDE = 7;
const DELAI_14_JOURS = 8;
const CREATION_ANNONCE_PAS_POSSIBLE = 11;

/*
 * Constante pour Android
 */
const ERROR = 9;
const INSERTED = 21;
const ANSWERED = 22;
const OK = 23;


$tab_messErreurs = array(
		TITRE_VIDE => 'Le champ titre est vide.',
		DESCRIPTION_VIDE => 'Le champ description est vide.',
		CATEGORIE_DEMANDE => 'Vous n\'avez pas choisi le type de demande.',
		CATEGORIE_OFFRE => 'Vous n\'avez pas choisi le type d\'offre.',
		DATE_EXPIRATION_VIDE => 'Vous n\'avez pas choisi de date d\'expiration',
		HEURE_EXPIRATION_VIDE => 'Vous n\'avez pas choisi d\'heure d\'expiration',
		DATE_EXPIRATION_NON_VALIDE => 'La date d\'expiration n\'est pas valide. ',
		DELAI_14_JOURS => 'Le délai doit être de 14 jours max. ',
		CREATION_ANNONCE_PAS_POSSIBLE => 'Seuil d\'annonces atteint, passez premium pour l\'augmenter', 
		ERROR => 'Erreur inconnue',
		INSERTED => 'Annonce envoyée',
		ANSWERED => 'Réponse bien prise en compte',
		OK => 'OK'
);
?>
