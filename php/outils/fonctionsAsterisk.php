<?php
/**
 * Fonction allant servir pour se connecter a la base de donnees pour Asterisk
 * @return mysqli
 */
function connect_bdd_asterisk() {
	if (pingServer ()) {
		$mysqli = new mysqli ( 'antonibra.ddns.net', 'asterisk_mysql', 'projet3t', 'asterisk_schema' );
		return $mysqli;
	} else
		return 0;
}
/**
 * Fonction allant ajouter un utilisateur a la base de donnees d'Asterisk
 *
 * @param object $mysqli        	
 * @param int $id
 *        	l'id de l'utilisateur
 * @param string $prenom
 *        	le prenom de l'utilisateur
 * @param string $nom
 *        	le nom de l'utilisateur
 * @param string $pwd
 *        	le mot de passe de l'utilisateur
 * @param string $email
 *        	le mail de l'utilisateur
 */
function ajoutAsterisk($mysqli, $id, $prenom, $nom, $pwd, $email) {
	$mysqli = connect_bdd_asterisk ();
	if ($mysqli) {
		$mysqli->set_charset ( "utf8" );
		
		// Asterisk utilise le md5 par defaut
		$pwd = md5 ( $id . ":asterisk:" . $pwd );
		
		$stmt = $mysqli->prepare ( "INSERT INTO users (id, prenom, nom, pwd, email, dateAjout)
			VALUES ((?),(?),(?),(?),(?),NOW())" );
		$stmt->bind_param ( "issss", $id, $prenom, $nom, $pwd, $email );
		$stmt->execute ();
		
		$mysqli->close ();
	}
}
/**
 * Fonction allant verifier si le serveur Asterisk est disponible.
 * @return true si le serveur est en ligne, false sinon
 */
function pingServer() {
	$result = 0;
	$host = 'antonibra.ddns.net';
	$port = 3306;
	$waitTimeoutInSeconds = 1;
	if ($fp = fsockopen ( $host, $port, $errCode, $errStr, $waitTimeoutInSeconds )) {
		$result = 1;
	} else {
		$result = 0;
	}
	fclose ( $fp );
	return $result;
}
?>