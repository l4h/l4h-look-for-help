<?php
/**
 * Fonction allant modifier une annonce.
 * @param int $annonceId l'id de l'annonce 
 * @param string $type le type d'annonce
 * @param table $tab_messErreurs est le tableau contenant les messages d'erreurs
 */
function modifierOffreDemande($annonceId,$type,$tab_messErreurs){
	$mysqli = connect_bdd();
	?>
	<div class="container fluid">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
	<?php 
	$err_message = array();
	$var_validite = true;
	$id = $_SESSION['id'];
	
	$dateExpirationInitiale = new DateTime (get_date_expi($annonceId, $type));
	$annonce = get_titre_descr_offre_demande($annonceId,$type);
	
	$dateCreation =  new DateTime ($annonce['dateCreation']);
	$dateCreation = $dateCreation->format( "Y-m-d H:i:s");
	
	$dateDepartTimestamp = strtotime($dateCreation);
	$date_limit = date("Y-m-d H:i:s", strtotime("+14 days", $dateDepartTimestamp ));
	
	$heureExpirationInitiale = $dateExpirationInitiale->format('H:i:s');
	$dateExpirationInitiale =  $dateExpirationInitiale->format( "Y-m-d H:i:s");
	

	$tempo = get_type($mysqli,$annonceId);
	$type_bdd = $tempo['tagNom'];
	$type_bddId =  $tempo['tagId'];
	$plusieurs_demandes = true;
	$today = date("Y-m-d H:i:s");
	if (isset ( $_POST ['modifAnnonce'] )) {

		$var_validite *= verifEmptyPostAnnonceCreation($type,$_POST,$err_message);

		if($var_validite) $dateExpiration = $_POST['expiration'].' '.$_POST['heure'];
		else{
			if(!isset($_POST['expiration']) && !isset($_POST['heure']))$dateExpiration  =  $date_limit;
			$dateExpiration = $_POST['expiration'].' '.$_POST['heure'];
		}	
		$var_validite *= compareDateAnnonceCreation($mysqli,$dateCreation,$dateExpiration,$date_limit,$err_message);

		
		// si validité OK
		if ($var_validite){
			$titre = $_POST ['titre'];
			$tag = $_POST ['tag'];
			$description = $_POST ['description'];
			updateAnnonce($mysqli,$type,$description,$titre,$today,$dateExpiration,$tag,$annonceId);

			if($type == 'offre') $url = 'Offre';
			else $url = 'Demande';
			$messageSucces = "Votre ".$type." a bien été modifiée.";
			$redirection = "/php/".$type."s/mes".$url."s.php";
			echo '</br>'.redirectionTime($messageSucces,$redirection).'</br>';			
		}
	}
	if (!$var_validite) {
	
	
	
		
		echo'<div class="alert alert-dismissable alert-warning hidden-print">
				<a href="#" class="close" data-dismiss="alert">&times;</a>';
		echo '<h3><b>Erreur</b></h3><center><p class="lead">';
		echo '';
		foreach ($err_message as $msg) echo $tab_messErreurs[$msg].'</br>';
		echo'</p></center>
		</div>';
	
	
	
	}
	if (!$var_validite || !isset($_POST['modifAnnonce'])){
?>
	<!--  FORMULAIRE HTML -->
		</div>
	</div>
</div>
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
						<h1>Modifier votre <?php echo $type;?></h1>
									
						<FORM METHOD="POST" ACTION="#">
									
							<label for="titre">Titre</label>
							<input type="text" name="titre" id="titre" value="<?php echo $annonce['titre'];?>"/></br>
							
							<label for="titre">Date d'expiration</label>
					<?php 
								if(!userEstPremium($mysqli,$id)){
									
					?>
								<input type="date" name="expiration" <?php echo 'value='. $dateExpirationInitiale; ?> max=<?php echo $date_limit; ?> min=<?php echo $today; ?> /></br></br></br>
								
					<?php 		
								}
								else{
					?> 
									<input type="date" name="expiration" <?php echo 'value='.$dateExpirationInitiale; ?> min=<?php echo $today; ?> /></br></br></br>
					<?php 
								}
					?>
								<label for="heure">Heure d'expiration</label> <input type="time" name="heure" value="<?php echo $heureExpirationInitiale;?>"/></br></br>
								<!-- Type de la demande -->
								<label for="type">Type</label>
								<SELECT name="tag" size="1">
								<?php 
									echo"<OPTION value = '$type_bddId'> $type_bdd";	
								?>
					<?php 
								$query = "SELECT * from tags ";
								$mysqli->query("SET NAMES utf8");
								if ($result = $mysqli->query($query)) {
									while ($row = $result->fetch_assoc()) {
											$tag = $row['tagId'];
											if($row['tagNom'] != $type_bdd) echo "<OPTION value = $tag >  $row[tagNom] ";
									}
									$result->free();
								}?>
								</SELECT></br>
								
								<!-- Description de la demande -->
								<legend>Description</legend>
								<textarea cols="80" rows="8" id="description" name="description"><?php echo $annonce['annonce'];?></textarea></br>
									
									
								<!-- Bouton envoi -->
								</br><input id="modifAnnonce" tabindex=4 type="submit" class="btn btn-primary" value="Modifier la demande" name="modifAnnonce"></br></br></br>
								</FORM>
					<?php
	}							
	
							
		?>
				</div>
			</div>
<?php
$mysqli->close();
}?>
