<?php
/**
 * Affichage d'un captcha
 */
function affiche_captcha() {
	require_once ('recaptchalib.php');
	if (racine_serveur() == 'http://www.l4h.be') $publickey = '6LeakP4SAAAAAKMO1PbteRvI81Zf0uiQ8R0wZig3';
	else if (racine_serveur() == 'http://l4h.zz.vc') $publickey = "6Le2SP0SAAAAAFq92GhIM0Susii6IPnGPc1bamzV";
	else if  (racine_serveur() == 'localhost') $publickey = "6LfXS_0SAAAAALE-_wanpJIP-QWuZUdGbTOmPoPf";
	else $publickey = "6LcX9PwSAAAAAF0_JETvXPZgTvxUWo7dzqj8gngB";
	echo recaptcha_get_html ( $publickey );
}
/**
 * Verification d'un captcha
 * @return true si l'entree dans le captcha correspond a l'image, false sinon
 */
function verif_captcha() {
	require_once ('recaptchalib.php');
	if (racine_serveur() == 'http://www.l4h.be') $privatekey = '6LeakP4SAAAAANI5UfjMfks37qKTwy9QUfxxyy1R';
	else if (racine_serveur() == 'http://l4h.zz.vc') $privatekey = '6Le2SP0SAAAAAO-DTGh66g2xEkdyIqu4o-ugvTey';
	else if (racine_serveur() == 'localhost') $privatekey = "6LfXS_0SAAAAAPHAGIEsxgTGRqAUzDe2EfhLcvzI";
	else $privatekey = "6LcX9PwSAAAAAFWDWMoDu9zW1xCfK5VeF4sHi4A7";
	$resp = recaptcha_check_answer ( $privatekey, $_SERVER ["REMOTE_ADDR"], $_POST ["recaptcha_challenge_field"], $_POST ["recaptcha_response_field"] );

	if (! $resp->is_valid)
		return false;
	else
		return true;
}
?>