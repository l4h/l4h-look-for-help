<?php
/**
 * Fonction allant retourner l'id de la derniere conversation.
 * @param object $mysqli
 * @return l'id de la derneire conversation cree
 */
function getConversationIdMax($mysqli) {
	$max = (mysqli_fetch_array ( $mysqli->query ( "SELECT MAX(conversationId) AS maxId FROM users_conversations;" ) ));
	return $max ['maxId'];
}
/**
 * Fonction allant retourner le plus grand id d'un utilisateur.
 * @param object $mysqli
 * @return un entier represantant l'id max du tableau des utilisateurs
 */
function getMaxUserId($mysqli) {
	$max = (mysqli_fetch_array ( $mysqli->query ( "SELECT MAX(id) AS maxId FROM users;" ) ));
	return $max ['maxId'];
}
/**
 * Fonction donnant l'id d'une conversation en fonction des utilisateurs concernes.
 * @param object $mysqli
 * @param int $userId1 l'id de l'utilisateur #1
 * @param int $userId2 l'id de l'utilisateur #2
 * @return l'id de la conversation entre deux utilisateurs donnes
 */
function getConversationId($mysqli, $userId1, $userId2) {
	$e = (mysqli_fetch_array ( $mysqli->query ( "SELECT conversationId FROM users_conversations
	WHERE (userId1 = " . $userId1 . " AND userId2 = " . $userId2 . ") OR (userId1 = " . $userId2 . " AND userId2 = " . $userId1 . ")" ) ));
	return $e ['conversationId'];
}
/**
 * Fonction allant retourner toutes les conversations d'un utilisateur donne.
 * @param object $mysqli
 * @param int $userId l'id de l'utilisateur
 * @return un tableau contenant l'id et les 2 utilisateurs.
 */
function getConversations($mysqli, $userId) {
	$requete = "SELECT cm.conversationId,userId1,userId2 from users_conversations uc join conversations_messages cm on cm.conversationId = uc.conversationId where uc.userId1 = (?) || uc.userId2 = (?) GROUP BY cm.conversationId order by MAX(dateCreation) DESC";
	$stmt = $mysqli->prepare ( $requete );
	$stmt->bind_param ( "ii", $userId, $userId );
	$stmt->execute ();
	$stmt->bind_result ( $col1, $col2, $col3);
	$result = array ();
	while ( $stmt->fetch () )
		array_push ( $result, array (
				$col1,
				$col2,
				$col3
		) );
	$stmt->close ();
	return $result;
}
/**
 * Fonction fournissant tous les messages s'une conversation donnee.
 * @param object $mysqli
 * @param int $convId l'id de la conversation
 * @return un tableau de tous les messages d'un utilisateur; il contient l'id de l'utilisateur qui a cree le message, le texte et la date  
 */
function getMessages($mysqli, $convId) {
	$stmt = $mysqli->prepare ( "SELECT userId, message, DATE_FORMAT(dateCreation, '%H:%i:%s %d/%m/%Y') AS date FROM conversations_messages WHERE conversationId = (?) ORDER BY dateCreation ASC;" );
	$stmt->bind_param ( "i", $convId );
	$stmt->execute ();
	$stmt->bind_result ( $col1, $col2, $col3 );
	$result = array ();
	while ( $stmt->fetch () )
		array_push ( $result, array (
				$col1,
				$col2,
				$col3 
		) );
	$stmt->close ();
	return $result;
}
/**
 * Fonction determinant si un utilisateur donne possede un ou des nouveaux messages.
 * @param object $mysqli 
 * @param int $userId l'id de l'utilisateur
 * @return true si l'utilisateur a un ou des nouveaux messages, faux sinon
 */
function nouveauxMessages($mysqli, $userId) {
	$re = (mysqli_fetch_array ( $mysqli->query ( "SELECT COUNT(*)<>SUM(vu) AS vu from users_conversations
JOIN conversations_messages ON users_conversations.conversationId = conversations_messages.conversationId
WHERE (userId1 = " . $userId . " OR userId2 = " . $userId . ") AND (userId <> " . $userId . ") ;" ) ));
	return $re ['vu'];
}
/**
 * Fonction allant mettre a vu tous les messages d'une conversation donnee.
 * @param object $mysqli
 * @param int $convId l'id de la conversation
 * @param int $userId l'id de l'utilisateur
 */
function mettreVu($mysqli, $convId, $userId) {
	$mysqli->query ( "UPDATE conversations_messages SET vu = TRUE WHERE conversationId = " . $convId . " AND userId = " . $userId . ";" );
}
/**
 * Fonction allant determiner si un utilisateur donne a un ou plusieurs messages nouveaux dans une conversation donnees.
 * @param object $mysqli
 * @param int $userId
 * @param int $conversationId
 * @return true si il y a des nouveaux messages, false sinon
 */
function nouveauxMessagesConversation($mysqli, $userId, $conversationId) {
	$re = (mysqli_fetch_array ( $mysqli->query ( "SELECT COUNT(*)<>SUM(vu) AS vu from users_conversations
JOIN conversations_messages ON users_conversations.conversationId = conversations_messages.conversationId
WHERE (userId1 = " . $userId . " OR userId2 = " . $userId . ") AND (userId <> " . $userId . ") AND (users_conversations.conversationId =" . $conversationId . ");" ) ));
	return $re ['vu'];
}
/**
 * Verifie que l'id de la conversation correspond a un utilisateur donne
 * @param object $mysqli
 * @param int $convId l'id de la conversation
 * @param int $userId l'id de l'utilisateur
 * @return true si l'utilisateur donne est concerne par l'id de la conversation donnee
 */
function conversationCorrecte($mysqli, $convId, $userId) {
	$stmt = $mysqli->prepare ( "SELECT COUNT(*) FROM users_conversations WHERE conversationId = (?) AND (userId1 = (?) OR userId2 = (?)); " );
	$stmt->bind_param ( "iii", $convId, $userId, $userId );
	$stmt->execute ();
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	return $result > 0;
}
/**
 * Fonction allant envoyer un message a l'id d'un utilisateur donne.
 * @param object $mysqli
 * @param string $texte le corps du message
 * @param int $dest l'id de l'utilisateur qui sera le destinataire
 */
function envoieMessage($mysqli, $texte, $dest) {
	$idConversation = getConversationId ( $mysqli, $_SESSION ['id'], $dest );
	
	if ($idConversation > 0 && $dest > 0) { // si conversation existe
		$stmt = $mysqli->prepare ( "INSERT INTO conversations_messages (conversationId,userId,message,dateCreation,vu) VALUES ((?),(?),(?),NOW(),FALSE)" );
		$stmt->bind_param ( "iis", $idConversation, $_SESSION ['id'], $texte );
		$stmt->execute ();
		$stmt->close ();
	} else if ($dest > 0) {
		if ($idConversation < 1) {
			$idConversation = getConversationIdMax ( $mysqli ) + 1;
		}
		$stmt = $mysqli->prepare ( "INSERT INTO conversations_messages (conversationId,userId,message, dateCreation, vu) VALUES ((?),(?),(?),NOW(),FALSE)" );
		$stmt->bind_param ( "iis", $idConversation, $_SESSION ['id'], $texte );
		$stmt->execute ();
		
		$stmt = $mysqli->prepare ( "INSERT INTO users_conversations (conversationId,userId1,userid2)  VALUES ((?),(?),(?))" );
		$stmt->bind_param ( "iii", $idConversation, $_SESSION ['id'], $dest );
		$stmt->execute ();
		$stmt->close ();
	}
	
}


function afficheCommentaires($mysqli,$userId){
	$stmt = $mysqli->prepare ( "SELECT commentateurId,commenteId,annonceId,typeAnnonce,message,avis,date,nom,prenom from users_commentaires uc join users u on u.id=uc.commentateurId WHERE commenteId = (?) order by date desc" );
	$stmt->bind_param ( "i", $userId );
	$stmt->execute ();
	$stmt->bind_result ( $col1, $col2, $col3,$col4,$col5,$col6,$col7,$col8,$col9 );
	$result = array ();
	while ( $stmt->fetch () )
		array_push ( $result, array (
				$col1,
				$col2,
				$col3,
				$col4,
				$col5,
				$col6,
				$col7,
				$col8,
				$col9 
		) );
	$stmt->close ();
	return $result;
}
?>