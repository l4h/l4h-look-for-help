<?php
function reponseOffreDemande($mysqli, $type) {
	$urlRedirect = "reponse" . $type . ".php";
	if (! empty ( $_GET ['ido'] )) {
		if (is_numeric ( $_GET ['ido'] )) {
			$annonceId = $_GET ['ido'];
			if (ownAnnonce ( $mysqli, $annonceId, $_SESSION ['id'], $type ) || ! annonceExiste ( $mysqli, $type, $annonceId )) {
				$upcase = ucfirst ( $type );
				$url = "/php/" . $type . "s/liste" . $upcase . "s.php";
				redirect ( $url );
			}
			if (! enAttente ( $mysqli, $annonceId, $_SESSION ['id'], $type )) {
				insertAnnonceListeAttente ( $mysqli, $type, $annonceId, $_SESSION ['id'] );
				
				if ($type == 'offre')
					$titre = 'L4H - Un utilisateur aimerait vos services.';
				if ($type == 'demande')
					$titre = 'L4H - Un utilisateur est prêt à vous aider.';
				
				$nomPrenomProposeur = getPrenomNom ( $mysqli, $_SESSION ['id'] );
				$idDestinataire = getIdUser ( $mysqli, $annonceId );
				$nomPrenomDestinataire = getPrenomNom ( $mysqli, $idDestinataire );
				$titreAnnonce = getTitreAnnonce ( $mysqli, $annonceId, $type );
				$description = getDescriptionAnnonce ( $mysqli, $annonceId );
				$email = emailUser ( $mysqli, $idDestinataire );
				
				$message = "Bonjour $nomPrenomDestinataire,</br></br>";
				
				if ($type == 'offre') {
					$message .= $nomPrenomProposeur . ' aimerait votre aide.';
					$apostrophe = "l'";
				}
				if ($type == 'demande') {
					$message .= $nomPrenomProposeur . ' est prêt à vous aider.';
					$apostrophe = "la";
				}
				
				$message .= '</br></br>Petit rappel de ' . $apostrophe . ' ' . $type . ': </br>';
				$message .= 'Titre : ' . $titreAnnonce . ' </br>';
				$message .= 'Description : ' . $description . '</br></br>';
				$message .= 'Pour plus d\'information, passez par notre site => <a href="http://l4h.be">http://l4h.be</a>.</br></br></br>';
				$message .= 'Nous vous remercions votre confiance et vous souhaitons une agréable journée, </br></br>';
				$message .= 'L\'équipe Looking for help. </br></br></br>';
				
				envoi_email ( $titre, $message, $email );
			}
			redirect ( $urlRedirect );
		}
	}
	if (! empty ( $_GET ['annonceId'] ) && isset ( $_GET ['cancel'] )) {
		if (is_numeric ( $_GET ['annonceId'] )) {
			cancelRelation ( $mysqli, $_GET ['annonceId'], $type );
		}
		redirect ( $urlRedirect );
	}
	echo "<div class='container fluid'><br/><br/><br/><br/>";
	echo "<h1 class='text-center'>" . ucfirst ( $type ) . "s : mes relations</h1>";
	
	$mesRelations = getMesRelations ( $mysqli, $type );
	
	$nbRelations = count ( $mesRelations );
	if ($nbRelations > 0) {
		echo "<br/><table class='sortable table table-bordered table-hover table-condensed'>
			<tr class='info'>
			<td></td>
			<th>Titre de l'" . $type . "</th>
			<th>Créateur</th>
			<th>Date d'expiration</th>
			<th>Date de mise en relation</th>
			<th>Statut de la relation</th>
			<td><b>Envoyer un message</b></td>
			<td><b>Annuler</b></td></tr>";
		$relationsActives = array ();
		for($i = 0; $i < $nbRelations; $i ++) {
			$annonceId = $mesRelations [$i] [0];
			$new = $mesRelations [$i] [2];
			$dateRelation = $mesRelations [$i] [1];
			$dateExpiration = $mesRelations [$i] [3];
			array_push ( $relationsActives, $annonceId );
			
			$enAttente = enAttente ( $mysqli, $annonceId, $_SESSION ['id'], $type );
			$attribue = attribue ( $mysqli, $annonceId, $_SESSION ['id'], $type );
			$estAttribue = annonceAttribue ( $mysqli, $annonceId, $type );
			
			$titre = getTitreAnnonce ( $mysqli, $annonceId, $type );
			echo "<tr>";
			if ($new < 1)
				echo '<td><strong>NEW</strong></td>';
			else
				echo '<td></td>';
			echo "<td>" . $titre . "</td>
	<td>" . getAnnonceCreateur ( $mysqli, $annonceId, $type ) . "</td>
		<td>" . $dateExpiration . "</td>	
		<td>" . $dateRelation . "</td>";
			if ($enAttente && ! $attribue && ! $estAttribue) {
				echo "<td class='btn-warning'>En Attente</td>";
			} else if ($enAttente && $attribue) {
				echo "<td class='btn-success'>Attribué</td>";
			} else if ($estAttribue)
				echo "<td class='btn-danger'>Rejeté</td>";
			
			echo "<td><button data-toggle='modal' data-target='#modalMessage" . $annonceId . "'><span class='glyphicon glyphicon-envelope'></span></button></td>";
			if (! $attribue || ! $estAttribue) {
				echo "<td><a href='" . racine_serveur () . "/php/" . $type . "s/reponse" . $type . ".php?annonceId=" . $annonceId . "&cancel' class='btn btn-danger' data-original-title='Annuler la relation' data-toggle='tooltip'  ><span class='glyphicon glyphicon-remove-sign'></span></a></td>";
			}
			echo "</tr>";
			
			if ($new < 1)
				vuAnnonceRelationCreateur ( $mysqli, $annonceId, $_SESSION ['id'], $type );
		}
		echo "</table>";
		echo "</div>";
	} else {
		echo "Vous n'avez pas encore de relations !";
	}
	if (! empty ( $relationsActives )) {
		$nbRelationsActives = count ( $relationsActives );
		
		for($j = 0; $j < $nbRelationsActives; $j ++) {
			$annonceId = $relationsActives [$j];
			$corrId = userId_annonce ( $mysqli, $type, $annonceId );
			$correspondant = getAnnonceCreateur ( $mysqli, $relationsActives [$j], $type );
			$titre = getTitreAnnonce ( $mysqli, $annonceId, $type );
			echo '
<div class="modal fade" id="modalMessage' . $annonceId . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-envelope"></span> Envoie d\'un message à ' . $correspondant . '</h4>
      </div>
      <div class="modal-body">
		
			<form id="formTexte" class="form-horizontal" role="form" method="post" accept-charset="UTF-8" >	
        	Titre:  ' . $titre . '</br>
        	<input type="hidden" name="titre' . $corrId . '" value="' . $titre . '">		
			<textarea style="resize:vertical" rows = "10" cols = "65" name = "texte' . $corrId . '"  placeholder="Tapez votre message ici. Le titre de l\'annonce concernée y sera inclus automatiquement." ></textarea>
				
      </div>
      <div class="modal-footer">
        	
        <input name="envoyer" type="submit" class="btn btn-primary" value="Envoyer" onclick="envoieMessage(' . $corrId . ')">
        		</form>
      </div>
    </div>
  </div>
</div>';
		}
		if (! empty ( $_POST )) {
			$dest = - 1;
			
			foreach ( $_POST as $key => $entry ) {
				if (strstr ( $key, 'texte' )) {
					$dest = intval ( filter_var ( $key, FILTER_SANITIZE_NUMBER_INT ) );
				}
			}
			if (! empty ( $_POST ['titre' . $dest] )) {
				if ($type == 'demande')
					$titre = "Mise en relation de la " . $type . ": " . $_POST ['titre' . $dest];
				else
					$titre = "Mise en relation de l'" . $type . ": " . $_POST ['titre' . $dest];
				$message = '<i>' . $titre . '</i></br>' . $_POST ['texte' . $dest];
				
				if ($dest > 0)
					envoieMessage ( $mysqli, $message, $dest );
			}
		}
	}
	
	$mesRelationsExpirees = getMesRelationsExpirées ( $mysqli, $type );
	
	$nbRelations = count ( $mesRelationsExpirees );
	if ($nbRelations > 0) {
		echo "<div class='container fluid'><br/><br/><br/><br/>";
		echo "<h1 class='text-center'>Historique des relations</h1>";
		echo "<br/><table class='sortable table table-bordered table-hover table-condensed'>
			<tr class='info'>
			<td></td>
			<th>Titre de l'" . $type . "</th>
			<th>Créateur</th>
			<th>Date d'expiration</th>
			<th>Date de mise en relation</th>
			<th>Statut de la relation</th>
			<td><b>Envoyer un message</b></td>
			<td></td></tr>";
		$relationsExpirees = array ();
		for($i = 0; $i < $nbRelations; $i ++) {
			$annonceId = $mesRelationsExpirees [$i] [0];
			$new = $mesRelationsExpirees [$i] [2];
			$dateRelation = $mesRelationsExpirees [$i] [1];
			$dateExpiration = $mesRelationsExpirees [$i] [3];
			array_push ( $relationsExpirees, $annonceId );
			
			$enAttente = enAttente ( $mysqli, $annonceId, $_SESSION ['id'], $type );
			$attribue = attribue ( $mysqli, $annonceId, $_SESSION ['id'], $type );
			$estAttribue = annonceAttribue ( $mysqli, $annonceId, $type );
			$titre = getTitreAnnonce ( $mysqli, $annonceId, $type );
			
			echo "<tr>";
			if ($new < 1)
				echo '<td><strong>NEW</strong></td>';
			else
				echo '<td></td>';
			echo "<td>" . $titre . "</td>
	<td>" . getAnnonceCreateur ( $mysqli, $annonceId, $type ) . "</td>
		<td>" . $dateExpiration . "</td>
		<td>" . $dateRelation . "</td>";
			if ($enAttente && ! $attribue && ! $estAttribue) {
				echo "<td class='btn-warning'>En Attente</td>";
			} else if ($enAttente && $attribue) {
				echo "<td class='btn-success'>Attribué</td>";
			} else if ($estAttribue)
				echo "<td class='btn-danger'>Rejeté</td>";
			
			echo "<td><button data-toggle='modal' data-target='#modalMessage" . $annonceId . "'><span class='glyphicon glyphicon-envelope'></span></button></td>";
			if ($enAttente && $attribue) {
				$annonceDe = annonceDe ( $mysqli, $annonceId );
				$nomPrenom = getPrenomNom ( $mysqli, $annonceDe );
				$userNote = userNote ( $mysqli, $annonceId, $type, $annonceDe );
				if ($userNote < 1) {
					if ($type == 'demande')
						$placeholder = "Merci de laisser un bref commentaire sur le prestataire.";
					if ($type == 'offre')
						$placeholder = "Merci de laisser un bref commentaire sur l'utilisateur presté.";
					?>
<td><button class="btn btn-warning btn-xs" data-toggle="modal"
		data-target="#modalCommenter<?php echo $annonceDe; ?>">Donner votre
		avis sur le prestataire</button>
			<?php
					//
					// Modal des commentaires sur l'user
					//
					echo '<div class="modal fade" id="modalCommenter' . $annonceDe . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					  <div class="modal-dialog">
					    <div class="modal-content">
					      <div class="modal-header">
					        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
					        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-envelope"></span> Commenter ' . $nomPrenom . '</h4>
					      </div>
					      <div class="modal-body">
							
								<form id="formTexte" class="form-horizontal" role="form" method="post" accept-charset="UTF-8" >	
					        	Titre:  ' . $titre . '</br>	
								<textarea style="resize:vertical" rows = "10" cols = "65" name = "texte' . $annonceDe . '"  placeholder="' . $placeholder . '"></textarea>
									
					      </div>
				      <div class="modal-footer">
				        <form class="form-horizontal" role="form" method="post" accept-charset="UTF-8">
							<button name="' . $annonceId . 'positif" type="submit" data-toggle="modal" data-target="#modalCommentaire".$annonceRealiseePar." class="btn btn-success btn-xs">Positif</button>
							<button name="' . $annonceId . 'neutre" type="submit" class="btn btn-default btn-xs">Neutre</button>
							<button name="' . $annonceId . 'negatif" type="submit" class="btn btn-danger btn-xs">Negatif</button>
							</form>
				      </div>
				    </div>
				  </div>
				</div>';
					//
					// fin du modal
					//
				}
			}
			echo "</tr>";
			
			if ($new < 1)
				vuAnnonceRelationCreateur ( $mysqli, $annonceId, $_SESSION ['id'], $type );
		}
		echo "</table>";
		echo "</div>";
	}
	if (! empty ( $relationsExpirees )) {
		$nbRelationsExpirees = count ( $relationsExpirees );
		
		for($j = 0; $j < $nbRelationsExpirees; $j ++) {
			$annonceId = $relationsExpirees [$j];
			$corrId = userId_annonce ( $mysqli, $type, $annonceId );
			$correspondant = getAnnonceCreateur ( $mysqli, $relationsExpirees [$j], $type );
			$titre = getTitreAnnonce ( $mysqli, $annonceId, $type );
			echo '
<div class="modal fade" id="modalMessage' . $annonceId . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-envelope"></span> Envoie d\'un message à ' . $correspondant . '</h4>
      </div>
      <div class="modal-body">
		
			<form id="formTexte" class="form-horizontal" role="form" method="post" accept-charset="UTF-8" >
        	Titre:  ' . $titre . '</br>
        	<input type="hidden" name="titre' . $corrId . '" value="' . $titre . '">
			<textarea style="resize:vertical" rows = "10" cols = "65" name = "texte' . $corrId . '"  placeholder="Tapez votre message ici. Le titre de l\'annonce concernée y sera inclus automatiquement." ></textarea>
		
      </div>
      <div class="modal-footer">
     
        <input name="envoyer" type="submit" class="btn btn-primary" value="Envoyer" onclick="envoieMessage(' . $corrId . ')">
        		</form>
      </div>
    </div>
  </div>
</div>';
		}
		
		for($j = 0; $j < $nbRelations; $j ++) {
			if (! empty ( $_POST )) {
				$dest = - 1;
				foreach ( $_POST as $key => $entry ) {
					if (strstr ( $key, 'positif' ) || strstr ( $key, 'neutre' ) || strstr ( $key, 'negatif' )) {
						$annonceId = intval ( filter_var ( $key, FILTER_SANITIZE_NUMBER_INT ) );
						$note = preg_replace ( '/[0-9 ]*+/', '', $key );
					}
					if (strstr ( $key, 'texte' )) {
						$dest = intval ( filter_var ( $key, FILTER_SANITIZE_NUMBER_INT ) );
					}
				}
				// Choix des notes
				if (empty ( $_POST ['titre' . $dest] )) {
					switch ($note) {
						case 'positif' :
							$note_num = 1;
							break;
						case 'neutre' :
							$note_num = 0;
							break;
						case 'negatif' :
							$note_num = - 1;
							break;
					}
					$annonceDe = annonceDe ( $mysqli, $annonceId );
					$commentaire = null;
					//Si le commentaire n'est pas vide
					if (! empty ( $_POST ['texte' . $annonceDe] )) {
						$commentaire = $_POST ['texte' . $annonceDe];
						commenterUser ( $mysqli, $_SESSION ['id'], $annonceDe, $annonceId, $type, $commentaire, $note_num );
					}
					noterUser ( $mysqli, $annonceId, $type, $_SESSION ['id'], $annonceDe, $note_num );
					if ($type == 'offre')
						$url = "reponseoffre.php";
					else
						$url = "reponsedemande.php";
					redirect ( $url );
				} else {
					if ($type == 'demande')
						$titre = "Mise en relation de la " . $type . ": " . $_POST ['titre' . $dest];
					else
						$titre = "Mise en relation de l'" . $type . ": " . $_POST ['titre' . $dest];
					$message = '<i>' . $titre . '</i></br>' . $_POST ['texte' . $dest];
					
					if ($dest > 0)
						envoieMessage ( $mysqli, $message, $dest );
				}
			}
		}
	}
	
	?>
<script>
function envoieMessage(corres){ 
	texte = document.forms["formTexte"]["correspondant".corres].value; 
	if(texte.length != null){
		document.getElementById("formTexte").submit();
	}
}
</script>

	</body>
<?php
}
?>