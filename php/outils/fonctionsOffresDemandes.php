<?php
function couleurFondCommentaire($avis){
	if($avis == 1) return  "background-color: rgba(65, 150, 65, 0.2)";
	elseif ($avis == -1) return " background-color: rgba(193, 46, 42, 0.2)";
	else return " background-color: rgba(105, 105, 105, 0.2)";
}

function annonceExiste($mysqli, $type, $annonceId) {
	$requete = "SELECT annonceId from users_annonce where annonceId = $annonceId";
	return  mysqli_num_rows ( $mysqli->query ( $requete ) );
}

/**
 * Cette fonction recherche l'id d'un user qui a créé l'annonce
 *
 * @param
 *        	$mysqli
 * @param $type offre
 *        	ou demande
 * @param $annonceId c'est
 *        	l'id de l'offre ou la demande concernée
 * @return renvoie l'id de l'utilisateur
 */
function userId_annonce($mysqli, $type, $annonceId) {
	$requete = "SELECT userId from users_annonce where annonceId = $annonceId";
	$user = mysqli_fetch_array ( $mysqli->query ( $requete ) );
	return $user ['userId'];
}

/**
 * Cherche le nombre d'offre ou demande en cours(non cloturée ou non expirée) pour un user.
 *
 * @param unknown $mysqli        	
 * @param unknown $userId
 *        	id de l'utilisateur concerné
 * @param unknown $type
 *        	offre ou demande
 * @return revoit un nombre
 */
function nbOffreDemandeEnCours($mysqli, $userId, $type) {
	$requete = "Select * from users_annonce ua join annonces a on (ua.annonceId = a.annonceId) 
				where ua.userId = '$userId' and type = '".$type."' and (a.dateCloture is NULL and a.dateExpiration > NOW())
				or (a.dateExpiration is NULL and a.dateCloture is NULL)";
	return mysqli_num_rows ( $mysqli->query ( $requete ) );
}

/**
 *
 * @param unknown $mysqli        	
 * @param unknown $annonceId
 *        	on passe l id de l'offre ou la demande
 * @param unknown $type
 *        	le type = offre ou = demande
 * @param unknown $bool
 *        	si 1 , on va cloturer l'annonce sinon on décloture si bool = 2
 */
function cloture_decloture($mysqli, $annonceId, $type, $bool) {
	$estCloturee = estCloturee ( $mysqli, $annonceId, $type );
	if (! $estCloturee) {
		if ($bool == 1) {
			$requete = "INSERT INTO annonces_cloturees ( annonceId ,cloturePar) VALUES ($annonceId," . $_SESSION ['id'] . ") ";
			$update = "UPDATE annonces SET dateCloture= NOW() WHERE annonceId = $annonceId";
		}
	} else {
		if ($bool == 2) {
			$requete = "DELETE FROM annonces_cloturees WHERE annonceId = " . $annonceId;
			$update = "UPDATE annonces SET dateCloture = NULL WHERE annonceId = $annonceId";
		}
	}
	$mysqli->query ( $requete );
	$mysqli->query ( $update );
}
function estCloturee($mysqli, $annonceId, $type) {
	return mysqli_num_rows ( $mysqli->query ( "SELECT annonceId from annonces_cloturees where annonceId = $annonceId ") );
}

/**
 * fonction destinée a trouver le niveau de cloture d'une annonce en fonction de la valeur du profil de l'utilisateur qui l'a cloturée
 *
 * @param unknown $mysqli        	
 * @param unknown $annonceId
 *        	id de l'offre ou la demande concernée
 * @param unknown $type
 *        	input offre ou demande
 * @return retourne la valeur du profil de l'utilisateur qui l'a cloturée
 */
function niveau_cloture($mysqli, $annonceId, $type) {
	$requete = "SELECT `cloturePar` FROM `annonces_cloturees` WHERE `annonceId` = $annonceId ";
	$user = mysqli_fetch_array ( $mysqli->query ( $requete ) );
	return valeurUser ( $mysqli, $user ['cloturePar'] );
}
/**
 *
 * @param unknown $mysqli        	
 * @param unknown $id        	
 * @return string
 */
function encadre_premium($mysqli, $id) {
	if ($_SESSION ['afficherMesAnnonces'] == 'oui' && $_SESSION ['id'] == $id)
		$div = 'border: solid #006699 1px; background-color: rgba(0, 102, 153, 0.4);';
	else if (userEstPremium ( $mysqli, $id ))
		$div = 'border: solid #EFD242 1px; background-color: rgba(239, 210, 66, 0.4);';
	else
		$div = '';
	return $div;
}

/**
 * calcule la distance entre 2 adresses
 *
 * @param unknown $adresse3
 *        	l'adresse d'origine
 * @param unknown $adresse4
 *        	l'adresse de destination
 * @return number
 */
function getDistance($adresse3, $adresse4) {
	$base_url = 'http://maps.googleapis.com/maps/api/directions/xml?sensor=false';
	$xml = simplexml_load_file ( "$base_url&origin=$adresse3&destination=$adresse4" );
	$distance = ( string ) $xml->route->leg->distance->value / 1000;
	$duration = ( string ) $xml->route->leg->duration->text;
	return $distance;
}
// on enregistre toutes les annonces dans un tableau afin de les trier
/**
 * fonction qui permet de créer le tableau des annonces
 *
 * @param unknown $adresseOrigin
 *        	c'est l'adresse de l'utilisateur qui fait sa recherche
 * @param unknown $tab
 *        	le return de la fonction liste_offres_demandes
 * @param unknown $proximite
 *        	qui correspond à la recherche des offres_demandes plus près que x km si 0 il n'y pas de recherche de proximité
 * @param unknown $tri
 *        	$tri = distance on trie de la plus proche à la plus éloignée
 * @return retourne un tableau d'offres ou demandes
 */
function tableau_annonces($adresseOrigin, $tab, $proximite, $tri) {
	$adresse_destination = array ();
	for($i = 0; $i < sizeof ( $tab ); $i ++) {
		
		if (! $tab [$i] ['visibiliteAdresse'])
			$adresse_destination [$i] = $tab [$i] ['codePost'] . '+' . $tab [$i] ['ville'];
		else
			$adresse_destination [$i] = $tab [$i] ['numero'] . '+' . $tab [$i] ['rue'] . '+' . $tab [$i] ['codePost'] . '+' . $tab [$i] ['ville'];
		
		$adresse_destination [$i] = str_replace ( " ", "+", $adresse_destination [$i] );
		$adresse_destination [$i] = str_replace ( "&#039;", "+", $adresse_destination [$i] );
		$tab [$i] ['adresseDestination'] = $adresse_destination [$i];
		$tab [$i] ['distance'] = getDistance ( $adresseOrigin, $adresse_destination [$i] );
	}
	// on trie si le $_POST['tri'] == distance
	
	if ($tri == 'distance')
		$tab = tri_distance ( $tab );
	if ($proximite > 0) {
		$j = 0;
		for($i = 0; $i < sizeof ( $tab ); $i ++) {
			if ($tab [$i] ['distance'] < $proximite) {
				$new_tabs [$j] = $tab [$i];
				$j ++;
			}
		}
		if (empty ( $new_tabs ))
			$tab = 0;
		else
			$tab = $new_tabs;
	}
	return $tab;
}
function tri_distance($tab) {
	$i = sizeof ( $tab );
	$ok = false;
	do {
		$ok = false;
		for($j = $i - 1; $j != 0; $j --) {
			if ($tab [$j] ['distance'] < $tab [$j - 1] ['distance']) {
				$temp = $tab [$j];
				$tab [$j] = $tab [$j - 1];
				$tab [$j - 1] = $temp;
				$ok = true;
			}
		}
	} while ( $ok );
	return $tab;
}
/**
 * Arrondit la distance à 2 chiffres derrière la virgule
 *
 * @param unknown $distance
 *        	nombre
 */
function distance_km($distance) {
	return round ( $distance, 2, PHP_ROUND_HALF_DOWN );
}

// fonction permettant de lister les offres et les demandes selon la chaine de caracère qu'on entre en paramètre 'offre' ou 'demande'
function liste_offre_demande($mysqli,$liste) {
	
	if (! empty ( $_SESSION ['visibiliteAdresse'] ))
		$adresse = $_SESSION ['numero'] . '+' . $_SESSION ['rue'] . '+' . $_SESSION ['codePost'] . '+' . $_SESSION ['ville'];
	else
		$adresse = $_SESSION ['codePost'] . '+' . $_SESSION ['ville'];
	$adresse = str_replace ( " ", "+", $adresse );
	$adresse = str_replace ( "&#039;", "+", $adresse );
	
	// Permet d'afficher mes offres ou pas dans la liste
	if (! empty ( $_POST ['checkbox'] ))
		if ($_POST ['checkbox'] == 'non')
			$_SESSION ['afficherMesAnnonces'] = null;
		else
			$_SESSION ['afficherMesAnnonces'] = $_POST ['checkbox'];
	
	
	// dans requetes.php crétation de la requete sql en fonction des options de recherche
	// on retourne un tableau des annonces
	$annonces = liste_offres_demandes ( $mysqli, $liste, $_POST, false, $_SESSION ['id'] );
	
	?>
<script>
function myFunction() {
    document.getElementById("myForm").submit();
}

function function_rechercheAvancee() {
    document.getElementById("rechercheAvancee").submit();
}
</script>

<div class="row">
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-md-6">
				<span>
					<form action="#" method="POST">
						<label><?php echo 'Afficher mes '.$liste.'s '; ?></label> oui <input
							value="oui" name="checkbox" onclick="this.form.submit()"
							type="radio"
							<?php if($_SESSION['afficherMesAnnonces'] != null)  echo'checked';?>>
						non <input value="non" name="checkbox"
							onclick="this.form.submit()" type="radio"
							<?php if($_SESSION['afficherMesAnnonces'] == null)  echo'checked';?>></br>
					</form>
				</span>
				<div class="input-group">
					<div class="input-group-btn search-panel">
						<form id="myForm" action="#" method="POST">
							<select class=" btn btn-default" name="tag"
								onchange="this.form.submit()">
								<option value='-1'>Toute catégorie</option>
							<?php
	
	$query = "SELECT * from tags ";
	$mysqli->query ( "SET NAMES utf8" );
	if ($result = $mysqli->query ( $query )) {
		
		/* Récupération un tableau associatif */
		while ( $row = $result->fetch_assoc () ) {
			$tag = $row ['tagId'];
			$option = '<OPTION value = ' . $tag . ' ';
			if (! empty ( $_POST ['tag'] ))
				if ($tag == $_POST ['tag'])
					$option .= ' selected ';
			$option .= " > $row[tagNom]";
			echo $option;
		}
		
		$result->free ();
	}
	?>
					</select>
					
					</div>

					<input type="text" class="form-control" name="recherche"
						placeholder="Recherche par mot clé..."> <span
						class="input-group-btn">
						<button class="btn btn-default glyphicon glyphicon-search"
							type="button" onclick="myFunction()" value="test"></button>
						</form>
						<button class="btn btn-default glyphicon glyphicon-plus-sign"
							data-toggle="modal" data-target="#myModal"
							alt="recherche avancée"></button>
					</span>

				</div>
			</div>
		</div>
	</div>


	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Recherche avancée</h4>
				</div>
				<div class="modal-body">
					<form id="rechercheAvancee" action="#" method="POST">
						<label class="col-sm-4 control-label" for="textinput">Catégorie</label>
						<select class=" btn btn-default" name="tag">
							<option value='-1'>Toute catégorie</option>
							<?php
	
	$query = "SELECT * from tags ";
	$mysqli->query ( "SET NAMES utf8" );
	if ($result = $mysqli->query ( $query )) {
		
		/* Récupération un tableau associatif */
		while ( $row = $result->fetch_assoc () ) {
			$tag = $row ['tagId'];
			$option = '<OPTION value = ' . $tag . ' ';
			if (! empty ( $_POST ['tag'] ))
				if ($tag == $_POST ['tag'])
					$option .= ' selected ';
			$option .= " > $row[tagNom]";
			echo $option;
		}
		
		$result->free ();
	}
	?>
					</select></br> </br> <label class="col-sm-4 control-label"
							for="textinput">Mot clé</label> <input type="text"
							class="form-control" name="recherche"
							placeholder="Recherche par mot clé..."> </br> </br> <label
							class="col-sm-4 control-label" for="textinput">Proximité (km)<i
							class="glyphicon glyphicon-pushpin"></i></label> <input
							type="number" class="form-control" name="proximite"
							placeholder="50"> </br> </br> <label
							class="col-sm-4 control-label " for="textinput">Réputation
							minimale (points)<i class="glyphicon glyphicon-thumbs-up"></i>

						</label> <input type="number" class="form-control"
							name="reputation" placeholder="50"></br> <label
							class="col-sm-4 control-label" for="textinput">Recherche parmi </label>
						<select class=" btn btn-default" name="premium" id="premium">
							<option value="0">Tous</option>
							<option value="1">Les premium</option>
							<option value="2">Les non premium</option>
						</select> </br> </br> <label class="col-sm-4 control-label "
							for="textinput">Auteur précis </label><input id="auteur"
							type="text" class="form-control" name="auteur"
							placeholder="auteur"> </br> <label class="col-sm-4 control-label"
							for="textinput">Trier par</label> <select
							class=" btn btn-default" name="tri" id="tri">
							<option value="0">- - - Choix - - -</option>
							<option value="distance">Distance</option>
							<option value="dateCreation">Date de création</option>
							<option value="dateExpiration">Date d'expiration</option>
						</select>

						</fieldset>
					</form>

					</br> </br>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Annuler</button>
						<input type="submit" class="btn btn-primary"
							onclick="function_rechercheAvancee()" value="Rechercher">
					</div>
				</div>
			</div>
		</div>
	</div>
	</br>
	<div class="row">
		<div class="col-xs-6 col-md-6" style="height: 550px; overflow: auto;">
			<?php
	if (sizeof ( $annonces ) == 0)
		echo '<p class="text-center">Il n\'y a aucun résultat pour cette recherche.</p>';
	else {
		if (empty ( $_POST ['tri'] ))
			$tri = - 1;
		else
			$tri = $_POST ['tri'];
		if (empty ( $_POST ['proximite'] ))
			$proximite = - 1;
		else
			$proximite = $_POST ['proximite'];
		
		$row = tableau_annonces ( $adresse, $annonces, $proximite, $tri );
		
		$requete = "SELECT commentateurId,commenteId,annonceId,typeAnnonce,message,avis,date,nom,prenom from users_commentaires uc join users u on u.id=uc.commentateurId order by date desc";
		$stmt = $mysqli->prepare ( $requete );
		$stmt->execute ();
		$stmt->bind_result ( $col1, $col2, $col3,$col4, $col5, $col6,$col7, $col8, $col9);
		$tab_commentaires = array ();
		while ( $stmt->fetch () )
			array_push ( $tab_commentaires, array (
					$col1,
					$col2,
					$col3,
					$col4,
					$col5,
					$col6,
					$col7,
					$col8,
					$col9
			) );
			$stmt->close ();
	
		$f = 0;
		for ($i=0;$i<sizeof($row);$i++){
			for ($j=0;$j<sizeof($tab_commentaires);$j++){
				if($row[$i]['id'] == $tab_commentaires[$j][1]){
					$row[$i]['commentaire'][$f] = $tab_commentaires[$j];
					$f++;
				}
				if($j == sizeof($tab_commentaires)-1) $f = 0;
			}
		}
		if ($row == 0)
			echo '<p class="text-center">Il n\'y a aucun résultat pour cette recherche.</p>';
		else {
			
			/* Récupération un tableau associatif */
			$adresse_destination = array ();
			$tab_annonce = array (
					array () 
			);
			for($i = 0; $i < sizeof ( $row ); $i ++) {
				
				// enregistrement des dates
				$dateExpiration = new DateTime ( $row [$i] ['dateExpiration'] );
				$dateCreation = new DateTime ( $row [$i] ['dateCreation'] );
				
				// variable pour afficher l'avatar
				$lien = racine_serveur () . '/images/avatars/';
				$emplacement = '../../images/avatars/';
				?>
			<!-- ajoute un style si l'user créateur de l'annonce est premium ou que l'useur vu voir ses propres annonces-->
			<div class="well" style="<?php echo encadre_premium($mysqli,$row[$i]['id']);?>">


				<div class="media">
					<a class="pull-left" href="#">
    		<?php echo  afficher_avatar($row[$i]['id'],$lien,$emplacement,100,100); ?>
  		</a>
					<div class="media-body">
						<h4 class="media-heading"><?php echo $row[$i]['titre'].' <small>['.$row[$i]['tagNom'].']</small>';?></h4>
						<p class="text-right">By <?php echo $row[$i]['prenom'].' '.$row[$i]['nom'];?></p>
						<p><?php echo strip_tags($row[$i]['annonce'],'<br>');?></p>
						<ul class="list-inline list-unstyled">
							<li><span><i class="glyphicon glyphicon-calendar"></i><?php echo ' '.$dateCreation->format ( 'd-m-Y H:i' ); ?></span></li>
							<li>|
							
							<li><span><i class="glyphicon glyphicon-time"></i><?php echo ' '.$dateExpiration->format ( 'd-m-Y H:i' ); ?></span></li>
							<li>|</li>
							<span><i class="glyphicon glyphicon-thumbs-up"></i><?php echo ' '.nbPointsUser($mysqli, $row[$i]['id']);?></span> <button class="btn btn-default glyphicon glyphicon-comment"  data-toggle="modal" data-target="#modalCommentaire<?php echo $row[$i]['id'];?>"></button>
							<li>|</li>
							<li><span class="glyphicon glyphicon-pushpin"></span> <?php echo ' à '.distance_km($row[$i]['distance']).' km';?>
            </li>
						</ul>
					</div>
					</br>
					<p class="text-center">
					
					
					<form id="contact" class="form-horizontal" role="form"
						method="post" accept-charset="UTF-8">
<?php if($row[$i]['id'] != $_SESSION['id']) {?>
						<input data-toggle='modal'
							data-target='#modalMessage<?php echo $row[$i]['annonceId'];?>'
							type="button" class="btn btn-warning" value="Contacter"> <?php }?><input
							type="hidden" name="origin" id="origin"
							value="<?php echo $adresse;?>"> <input type="hidden"
							name="destination" id="destination"
							value="<?php echo $row[$i]['adresseDestination'];?>"> 
							<?php if($row[$i]['id'] != $_SESSION['id']) {?>
							<a
							href="<?php echo "/php/".$liste."s/reponse".$liste.".php"?><?php echo '?ido='.$row[$i]['annonceId'];?>"
							class="btn btn-primary">Mettre en relation</a> <input
							type="button" class="btn btn-info" value="Afficher l'itinéraire"
							onclick="javascript:calculate('<?php echo $row[$i]['adresseDestination'];?>')">
							<?php }?>
					</form>
					</p>
				</div>
			</div>
					<?php
			}
			for($i = 0; $i < sizeof ( $row ); $i ++) {
				$nomPrenom = getPrenomNom ( $mysqli, $row [$i] ['id'] );
				echo '
<div class="modal fade" id="modalMessage' . $row [$i] ['annonceId'] . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-envelope"></span> Envoie d\'un message à ' . $nomPrenom . '</h4>
      </div>
      <div class="modal-body">
		
			<form id="formTexte" class="form-horizontal" role="form" method="post" accept-charset="UTF-8" >
			Titre:  '.$row[$i]['titre']  .'</br>
        	<input type="hidden" name="titre' . $row [$i] ['id'] . '" value="' . $row[$i]['titre']. '">	
			<textarea style="resize:vertical" rows = "10" cols = "65" name = "texte' . $row [$i] ['id'] . '" placeholder="Tapez votre message ici. Le titre de l\'annonce concernée y sera inclus automatiquement."></textarea>
		
      </div>
      <div class="modal-footer">
  
        <input name="envoyer" type="submit" class="btn btn-primary" value="Envoyer" onclick="envoieMessage(' . $row [$i] ['id'] . ')">
        		</form>
      </div>
    </div>
  </div>
</div>';
				
echo'<div class="modal fade" id="modalCommentaire' . $row [$i] ['id'] . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Commentaire(s) à propos de '.$nomPrenom.'</h4>
      </div>
      <div class="modal-body">
        <div class="panel panel-default widget">
            <div class="panel-body">
                <ul class="list-group"><div style="max-height: 300px; overflow-x: auto;">';
			//	$commentaires = afficheCommentaires($mysqli,$row [$i] ['id']);
				if(!empty($row[$i]['commentaire'])){
					for($j = 0;$j<sizeof($row[$i]['commentaire']);$j++){
                    echo'
					<li class="list-group-item" style="'.couleurFondCommentaire($row[$i]['commentaire'][$j][5]).'">
                        <div class="row">
                            <div class="col-xs-2 col-md-1">
								'.afficher_avatar($row[$i]['commentaire'][$j][0], $lien, $emplacement, 40, 40).'
							</div>
                            <div class="col-xs-10 col-md-11">
                                <div>
                                    <div class="mic-info">
                                        Par: <a href="#">'.$row[$i]['commentaire'][$j][7].' '.$row[$i]['commentaire'][$j][8].'</a> à '.afficheDate($row[$i]['commentaire'][$j][6]).'
                                    </div>
                                </div>
                                <div class="comment-text">
 								'.$row[$i]['commentaire'][$j][4].' 
                                </div>
                            </div>
                        </div>
                    </li>';
				}
			}else echo'Cet utilisateur n\'a pas encore d\'avis';
			echo'</div></ul>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>';
			}
			
			if (! empty ( $_POST )) {
				$dest = - 1;
				
				foreach ( $_POST as $key => $entry ) {
					if (strstr ( $key, 'texte' )) {
						$dest = intval ( filter_var ( $key, FILTER_SANITIZE_NUMBER_INT ) );
					}
				}
				if($dest > -1){
					if($liste == 'demande') $titre = "Demande plus d'informations par rapport à la ".$liste.": ".$_POST ['titre' . $dest];
					else  $titre = "Demande plus d'informations par rapport à l'".$liste.": ".$_POST ['titre' . $dest];
					
					$message = '<i>'. $titre.'</i></br>'.$_POST ['texte' . $dest];
					envoieMessage($mysqli, $message, $dest);
				}
			}
			?>
</div>
		<div class="col-xs-6 col-md-6">
			<div class="col-lg-8"
				style="position: fixed; top: 170px; width: 495px; float: right; height: 550px; overflow: auto;">
					

    <?php
			
			$url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $adresse . "&key=AIzaSyDrNzSlMI6dRzhj-EwUhG6cRC3nnKnkbQY";
			$response = file_get_contents ( $url );
			$response = json_decode ( $response, true );
			if ((! empty ( $response ['results'] ))) {
				$lat = $response ['results'] [0] ['geometry'] ['location'] ['lat'];
				$long = $response ['results'] [0] ['geometry'] ['location'] ['lng'];
			}
			
			?><div id="map">
					<p>Veuillez patienter pendant le chargement de la carte...</p>
				</div>
				<div id="panel"></div>


				<!-- Include Javascript -->
				<script type="text/javascript" src="jquery/jquery.min.js"></script>
				<script type="text/javascript"
					src="jquery/jquery-ui-1.8.12.custom.min.js"></script>

				<script>

    var map;
    var panel;
    var initialize;
    var calculate;
    var direction;

    var lat = "<?php echo $lat;?>";
    var lng =  "<?php echo $long; ?>";

      
    initialize = function(lat,lng){
      var latLng = new google.maps.LatLng(lat, lng); // Correspond au coordonnées de Lille
      var myOptions = {
        zoom      : 14, // Zoom par défaut
        center    : latLng, // Coordonnées de départ de la carte de type latLng 
        mapTypeId : google.maps.MapTypeId.ROADMAP, // Type de carte, différentes valeurs possible HYBRID, ROADMAP, SATELLITE, TERRAIN
        maxZoom   : 20
      };
      
      map      = new google.maps.Map(document.getElementById('map'), myOptions);
      panel    = document.getElementById('panel');
      
      var marker = new google.maps.Marker({
        position : latLng,
        map      : map,
        title    : "Titre de la carte"
        //icon     : "marker_lille.gif" // Chemin de l'image du marqueur pour surcharger celui par défaut
      });
      
      var contentMarker = [
          '<div id="containerTabs">Vous êtes ici.</div>'
      ].join('');

      var infoWindow = new google.maps.InfoWindow({
        content  : contentMarker,
        position : latLng
      });
      
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.open(map,marker);
      });
      
      google.maps.event.addListener(infoWindow, 'domready', function(){ // infoWindow est biensûr notre info-bulle
        jQuery("#tabs").tabs();
      });
      
      
      direction = new google.maps.DirectionsRenderer({
        map   : map,
        panel : panel // Dom element pour afficher les instructions d'itinéraire
      });

    };

    calculate = function(adresse){
        origin      = document.getElementById('origin').value; // Le point départ
        destination =  adresse; //document.getElementById('destination').value; // Le point d'arrivé
        if(origin && destination){
            var request = {
                origin      : origin,
                destination : destination,
                travelMode  : google.maps.DirectionsTravelMode.DRIVING // Mode de conduite
            }
            var directionsService = new google.maps.DirectionsService(); // Service de calcul d'itinéraire
            directionsService.route(request, function(response, status){ // Envoie de la requête pour calculer le parcours
                if(status == google.maps.DirectionsStatus.OK){
                    direction.setDirections(response); // Trace l'itinéraire sur la carte et les différentes étapes du parcours
                }
            });
        }
    };
   
    initialize(lat,lng);

    </script>




			</div>
		</div>
	</div>

<?php
		}
	}
	
	?>
			
			
<?php
}

/**
 * Liste les offres de tout le monde
 *
 * @return id/auteur/ville/titre/offre/dateCreation/dateExpiration
 */
function liste_offres_valables($mysqli, $filtre) {
	$requete = liste_annonce($mysqli, '%', 'offre', $filtre);

	return $requete;
}

/**
 * Cette fonction permet de lister la liste des offres d'un utilisateur selon son id
 *
 * @param $id :
 *        	l'id de l'utilisateur à lister les offres
 * @param $enCours :
 *        	boolean qui renvoit mes offres en cours ou les cloturées/expirées
 */
function liste_mes_offresDemandes($mysqli, $type, $id, $enCours) {
	$requete = "SELECT tagNom,dateCreation,dateExpiration,a.annonceId,annonce,titre,vuReceveur 
			from annonces a 
			join annonce_tags at on at.annonceId = a.annonceId 
			join users_annonce ua on ua.annonceId = a.annonceId 
			join tags t on t.tagId = at.tagId 
			join users u on u.id = ua.userId 
			left join annonce_liste_attente ala on ala.annonceId = a.annonceId 
			where  u.id = $id and type = '".$type."' ";
	if ($enCours)
		$requete .= " and ( a.dateExpiration >= NOW() ";
	else
		$requete .= " and ( a.dateExpiration < NOW() ";
	
	if ($enCours)
		$requete .= " and !";
	else
		$requete .= "or ";
	$requete .= "exists(select * from annonces_cloturees where annonceId = a.annonceId)) group by a.annonceId order by dateCreation desc";
	$result = fetch ( $mysqli->query ( $requete ) );
	return $result;
}

/**
 *
 * @param unknown $mysql        	
 * @param unknown $id        	
 * @return unknown
 */
function liste_mes_offres_query($mysqli, $id, $filtre) {
	return liste_annonce($mysqli, $id, 'offre', $filtre);
}

/**
 * Liste les annonces d'un utilisateur
 * @param unknown $mysqli bdd ouverte
 * @param unknown $id int ou '%'
 * @param unknown $type 'offre'/'demande'/'%'
 * @return unknown
 */
function liste_annonce($mysqli, $id, $type, $filtre){
	$requete = $mysqli->query ( "SELECT annonces.annonceId AS id, users.nom AS auteur, users.ville AS ville, titre, annonce as advert, dateCreation, dateExpiration
				FROM annonces, users_annonce, users, annonce_tags
				WHERE annonces.annonceId = users_annonce.annonceId
				AND users_annonce.userId = users.id
				AND annonces.dateCloture IS NULL
				AND annonce_tags.tagId LIKE '" . $filtre . "'
				AND annonce_tags.annonceId = annonces.annonceId
				AND type LIKE '" . $type ."'
				AND users.id LIKE '" . $id . "'
				AND annonces.dateExpiration > NOW( ) " );
	return $requete;
}

/**
 *
 * @param unknown $mysql        	
 * @param unknown $id        	
 * @return unknown
 */
function liste_mes_demandes_query($mysqli, $id, $filtre) {
	return liste_annonce($mysqli, $id, 'demande', $filtre);
}

/**
 *
 * @param unknown $mysql        	
 * @param unknown $id        	
 * @return unknown
 */
function liste_demandes_valable($mysqli, $filtre) {
	return liste_annonce($mysqli, '%', 'demande', $filtre);
}

/**
 * Cette fonction permet de lister la liste des demandes d'un utilisateur selon son id
 *
 * @param $id: l'id
 *        	de l'utilisateur à lister les demandes
 */
function liste_mes_demandes($id) {
	$mysqli = connect_bdd ();
	$requete = "select demandes.demandeId,titre,demande,dateCreation, dateExpiration from demandes, users_demande where users_demande.userId = $id and demandes.demandeId=users_demande.demandeId AND demandes.dateCloture is NULL";
	$result = $mysqli->query ( $requete );
	$mysqli->close ();
	return ($result);
}

/**
 * Cette fonction permet de vérifier qu'une annonce existe et si son propriétaire est celui passé en paramètre
 *
 * @param $annonceId =
 *        	id de l'annonce à vérifier
 * @param $type =
 *        	string = offre ou demande
 * @param $userId =
 *        	id de la personne à vérifier
 * @return boolean = true si l'annonce existe et si userId en est propriétaire, false sinon
 */
function verif_OffreDemande_id($annonceId, $type, $userId) {
	$verif = true;
	
	$mysqli = connect_bdd ();
	$requete = "SELECT `annonceId` FROM `annonces` o WHERE annonceId = $annonceId and dateExpiration > NOW() and dateCloture is null";
	$temp = $mysqli->query ( $requete );
	$result = mysqli_num_rows ( $temp );
	if ($result != 1)
		$verif = false;
	
	$requete = "SELECT userId FROM `users_annonce` where `annonceId` = $annonceId";
	$temp = $mysqli->query ( $requete );
	$result = mysqli_fetch_array ( $temp );
	if ($result ['userId'] != $userId)
		$verif = false;
	$mysqli->close ();
	return $verif;
}

/**
 * Cette fonction permet de récupérer la date d'expiration d'une offre ou demande
 *
 * @param $id de
 *        	l'offre ou de la demande
 * @param $type "demande"
 *        	pour traiter une demande; "offre" pour traiter une offre
 * @return la date d'expiration de l'offre/demande
 */
function get_date_expi($id, $type) {
	$mysqli = connect_bdd ();
	$requete = "SELECT `dateExpiration` FROM `annonces` WHERE `annonceId` = $id";
	$temp = $mysqli->query ( $requete );
	$result = mysqli_fetch_array ( $temp );
	$mysqli->close ();
	return $result ['dateExpiration'];
}

/**
 * Cette fonction permet de récupérer le titre et la description et la dateCreation d'une demande/offre
 *
 * @param $demId :
 *        	id de la demande à récupérer
 * @return un tableau contenant le titre et la description de la demande
 */
function get_titre_descr_offre_demande($annonceId, $type) {
	$mysqli = connect_bdd ();
	$requete = "SELECT annonce,`titre`,dateCreation FROM `annonces` WHERE `annonceId` = $annonceId and type = '".$type."'";
	$temp = $mysqli->query ( $requete );
	$result = mysqli_fetch_array ( $temp );
	$mysqli->close ();
	return $result;
}

/**
 * Cette fonction permet de récupérer le type d'une demande/offre selon la chaine passée en paramètre
 *
 * @param $id :
 *        	id de l'offre/demande
 * @param $msg :
 *        	demande ou offre
 * @return tableau contenant le type d'offre
 */
function get_type($mysqli,$id) {
	$requete = "SELECT tagNom,tagId FROM tags WHERE tagId = (SELECT tagId FROM annonce_tags WHERE annonceId = $id)";
	$temp = $mysqli->query ( $requete );
	$result = mysqli_fetch_array ( $temp );
	return $result;
}
function get_id($mysqli,$tag) {
	$requete = "SELECT tagId FROM `tags` WHERE tagNom = (?)";
	$stmt = $mysqli->prepare ($requete);
	$stmt->bind_param ( "s", $tag );
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	echo $result;
	return $result;
	
}
function getAnnonceHasard($mysqli, $type) {
	$nb = fetch ( $mysqli->query ( "SELECT annonceId from annonces where type = '".$type."' " ) );
	if(empty($nb)) return 0;
	$randAnnonce = rand ( 0, sizeof ( $nb ) - 1 );
	return mysqli_fetch_array ( $mysqli->query ( "SELECT * from annonces a join annonce_tags at on at.annonceId = a.annonceId join tags t on t.tagId= at.tagId where a.annonceId = " . $nb [$randAnnonce] ['annonceId']." and a.type = '".$type."'" ) );
}
function annonceRealisee($mysqli, $annonceId, $type) {
	if (! ($stmt = $mysqli->prepare ( "SELECT COUNT(*) FROM annonces_attributions
JOIN annonces_cloturees ON annonces_attributions.annonceId = annonces_cloturees.annonceId
WHERE annonces_attributions.annonceId = (?);" )))
		echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
	if (! $stmt->bind_param ( "i", $annonceId ))
		echo "Echec lors du liage des paramètres : (" . $stmt->errno . ") " . $stmt->error;
	if (! $stmt->execute ())
		echo "Echec lors de l'exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	return $result;
}
function annonceRealiseePar($mysqli, $type, $annonceId) {
	if (! ($stmt = $mysqli->prepare ( "SELECT userId FROM annonces_attributions
JOIN annonces_cloturees ON annonces_attributions.annonceId = annonces_cloturees.annonceId
WHERE annonces_attributions.annonceId = (?);" )))
		echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
	if (! $stmt->bind_param ( "i", $annonceId ))
		echo "Echec lors du liage des paramètres : (" . $stmt->errno . ") " . $stmt->error;
	if (! $stmt->execute ())
		echo "Echec lors de l'exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	return $result;
}
function annonceDe($mysqli, $annonceId){
	$stmt = $mysqli->prepare ( "SELECT id from users u join users_annonce ua on ua.userId = u.id where ua.annonceId = (?)" );
	$stmt->bind_param ( "i", $annonceId );
	$stmt->execute ();
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	return $result;
}
function userNote($mysqli, $annonceId, $type, $userIdRecevant) {
	$stmt = $mysqli->prepare ( "SELECT COUNT(*) FROM users_points WHERE userIdRecevant = (?) AND annonceId = (?);" );
	$stmt->bind_param ( "ii", $userIdRecevant, $annonceId );
	$stmt->execute ();
	$stmt->bind_result ( $result );
	$stmt->fetch ();
	$stmt->close ();
	return $result;
}
function noterUser($mysqli, $annonceId, $type, $userIdDonnant, $userIdRecevant, $points) {
	$stmt = $mysqli->prepare ( "INSERT INTO users_points (userIdRecevant, points, userIdDonnant, datePoints, annonceId) 
				VALUES ((?),(?),(?),NOW(),(?));" );
	$stmt->bind_param ( "iiii", $userIdRecevant, $points, $userIdDonnant, $annonceId );
	$stmt->execute ();
	$stmt->close ();
}


function commenterUser($mysqli, $commentateurId, $commenteId,$annonceId, $typeAnnonce, $message,$avis){
	$stmt = $mysqli->prepare ( "INSERT INTO users_commentaires (commentateurId, commenteId,annonceId, typeAnnonce, message,avis,date)
				VALUES ((?),(?),(?),(?),(?),(?),NOW());" );
	$stmt->bind_param ( "iiissi", $commentateurId, $commenteId, $annonceId, $typeAnnonce, $message,$avis );
	$stmt->execute ();
	$stmt->close ();
}

?>

