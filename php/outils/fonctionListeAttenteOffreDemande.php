<?php
function listeAttenteOffreDemande($mysqli, $type) {
	$lien = racine_serveur () . '/images/avatars/';
	$emplacement = '../../images/avatars/';
	if (isset ( $_GET ['ido'] ) && ! empty ( $_GET ['ido'] ) && is_numeric ( $_GET ['ido'] )) {
		$annonceId = $_GET ['ido'];
		if (isInListeAttente ( $mysqli, $annonceId, $type )) {
			$stmt = $mysqli->prepare ( "SELECT titre, annonce, dateCreation, dateExpiration  FROM annonces WHERE annonceId = (?) and type = (?)" );
			$stmt->bind_param ( "is", $annonceId,$type );
			$stmt->execute ();
			$stmt->bind_result ( $col1, $col2, $col3, $col4 );
			$annonceAttente = array ();
			while ( $stmt->fetch () )
				array_push ( $annonceAttente, $col1, $col2, $col3, $col4 );
			
			echo "<h1 class='text-center'>La liste d'attente</h1>";
			echo '<div class="container">
					<div class="col-md-8">
						<h4><span class="label label-info">Votre ' . $type . '</span></h4>
							<div class="form-horizontal alert alert-info breadcrumb">
								<div class="form-group">		
									<p class="col-sm-3 strong">Titre</p>
								    <p class="col-sm-8">' . $annonceAttente [0] . '</p>
								</div>
				
								<div class="form-group">		
									<p class="col-sm-3 strong">Description</p>
								    <p class="col-sm-8">' . $annonceAttente [1] . '</p>
								</div>
				
								<div class="form-group">		
									<p class="col-sm-3 strong">Date de création</p>
								    <p class="col-sm-8">' . $annonceAttente [2] . '</p>
								</div>
				
								<div class="form-group">		
									<p class="col-sm-3 strong">Date d\'expiration</p>
								    <p class="col-sm-8">' . $annonceAttente [3] . " <i>[il reste " . calculJoursRestants ( $mysqli, $annonceId, $type ) . ']</i></p>
								</div>
							</div>
					</div>
				</div>
				';
			
			echo "<div class='container'><table class='sortable table table-bordered table-hover table-condensed'>
				<tr class='info'>
				<th>Prénom/Nom</th>
				<th>Age</th>
				<th>Proximité</th>
				<th>Premium</th>
				<th>Points</th>
				<th>Date d'ajout</th>
				<th>Envoyer un message</th>
				<th>Choisir</th></tr>";
			
			$stmt = $mysqli->prepare ( "SELECT users.id, CONCAT(users.prenom,' ', UPPER(users.nom)), 
			(SELECT DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(users.dateNaissance, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(users.dateNaissance, '00-%m-%d')) AS age), 
			annonce_liste_attente.dateEntreeListe, 
			CONCAT( users.codePost,'+',users.ville,'+', users.pays),users.rue,
			users.visibiliteAdresse
			FROM annonce_liste_attente 
			JOIN users ON annonce_liste_attente.userId = users.id
			WHERE annonce_liste_attente.annonceId = (?)
				ORDER BY annonce_liste_attente.dateEntreeListe;" );

			$stmt->bind_param ( "i", $annonceId );
			$stmt->execute ();
			$stmt->bind_result ( $col1, $col2, $col3, $col4, $col5, $col6,$col7 );
			$result = array ();
			while ( $stmt->fetch () )
				array_push ( $result, array (
						$col1,
						$col2,
						$col3,
						$col4,
						$col5,
						$col6,
						$col7
				) );
			$stmt->close ();
			$nbRelations = count ( $result );
			$monAdresse = "";
			if(!empty($_SESSION ['numero'])) $monAdresse.= $_SESSION ['numero'].' ';
			if(!empty($_SESSION ['rue'])) $monAdresse.= $_SESSION ['rue'].' ';
			$monAdresse .= $_SESSION ['codePost'] . "+" . $_SESSION ['ville'] . "+" . $_SESSION ['pays'];
			$monAdresse = str_replace ( "&#039", "+", $monAdresse );
			$monAdresse = str_replace ( " ", "+", $monAdresse );
			$listeAttente = array ();
			$bloque = annonceAttribue($mysqli, $annonceId, $type);
			// Affichage de chaques lignes.
			for($i = 0; $i < $nbRelations; $i ++) {
				$userId = $result [$i] [0];
				$nomPrenom = $result [$i] [1];
				$age = $result [$i] [2];
				$dateAjout = $result [$i] [3];
				$adresse = "";
				if(empty($result [$i] [5])) $adresse .= $result [$i] [5];
				$adresse .= $result [$i] [4];
				$adresse = str_replace ( "&#039", "+", $adresse );
				$adresse = str_replace ( " ", "+", $adresse );
				echo "<tr>";
				echo "<td>" . $nomPrenom . "</td>
				<td>" . $age . " ans</td>
				<td>" . round ( getDistance ( $adresse, $monAdresse ), 1 ) . " km</td>";
				if (userEstPremium ( $mysqli, $userId )) {
					echo "<td class='btn-success'><span class='glyphicon glyphicon-plus-sign'></span></td>";
				} else {
					echo "<td class='btn-warning'><span class='glyphicon glyphicon-minus-sign'></span></td>";
				}
				echo "<td>" . nbPointsUser ( $mysqli, $userId ) . " <button class='glyphicon glyphicon-comment'  data-toggle='modal' data-target='#modalCommentaire".$result [$i] [0]."'></button></td>
				<td>" . $dateAjout . "</td>
				<td><button data-toggle='modal' data-target='#modalMessage" . $userId . "'><span class='glyphicon glyphicon-envelope'></span></button></td>";
				
				$enAttente = enAttente ( $mysqli, $annonceId, $userId, $type );
				$attribution = attribue ( $mysqli, $annonceId, $userId, $type );
				
				if (($enAttente && !$attribution  && !$bloque)) {
					echo "<form class='form-horizontal' role='form' method='post' accept-charset='UTF-8'>
						<td><button type=submit name='choisir" . $userId . "'><span class='glyphicon glyphicon-hand-left'></span></button></td>
				</form>";
				} else if ($attribution) {
					echo "<td class = 'label-info' style='color:white;'>Choisi</td>";
				}
				else {
					echo "<td class = 'label-default' style='color:white;'>Ignoré</td>";
				}
				
				echo "<tr>";
				array_push ( $listeAttente, $userId );
				//
				//Modal des commentaires sur l'user
				//
				echo'<div class="modal fade" id="modalCommentaire' . $result [$i] [0] . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title" id="myModalLabel">Commentaire(s) à propos de '.$nomPrenom.'</h4>
				      </div>
				      <div class="modal-body">
				        <div class="panel panel-default widget">
				            <div class="panel-body">
				        		
				                <ul class="list-group"><div style="max-height: 300px; overflow-x: auto;">';
								$commentaires = afficheCommentaires($mysqli,$result [$i] [0]);
								if(!empty($commentaires)){
									for($z = 0;$z<sizeof($commentaires);$z++){
										echo'
									<li class="list-group-item" style="'.couleurFondCommentaire($commentaires[$z][5]).'">
				                        <div class="row">
				                            <div class="col-xs-2 col-md-1">
												'.afficher_avatar($commentaires[$z][0], $lien, $emplacement, 40, 40).'
											</div>
				                            <div class="col-xs-10 col-md-11">
				                                    <div class="mic-info">
				                                        Par: <a href="#">'.$commentaires[$z][7].' '.$commentaires[$z][8].'</a> à '.afficheDate($commentaires[$z][6]).'
				                                    </div>
				                                <div class="comment-text">
				 								'.$commentaires[$z][4].'
				                                </div>
				                            </div>
				                        </div>
				                    </li>';
									}
								}else echo'Cet utilisateur n\'a pas encore d\'avis';
								echo'</div></ul>
									
				            </div>
				        </div>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				      </div>
				    </div>
				  </div>
				</div>';
				//
				//fin du modal
				//
			}
			echo "</table></div>";
		} else
			redirect ( '/index.php' );
	} else
		redirect ( '/index.php' );
	
	$nbUsersListe = count ( $listeAttente );
	
	for($j = 0; $j < $nbUsersListe; $j ++) {
		$nomPrenom = getPrenomNom ( $mysqli, $listeAttente [$j] );
		echo '
<div class="modal fade" id="modalMessage' . $listeAttente [$j] . '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-envelope"></span> Envoie d\'un message à ' . $nomPrenom . '</h4>
      </div>
      <div class="modal-body">

			<form id="formTexte" class="form-horizontal" role="form" method="post" accept-charset="UTF-8" >
        	Titre:  '.$annonceAttente [0]  .'</br>
        	<input type="hidden" name="titre' . $listeAttente [$j] . '" value="' . $annonceAttente [0]. '">	
			<textarea style="resize:vertical" rows = "10" cols = "65" name = "texte' . $listeAttente [$j] . '" placeholder="Tapez votre message ici.Le titre de l\'annonce concernée y sera inclus automatiquement."></textarea>

      </div>
      <div class="modal-footer">
     
        <input name="envoyer" type="submit" class="btn btn-primary" value="Envoyer" onclick="envoieMessage(' . $listeAttente [$j] . ')">
        		</form>
      </div>
    </div>
  </div>
</div>';
	}
	if (! empty ( $_POST )) {
		$dest = - 1;
		$choix = - 1;
		
		foreach ( $_POST as $key => $entry ) {
			if (strstr ( $key, 'texte' )) {
				$dest = intval ( filter_var ( $key, FILTER_SANITIZE_NUMBER_INT ) );
			}
		}
		if ($dest > 0)
			if($type == 'demande') $titre = "Concerne votre mise en relation par rapport à ma ".$type.": ".$_POST ['titre' . $dest];
			else  $titre = "Concerne votre mise en relation par rapport à mon ".$type.": ".$_POST ['titre' . $dest];
				
			$message = '<i>'. $titre.'</i></br>'.$_POST ['texte' . $dest];
			
			envoieMessage ( $mysqli, $message, $dest );
		
		foreach ( $_POST as $key => $entry ) {
			if (strstr ( $key, 'choisir' )) {
				$choix = intval ( filter_var ( $key, FILTER_SANITIZE_NUMBER_INT ) );
			}
		}
		
		if ($choix > 0) {
			$annonceId = intval ( filter_var ( $_GET ['ido'], FILTER_SANITIZE_NUMBER_INT ) );
			
			acceptRelation($mysqli,$type,$annonceId,$choix);
			$titre = 'L4H - Un utilisateur vous a choisi.';
			$email = emailUser($mysqli,$choix);
			$nomPrenomCreateur = getPrenomNom($mysqli, $_SESSION['id']);
			$nomPrenomDestinataire = getPrenomNom($mysqli, $choix);
			$titreAnnonce = getTitreAnnonce($mysqli, $annonceId, $type);
			$description = getDescriptionAnnonce($mysqli, $annonceId);
			
			//Envoie du mail
			$message = "Bonjour $nomPrenomDestinataire,</br></br>";
			$message .= 'Vous avez été choisi par '.$nomPrenomCreateur.' </br>';
			$message .= 'pour la réalisation du service demandé. </br></br>';
			$message .= 'Petit rappel: </br>';
			$message .= 'Titre : '.$titreAnnonce.' </br>';
			$message .= 'Description : '.$description.'</br></br>';
			$message .= 'Pour plus d\'information, passez par notre site => <a href="http://l4h.be">http://l4h.be</a>.</br></br></br>';
			$message .= 'Nous vous remercions votre confiance et vous souhaitons une agréable journée, </br></br>';
			$message .= 'L\'équipe Looking for help. </br></br></br>';
			
			envoi_email($titre, $message, $email);
			
			
			redirect ("listeAttente".$type.".php?ido=".$annonceId);
		}
	}	
	?>
<script>
function envoieMessage(corres){
	texte = document.forms["formTexte"]["correspondant".corres].value; 
	if(texte.length != null){
		document.getElementById("formTexte").submit();
	}
}
</script>
</body>
<?php
}
?>