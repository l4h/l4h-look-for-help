<?php

/**
 * On vérifie que les champs des annonces en cours de création sont bien rempli
 * le tableau post contient titre,description,tag(tagId),expiration (date d'esxpiration) ,heure(d'expiration)
 * @param unknown $_POST
 * @param return renvoie un tableau de messages d'erreurs.
 */
function verifEmptyPostAnnonceCreation($type,$POST,&$err_message){
	$var_validite = true;
	if (empty ( $POST ['titre'] )){
		$err_message[] = TITRE_VIDE;
		$var_validite = false;
	}
	if (empty ( $POST ['description'] )){
		$err_message[] = DESCRIPTION_VIDE;
		$var_validite = false;
	}
	if (empty ( $POST ['tag'] )){
		if($type == 'demande') $err_message[] = CATEGORIE_DEMANDE;
		else $err_message[] = CATEGORIE_OFFRE;
		$var_validite = false;
	}
	if (empty ( $POST ['expiration'] )){
		$err_message[] = DATE_EXPIRATION_VIDE;
		$var_validite = false;
	}
	if (empty ( $POST ['heure'] )){
		$err_message[] = HEURE_EXPIRATION_VIDE;
		$var_validite = false;
	}
	return $var_validite;
}
	
	
/**
 *on compare les dates pour voir si l'esxpiration est moins récente que today
 * @param unknown $mysqli
 * @param unknown $today
 * @param unknown $dateExpiration
 * @param unknown $date_limit 
 * @param unknown $err_message tableau de message d'erreurs
 * @return boolean true ou false
 */
function compareDateAnnonceCreation($mysqli,$today,$dateExpiration,$date_limit,&$err_message){
	if(strtotime($dateExpiration) < strtotime($today)){
		$err_message[] = DATE_EXPIRATION_NON_VALIDE;
		if(!userEstPremium($mysqli,$_SESSION['id'])) $err_message[] = DELAI_14_JOURS;
		return false;
	}
	return true;
}
	
/**
 * on vérifie si la création d'une annonce est possible et donc si l'user n'est pas premium, s'il a pas une annonc déjà en cours
 * @param unknown $mysqli
 * @param unknown $id id de l'user
 * @param unknown $type offre ou demande
 * @return boolean true ou false
 */
function verifPossibiliteCreationAnnonce($mysqli,$id,$type){
	if(!userEstPremium($mysqli,$id)){
		if(nbOffreDemandeEnCours($mysqli,$id,$type) > 0){
			return  false;
		}
	}
	return true;
}

?>