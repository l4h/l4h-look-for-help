<?php 
//////////////////////////////
/////////Inscription//////////
//////////////////////////////

/**
 * recheche d'un user ayant le mail donné
 * @param unknown $mysqli
 * @param string $email email de l'utilisateur pour qui on veut connaitre l'id
 * @return mysqli->query
 */
function select_id_users($mysqli,$email){
	$query = $mysqli->query ( "SELECT id FROM users WHERE email = '$email'" );
	return $query;
}

function emailUser($mysqli,$userId){
	$select  = mysqli_fetch_array($mysqli->query("SELECT email from users where id = $userId"));
	return $select['email'];
}

function getDescriptionAnnonce($mysqli, $annonceId){
	$select  = mysqli_fetch_array($mysqli->query("SELECT annonce from annonces where annonceId = $annonceId"));
	return $select['annonce'];
}

function getIdUser($mysqli,$annonceId){
	$select = mysqli_fetch_array($mysqli->query("SELECT userId from users_annonce where annonceId = $annonceId "));
	return $select['userId'];
}

/**
 * insertion dans la bdd des infos users + users_profil + activation code pour l'inscription
 * @param unknown $mysqli
 * @param string $nom
 * @param string $prenom
 * @param string $dateNaiss
 * @param string $rue
 * @param string $numero
 * @param string $codePost
 * @param string $ville
 * @param string $pays
 * @param string $visibiliteAdresse
 * @param string $newsletter
 * @param string $email
 * @param string $gsm
 * @param string $pwd
 * @param string $salt
 * @param string $activationCode
 */
function creation_new_user($mysqli,$nom,$prenom,$dateNaiss,$rue,$numero,$codePost,$ville,$pays,$visibiliteAdresse,$newsletter,$email,$gsm,$pwd,$salt,$activationCode){
	if(empty($numero)&&empty($rue)){
		$stmt = $mysqli->prepare ( "INSERT INTO users ( nom,prenom,dateNaissance,codePost,ville,pays,visibiliteAdresse,newsletter,email,gsm,pwd,dateInscription,salt)
			VALUES ((?),(?),(?),(?),(?),(?),'$visibiliteAdresse','$newsletter',(?),(?),(?),NOW(),(?))");
		$stmt->bind_param ( "ssssssssss",$nom,$prenom,$dateNaiss,$codePost,$ville,$pays,$email,$gsm,$pwd,$salt);
	}
	else if(empty($numero)&&!empty($rue)){
		$stmt = $mysqli->prepare ( "INSERT INTO users ( nom,prenom,dateNaissance,rue,codePost,ville,pays,visibiliteAdresse,newsletter,email,gsm,pwd,dateInscription,salt)
				VALUES ((?),(?),(?),(?),(?),(?),(?),'$visibiliteAdresse','$newsletter',(?),(?),(?),NOW(),(?))");
		$stmt->bind_param ( "sssssssssss",$nom,$prenom,$dateNaiss,$rue,$codePost,$ville,$pays,$email,$gsm,$pwd,$salt);
		
	}else if (empty($rue)&&!empty($numero)){
		$stmt = $mysqli->prepare ( "INSERT INTO users ( nom,prenom,dateNaissance,numero,codePost,ville,pays,visibiliteAdresse,newsletter,email,gsm,pwd,dateInscription,salt)
				VALUES ((?),(?),(?),(?),(?),(?),(?),'$visibiliteAdresse','$newsletter',(?),(?),(?),NOW(),(?))");
		$stmt->bind_param ( "sssssssssss",$nom,$prenom,$dateNaiss,$numero,$codePost,$ville,$pays,$email,$gsm,$pwd,$salt);
		
	}else{
		$stmt = $mysqli->prepare ( "INSERT INTO users ( nom,prenom,dateNaissance,rue,numero,codePost,ville,pays,visibiliteAdresse,newsletter,email,gsm,pwd,dateInscription,salt)
				VALUES ((?),(?),(?),(?),(?),(?),(?),(?),'$visibiliteAdresse','$newsletter',(?),(?),(?),NOW(),(?))");
		$stmt->bind_param ( "ssssssssssss",$nom,$prenom,$dateNaiss,$rue,$numero,$codePost,$ville,$pays,$email,$gsm,$pwd,$salt);
	}
	$stmt->execute ();
	$stmt->close();
	
	// recherche de l'id du user créé
	$id = mysqli_fetch_array ( select_id_users($mysqli,$email) );
	// création de lien dans users_profil
	$mysqli->query ( " INSERT INTO users_profil (userId,profilId) values (".$id['id'].",5)" );
	
	$mysqli->query ( "INSERT INTO activations (userId,activationCode) VALUES (".$id['id'].",'$activationCode')" );

	
}


//////////////////////////////
//////////Connexion///////////
//////////////////////////////




/**
 * Recherche de toutes les données d'un utilisateur via son email
 * @return mysqli->query
 */
function select_all_from_users($mysqli,$email){
	$query= $mysqli->query ( " SELECT * FROM users
														JOIN users_profil ON users.id = users_profil.userId
														JOIN profils ON users_profil.profilId = profils.profilId
														WHERE email='".$email."';" );
	return $query;
}


function select_max_profilValue($mysqli,$id){
	$query = $mysqli->query ( " SELECT max(valeur) as valeur 
								from users_profil, profils
			 					where userId='$id'
								AND users_profil.profilId = profils.profilId");
	return $query;
}
/**
 * recherche du profilId correspondant à (user en activation, user banni,...)
 * @param unknown $mysqli
 * @param unknown $id de l'user concerné
 * @return $mysqli->query
 */
function select_max_profilId($mysqli,$id){
	$query = $mysqli->query ( " SELECT max(profilId) as profilId
			from users_profil
			where userId='$id'");
			return $query;
}

/**
 * recherche de activationCode d'un utilisateur s'il y en a un
 * @param unknown $mysqli
 * @param unknown $id de l'user concerné
 * @return $mysqli->query
 */
function select_activationCode($mysqli,$id){
	$query = $mysqli->query ( " SELECT activationCode from activations where userId='$id'" );
	return $query;
}


/**
 * suppression du profilId 4 
 * @param unknown $mysqli
 * @param unknown $id de l'ustilisateur concerné
 */
function delete_profilId($mysqli,$id){
	$mysqli->query ( "DELETE from users_profil where userId='$id' and profilId = 4");
}

/**
 * réactivation de l'utilisateur en retirant l'activationCode de la table et suppression du profilId = 4
 * @param unknown $mysqli
 * @param int  $id de l'ustilisateur concerné
 * @param string $activationCode code d'activation de l'user
 */
function reactivation_user($mysqli,$id,$activationCode){
	delete_profilId($mysqli,$id);
	$mysqli->query ( "DELETE from activations where userId='$id' and activationCode = '" . $activationCode . "' and activationCode LIKE 'reac%';" );
}

// s'il y a un activationCode dans l'url on regarde s'il est 'réac' ou 'acti' et valable
//on passe en paramètre l'id du user et le type d'activation qui est acti ou reac
function select_all_activations($mysqli,$id,$activationCode,$typeActivation){
	$query = $mysqli->query("SELECT * from activations where userId='$id' and activationCode = '" . $activationCode . "' and activationCode LIKE '".$typeActivation."';" );
	return $query;
}


// MAJ du profilId lors de l'activation
function update_profilId($mysqli,$id){
	$mysqli->query ( "UPDATE users_profil SET  profilId = 3 WHERE profilId = 5 and userId = '$id'");
}

/**
 * On donne 15 jours de premium gratuit lors de l'activation
 */
function updateUserPremium($mysqli,$nbJours,$userId){
	$mysqli->query("UPDATE users SET dateDebutPremium = NOW(), nbJoursPremium = $nbJours  where id = $userId");
}

//suppression du code d'activation ou react.. dans la table
function delete_activations($mysqli,$id,$activationCode,$typeActivation){
	$mysqli->query ( "delete from activations where userId='$id' and activationCode LIKE '".$typeActivation."';" );
}

//maj de la date d'activation
function maj_dateActivation($mysqli,$id){
	$mysqli->query ( "update users set dateActivation = NOW() where id='$id'" );
}


//maj du mot de passe et du salt
function maj_pwd_salt($mysqli,$id,$pwd,$salt){
	$stmt = $mysqli->prepare ( "update users set  salt = (?), pwd = (?) where id='$id'" );
	$stmt->bind_param ( "ss", $salt, $pwd );
	$stmt->execute ();
	$stmt->close();
	//$mysqli->query ( "update users set  salt = '" .$salt. "', pwd = '".$pwd."' where id='$id'" );
}

//verifier si le compte est désactiver
function isDesactive($id){
	$mysqli = connect_bdd();
	$query= $mysqli->query ( " SELECT desactive FROM users where id = '$id'");
	$isDesactive = mysqli_fetch_array($query);
	$mysqli->close();
	return $isDesactive['desactive'];
}


//////////////////////////////
///////Modifier Profil////////
//////////////////////////////


//Maj du nom
function update_nom($mysqli,$id,$nom){
	//$mysqli->query( " UPDATE users SET nom='$nom' WHERE id='$id'");
	$stmt = $mysqli->prepare ( "UPDATE users SET nom=(?) WHERE id=(?)" );
	$stmt->bind_param ( "si", $nom, $id );
	$stmt->execute ();
	$stmt->close();
}

//Maj du prenom
function update_prenom($mysqli,$id,$prenom){
	//$mysqli->query( " UPDATE users SET prenom='$prenom' WHERE id='$id'");
	$stmt = $mysqli->prepare ( "UPDATE users SET prenom=(?) WHERE id=(?)" );
	$stmt->bind_param ( "si", $prenom, $id );
	$stmt->execute ();
	$stmt->close();
}

//Maj du gsm
function update_gsm($mysqli,$id,$gsm){
	//$mysqli->query( " UPDATE users SET gsm='$gsm' WHERE id='$id'");
	$stmt = $mysqli->prepare ( "UPDATE users SET gsm=(?) WHERE id=(?)" );
	$stmt->bind_param ( "si", $gsm, $id );
	$stmt->execute ();
	$stmt->close();
}



//Maj du mail
function update_email($mysqli,$id,$email){
	//$mysqli->query( " UPDATE users SET email='$email' WHERE id='$id'");
	$stmt = $mysqli->prepare ( "UPDATE users SET email=(?) WHERE id=(?)" );
	$stmt->bind_param ( "si", $email, $id );
	$stmt->execute ();
	$stmt->close();
}


//Maj de la rue
function update_rue($mysqli,$id,$rue){
	if($rue == null) $mysqli->query( " UPDATE users SET rue = null WHERE id='$id'");
	else{
		//$mysqli->query( " UPDATE users SET rue='$rue' WHERE id='$id'");
		$stmt = $mysqli->prepare ( "UPDATE users SET rue=(?) WHERE id=(?)" );
		$stmt->bind_param ( "si", $rue, $id );
		$stmt->execute ();
		$stmt->close();
	}
}

//Maj du numero
function update_numero($mysqli,$id,$numero){
	if($numero == null) {
		$mysqli->query( " UPDATE users SET numero = null WHERE id='$id'");
	}
	else{
		//$mysqli->query( " UPDATE users SET numero='$numero' WHERE id='$id'");
		$stmt = $mysqli->prepare ( "UPDATE users SET numero=(?) WHERE id=(?)" );
		$stmt->bind_param ( "si", $numero, $id );
		$stmt->execute ();
		$stmt->close();
	}
}


//Maj du code postal
function update_codePost($mysqli,$id,$codePost){
	//$mysqli->query( " UPDATE users SET codePost='$codePost' WHERE id='$id'");
	$stmt = $mysqli->prepare ( "UPDATE users SET codePost=(?) WHERE id=(?)" );
	$stmt->bind_param ( "ii", $codePost, $id );
	$stmt->execute ();
	$stmt->close();
}

//Maj de la ville
function update_ville($mysqli,$id,$ville){
	//$mysqli->query( " UPDATE users SET ville='$ville' WHERE id='$id'");
	$stmt = $mysqli->prepare ( "UPDATE users SET ville=(?) WHERE id=(?)" );
	$stmt->bind_param ( "si", $ville, $id );
	$stmt->execute ();
	$stmt->close();
}

//Maj du pays
function update_pays($mysqli,$id,$pays){
	//$mysqli->query( " UPDATE users SET pays='$pays' WHERE id='$id'");
	$stmt = $mysqli->prepare ( "UPDATE users SET pays=(?) WHERE id=(?)" );
	$stmt->bind_param ( "si", $pays, $id );
	$stmt->execute ();
	$stmt->close();
}

//Maj de la visibilité adresse
function update_visibiliteAdresse($mysqli,$id,$visibiliteAdresse){
	//$mysqli->query( " UPDATE users SET visibiliteAdresse='$visibiliteAdresse' WHERE id='$id'");
	$stmt = $mysqli->prepare ( "UPDATE users SET visibiliteAdresse=(?) WHERE id=(?)" );
	$stmt->bind_param ( "ii", $visibiliteAdresse, $id );
	$stmt->execute ();
	$stmt->close();
}

/**
 * activation , désactivation du compte
 * @param unknown $mysqli
 * @param unknown $id id de l'user a désactiver
 * @param unknown $val 1 = désactivé 0 = actif
 */
function activer_desactiver($mysqli,$id,$val){
	$mysqli->query( " UPDATE users SET desactive = '$val' WHERE id='$id'");
	$stmt = $mysqli->prepare ( "UPDATE users SET desactive=(?) WHERE id=(?)" );
	$stmt->bind_param ( "ii", $val, $id );
	$stmt->execute ();
	$stmt->close();
}

/////////////////////////
/////offres///demandes///
/////////////////////////
/**
 * liste les offres et demandes
 * @param unknown $mysqli
 * @param unknown $liste on passe 'offre' ou 'demande' en paramètre en fonction de ce qu'on veut lister
 * @param unknown $tab on passe les condition de listage (recherche avancées ) sous forme de tableau ($_POST) 
 * ce tableau peut contenir 
 * tag = int tagId de la catégorie
 * recherche = le mot clé qui permet la recherche dans les titres et descriptions
 * proximite = un nombre ex 42 pour 42 km
 * reputation = le nombre de points de réputation > ou = a ce qu'on veut afficher
 * premium = un nombre pour rechercher tous les users (0), les premiums (1), les non premiums (2)
 * auteur = un nom de l'auteur qu'on recherche
 * tri = string "distance" ,"dateCreation","dateExpiration"
 * @param unknown $val false = listage pour les users, true = listage admin
 * @param unknown $idUserEnCours  id de l'utilisateur qui ne doit pas voir ses offres/demandes  , 0 si tout afficher(administration)
 */

function liste_offres_demandes($mysqli,$type,$tab,$val,$idUserEnCours){
	$param1 = '';
	$nb = '';
	$requete = 'SELECT nom,prenom,tagNom,dateCreation,dateExpiration,a.annonceId,annonce,numero,rue,codePost,ville,pays,id,titre,visibiliteAdresse '; 
	$prepare = $requete;
	if(!$val) $requete.=',SUM(up.points)as nbPoints ';
	
	$requete .=' from annonces a
				 join users_annonce ua on ua.annonceId = a.annonceId
				 join users u on u.id = ua.userId
				 join annonce_tags at on at.annonceId = a.annonceId 
				 join tags t on t.tagId = at.tagId
				 left join users_points up on up.userIdRecevant = u.id where a.type = "'.$type.'" ';
	$prepare = $requete;
	if (! empty ( $tab['tag'] ) && $tab['tag'] != '-1') {
		$tagId = $tab['tag'];
		$requete .= " and t.tagId = $tagId ";
		$prepare .= " and t.tagId = $tagId ";
	}
	if (! empty ( $tab['recherche'] )) {
		$recherche = $tab['recherche'];
		$requete .= " and ( a.annonce like '%$recherche%' || a.titre like '%$recherche%') ";
		$prepare .= " and ( a.annonce like (?) || a.titre like (?)) ";
		$param1 .= 'ss';
		$recherche = "%$recherche%";
		$nb .= '1';
	}
	if (! empty ( $tab['auteur'] )) {
		$auteur = $tab['auteur'];

		$requete .= " and ( u.nom like '%$auteur%' || u.prenom like '%$auteur%') ";
		$prepare .= " and ( u.nom like (?) || u.prenom like (?)) ";
		$param1 .= 'ss';
		$auteur = "%$auteur%";
		$nb .= '2';
	}
	if (! empty ( $tab['premium'] )) {
		$premium = $tab['premium'];
		if($premium == 1) {
			$requete .= " and ((u.dateDebutPremium + INTERVAL u.nbJoursPremium DAY ) >= NOW()) ";
			$prepare .= " and ((u.dateDebutPremium + INTERVAL u.nbJoursPremium DAY ) >= NOW()) ";
		}
		if($premium == 2){
			$requete .= " and ((u.dateDebutPremium + INTERVAL u.nbJoursPremium DAY ) < NOW() || u.dateDebutPremium is null) ";
			$prepare .= " and ((u.dateDebutPremium + INTERVAL u.nbJoursPremium DAY ) < NOW() || u.dateDebutPremium is null) ";
		}
	}
	if($val){
		if(empty($tab['expiration'])){
			$requete .= ' and a.dateExpiration >= NOW() ';
			$prepare .= ' and a.dateExpiration >= NOW() ';
				
			$requete .= ' and a.dateCloture is NULL ';
			$prepare .= ' and a.dateCloture is NULL ';
		}
		else{
			$requete .= ' and ( a.dateExpiration < NOW() ';
			$prepare .= ' and ( a.dateExpiration < NOW() ';
		
			$requete .= ' or a.dateCloture is NOT NULL ) ';
			$prepare .= ' or a.dateCloture is NOT NULL )';
		}
	}
	$groupby = false;		
	if(!$val) {
		$requete .= ' and a.dateExpiration >= NOW() ';
		$prepare .= ' and a.dateExpiration >= NOW() ';
		
		if(empty($_SESSION['afficherMesAnnonces']) || $_SESSION['afficherMesAnnonces'] == null){
			$requete .= ' and u.id != ' . $idUserEnCours.' ';
			$prepare .= ' and u.id != ' . $idUserEnCours.' ';
		}
		$requete .= ' and ua.userId = u.id and u.desactive != 1 ';
		$prepare .= ' and ua.userId = u.id and u.desactive != 1 ';
		$requete .= ' and a.dateCloture is NULL ';
		$prepare .= ' and a.dateCloture is NULL ';
		$requete .= '  and a.annonceId not in (SELECT annonceId from annonces_attributions ) ';
		$prepare .= '  and a.annonceId not in (SELECT annonceId from annonces_attributions ) ';
	}

	if(!empty($tab['reputation'])){
		$nbPointsReputMin = $tab['reputation'];
		$groupby = true;
		$requete.='  GROUP BY a.annonceId ';
		$prepare .= '  GROUP BY a.annonceId ';
		$requete = 'SELECT * from ('.$requete.') as b where b.nbPoints >= '.$nbPointsReputMin ;
		$prepare = 'SELECT * from ('.$prepare.') as b where b.nbPoints >= (?)' ;
		$param1 .= 'i';
		$nb .= '3';
	}
	if(!$groupby){
		$requete.='  GROUP BY a.annonceId';
		$prepare .= '  GROUP BY a.annonceId';
	}
	//on trie par date d'expiration ou par date de création  la distance se fait via un autre algo
	if (! empty ( $tab['tri'] ) && $tab['tri'] != 'distance' ) {
		$tri = $tab['tri'];
		$requete .= " ORDER BY $tri asc ";
		$prepare .= " ORDER BY $tri asc ";
	}
	if(empty ( $tab['tri'] )){
		$requete .= " ORDER BY dateCreation desc ";
		$prepare .= " ORDER BY dateCreation desc ";
	}
	if (! ($stmt = $mysqli->prepare ( $prepare )))
		return 0;

	if($nb == 1) 	$req_param = $stmt->bind_param ( $param1,$recherche,$recherche );
	else if($nb == 2)	$req_param = $stmt->bind_param ( $param1,$auteur ,$auteur );
	else if($nb == 3)	$req_param = $stmt->bind_param ( $param1,$nbPointsReputMin );
	else if($nb == 12)	$req_param = $stmt->bind_param ( $param1,$recherche,$recherche, $auteur ,$auteur );
	else if($nb == 23)	$req_param = $stmt->bind_param ( $param1, $auteur ,$auteur,$nbPointsReputMin );
	else if($nb == 13)	$req_param = $stmt->bind_param ( $param1,$recherche,$recherche,$nbPointsReputMin );
	else if($nb == 123)	$req_param = $stmt->bind_param ( $param1,$recherche,$recherche, $auteur ,$auteur,$nbPointsReputMin );
	else $req_param = 1;							
	
	if (! $req_param ) return 0;
	
	if (! $stmt->execute ()) return 0;
	$result = fetch($stmt);
	$stmt->close();
	return $result;
}


/////////////////////////
///annonces//création////
/////////////////////////

function insertNewAnnonce($mysqli,$type,$description,$titre,$today,$expiration,$tagId){
	$stmt = $mysqli->prepare ( "INSERT INTO annonces (annonce, titre, dateCreation,dateExpiration,type) VALUES ((?),(?),(?),(?),(?))");
	$stmt->bind_param ( "sssss",$description,$titre,$today,$expiration,$type );
	$stmt->execute ();
	$stmt->close();
	
	$annonceId = $mysqli->insert_id;
	
	$stmt = $mysqli->prepare ( "INSERT INTO annonce_tags (annonceId,tagId) VALUES (".$annonceId.",(?))");
	$stmt->bind_param ( "i",$tagId );
	$stmt->execute ();
	$stmt->close();
	
	$mysqli->query( "INSERT INTO users_annonce (userId,annonceId) VALUES (".$_SESSION['id'].",".$annonceId." )");
}

function nbAnnonceEnCours($mysqli,$type,$id){
	$nb = $mysqli->query("Select * from users_annonce ud join annonces d on (ud.annonceId = d.annonceId) where ud.userId = '$id' and type = '".$type."'  and (d.dateCloture is NULL and d.dateExpiration > NOW())or (d.dateExpiration is NULL and d.dateCloture is NULL)");
	return mysqli_num_rows($nb);
}

/////////////////////////
//Annonces modification//
/////////////////////////

function updateAnnonce($mysqli,$type,$description,$titre,$today,$expiration,$tagId,$annonceId){
	$stmt = $mysqli->prepare ( "UPDATE `annonces` SET `annonce`=(?),`titre`=(?),`dateExpiration` = (?) WHERE annonceId = $annonceId and type = '".$type."' ");
	$stmt->bind_param ( "sss",$description,$titre,$expiration );
	$stmt->execute ();
	$stmt->close();
	//$tagId = get_id($mysqli,$tagId);	
	$requete = "UPDATE `annonce_tags` SET tagId= $tagId WHERE `annonceId` = $annonceId";
	$mysqli->query($requete);
}

/////////////////////////
//////////header/////////
/////////////////////////

function nbNewAnnonceRelation($mysqli,$userId,$type){
	$result = mysqli_fetch_array($mysqli->query("SELECT count(annonceId) as nb FROM `annonce_liste_attente` WHERE vuReceveur = false and annonceId in (SELECT ua.annonceId from users_annonce ua join annonces a on a.annonceId = ua.annonceId where userId = $userId and a.type = '".$type."')"));
	if($result['nb'] > 0) return $result['nb'];
	else return "";
}

function vuAnnonceRelationCreateur($mysqli,$annonceId,$userId,$type){
	$mysqli->query("UPDATE annonce_liste_attente SET vuCreateur = true  WHERE vuCreateur = false and annonceId = $annonceId and userId = $userId ");
}

function vuAnnonceRelationReceveur($mysqli,$annonceId,$userId,$type){
	$mysqli->query("UPDATE annonce_liste_attente SET vuReceveur = true  WHERE vuReceveur = false and annonceId = $annonceId ");
}

/////////////////////////
///////reponseAnnonce////
/////////////////////////


function insertAnnonceListeAttente($mysqli,$type,$annonceId,$userId){
	$stmt = $mysqli->prepare ( "INSERT INTO annonce_liste_attente (annonceId,userId,dateEntreeListe) VALUES ((?), (?), NOW())" );
	$stmt->bind_param ( "ii", $annonceId, $userId );
	$stmt->execute ();
	$stmt->close();
}

function notifNewAnnonceListeAttente($mysqli,$annonceId){
	$stmt = $mysqli->prepare ( "SELECT vuCreateur from annonce_liste_attente where annonceId = (?)");
	$stmt->bind_param ( "i", $annonceId );
	$stmt->execute ();
	$stmt->bind_result ( $result );
	$stmt->fetch();
	$stmt->close();
	return $result;
}

function nbNewNotifRelation($mysqli,$userId,$type){
	$result = mysqli_fetch_array($mysqli->query ( "SELECT count(vuCreateur) as nbNonVuCreateur from annonce_liste_attente where userId = $userId and vuCreateur is false and annonceId in (SELECT annonceId from annonces where type = '".$type."')"));
	if ($result['nbNonVuCreateur'] == 0) return "";
	return $result['nbNonVuCreateur'];
}


?>