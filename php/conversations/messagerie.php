<?php
include ('../header.php');
if (! connected ())
	redirect ( '/index.php' );
?>
<html>
<body>
	<br />
	<br />
	<br />
	<br />
	<h1 class='text-center'>Messagerie</h1>

	<link
		href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css"
		rel="stylesheet">

	<div class="container">
		<div class="col-md-12">

			<div class="conversation-wrap col-xs-3">
<?php

$mysqli = connect_bdd ();
$mysqli->set_charset ( "utf8" );
$conv = getConversations ( $mysqli, $_SESSION ['id'] );
$nbConv = count ( $conv );

$lien = racine_serveur () . '/images/avatars/';
$emplacement = '../../images/avatars/';

echo '<form enctype="multipart/form-data" method="post">';
for($i = 0; $i < $nbConv; $i ++) {
	if ($conv [$i] [1] == $_SESSION ['id']) {
		$correspondant = $conv [$i] [2];
	} else {
		$correspondant = $conv [$i] [1];
	}
	if (nouveauxMessagesConversation ( $mysqli, $_SESSION ['id'], $conv [$i] [0] )) {
		echo '<div class="media conversation" style="background-color:yellow;">';
		echo "<span class='glyphicon glyphicon-envelope clignotant'></span> Nouveau(x) !";
	} else {
		echo '<div class="media conversation" >';
	}
	echo '				<a class="pull-left"><button type="submit" name="choix' . $conv [$i] [0] . '">';
	echo afficher_avatar ( $correspondant, $lien, $emplacement, 64, 64 );
	echo '				</button></a>
					<div class="media-body">
						<br/><h5 class="media-heading">' . getPrenomNom ( $mysqli, $correspondant ) . '</h5>
					</div>
				</div>';
}
?>
			</div>
<?php
if (! empty ( $_POST ) || ! empty ( $_GET ['idc'] )) {
	if (! empty ( $_GET ['idc'] ) && !conversationCorrecte ( $mysqli, $_GET ['idc'], $_SESSION ['id'] )) {
		redirect("messagerie.php");
	}
		if (! empty ( $_GET ['idc'] )) {
			$convId = intval ( filter_var ( $_GET ['idc'], FILTER_SANITIZE_NUMBER_INT ) );
		}
		if (! empty ( $_POST )) {
			foreach ( $_POST as $key => $entry ) {
				if (strstr ( $key, 'choix' )) {
					$convId = intval ( filter_var ( $key, FILTER_SANITIZE_NUMBER_INT ) );
					$mysqli->close ();
					redirect ( "messagerie.php?idc=" . $convId );
				}
			}
		}
		// Affichage de la conversation
		$messages = getMessages ( $mysqli, $convId );
		$nbMessages = count ( $messages );
		
		echo '<div class="message-wrap col-xs-9">';
		echo'<div id="chat">';
		for($a = 0; $a < $nbMessages; $a ++) {
			$auteur = $messages [$a] [0];
			$texte = $messages [$a] [1];
			$date = $messages [$a] [2];
			
			if ($auteur != $_SESSION ['id']) {
				$mysqli->query ( "UPDATE conversations_messages SET vu = TRUE WHERE conversationId = " . $convId . " AND userId = " . $auteur . ";" );
			}
			echo '
			<div class="media msg">
			<a class="pull-left">' . afficher_avatar ( $auteur, $lien, $emplacement, 64, 64 ) . '</a>
			<div class="media-body">
			<small class="pull-right time"><i class="fa fa-clock-o"></i>
			' . $date . '</small>
	
			<h5 class="media-heading strong">' . getPrenomNom ( $mysqli, $auteur ) . '</h5>
			<small class="col-lg-10">' . $texte . '</small>
			</div>
			</div>';
		}
		echo '</div>';
		echo ' <div class="send-wrap ">
		
			<textarea class="form-control send-message" rows="3"
			placeholder="Ecrire une réponse..." name="input' . $convId . '"></textarea>
	
			</div>
			<div class="btn-panel">
			<button name="envoie' . $convId . '" type="submit" class="col-lg-4 text-right btn btn-info pull-right" 
							style="color: black;">Envoyer</button>
			</div>
			</div>
			</div>
			</div>';
}
echo "</form>";
// Envoie d'un message
if (isset ( $convId ) && ! empty ( $_POST ['input' . $convId] ) && isset ( $_POST ['envoie' . $convId] )) { // Envoie d'un message
	$texte = $_POST ['input' . $convId];
	
	if (! ($stmt = $mysqli->prepare ( "INSERT INTO conversations_messages (conversationId,userId,message, dateCreation,vu) VALUES ((?),(?),(?), NOW(), FALSE);" )))
		echo "Echec de la préparation : (" . $mysqli->errno . ") " . $mysqli->error;
	$monId = intval ( $_SESSION ['id'] );
	if (! $stmt->bind_param ( "iis", $convId, $monId, $texte ))
		echo "Echec lors du liage des paramètres : (" . $stmt->errno . ") " . $stmt->error;
	if (! $stmt->execute ())
		echo "Echec lors de l'exécution de la requête : (" . $stmt->errno . ") " . $stmt->error;
	$stmt->close ();
	redirect ( "messagerie.php?idc=" . $convId );
}
$mysqli->close ();
?>
</div>
</div>
		<script>
			var x = document.getElementById('chat');
			x.scrollTop = x.scrollHeight;
		</script>
</body>
<?php
include ('../footer.php');
?>