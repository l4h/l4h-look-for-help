<?php 
if (! connected () || ! admin ())
	redirect('/index.php'); 

?>
<div class="container">
	<div class="row">

        <h3><span class="label label-primary">Newsletter</span></h3></br>
<?php 
if(isset($_POST['valider']))
	if($_POST['valider'] == "oui"){

$nbmail = 0;
$mysqli = connect_bdd();
$tab_users = $mysqli->query('SELECT newsletter,id,email,rue,numero,codePost,ville,pays,visibiliteAdresse from users where newsletter is true and desactive is false ');
$tab_users= fetch($tab_users);
$nbCat = nbCategorie($mysqli);

for($z=0;$z<sizeof($tab_users);$z++){
$message = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Template mailing Alsacreations</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style type="text/css">
    /* Fonts and Content */
    body, td { font-family: \'Helvetica Neue\', Arial, Helvetica, Geneva, sans-serif; font-size:14px; }
    body { background-color: #2A374E; margin: 0; padding: 0; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; }
    h2{ padding-top:12px; /* ne fonctionnera pas sous Outlook 2007+ */color:#0E7693; font-size:22px; }

    </style>
   
</head>
<body style="margin:0px; padding:0px; -webkit-text-size-adjust:none;">

    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="background-color:rgb(42, 55, 78)" >
        <tbody>
            <tr>
                <td align="center" bgcolor="#2A374E">
                    <table  cellpadding="0" cellspacing="0" border="0">
                        <tbody>                            
                            <tr>
                                <td class="w640"  width="640" height="10"></td>
                            </tr>
                            <tr>
                                <td class="w640"  width="640" height="10"></td>
                            </tr>


                            <!-- entete -->
                            <tr class="pagetoplogo">
                                <td class="w640"  width="640">
                                    <table  style="margin-left: auto; margin-right: auto;" bgcolor="#F2F0F0">
                                        <tbody>
                                            <tr>
                                                <td class="w30"  width="30"></td>
                                                <td  style="margin-left: auto; margin-right: auto; ">
                                                    <div class="pagetoplogo-content">
                                                        <img class="w580" style="text-decoration: none; display: block; color:#476688; font-size:30px;" src="http://l4h.be/images/site/carte-bxl-accueil.png" alt="Mon Logo" width="334" height="103"/>
                                                    </div>
                                                </td> 
                                                <td class="w30"  width="30"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <!-- separateur horizontal -->
                            <tr>
                                <td  class="w640"  width="640" height="1" bgcolor="#d7d6d6"></td>
                            </tr>

                             <!-- contenu -->
                            <tr class="content">
                                <td class="w640" class="w640"  width="640" bgcolor="#ffffff">
                                    <table class="w640"  width="640" cellpadding="0" cellspacing="0" border="0">
                                        <tbody>
                                            <tr>
                                                <td  class="w30"  width="30"></td>
                                                <td  class="w580"  width="580">
                                                    <!-- une zone de contenu -->
                                                    <table class="w580"  width="580" cellpadding="0" cellspacing="0" border="0">
                                                        <tbody>                                                            
                                                            <tr>
                                                                <td class="w580"  width="580">
                                                                    <h2 style="color:#0E7693; font-size:22px; padding-top:12px;">
                                                                        Quelqu\'un propose ses services  </h2>

                                                                    <div align="left" class="article-content">';                                                              
                                                             
                                                             
                                                             
                                                             if(!empty($tab_users[$z]['visibiliteAdresse']))
                                                             	$adresse = $tab_users[$z]['rue'] . '+' . $tab_users[$z]['codePost'] . '+' . $tab_users[$z]['ville'];
                                                             else $adresse =  $tab_users[$z]['codePost'] . '+' .$tab_users[$z]['ville'];
                                                             $adresse = str_replace ( " ", "+", $adresse );
                                                             $adresse = str_replace ( "&#039;", "+", $adresse );
                                                             
                                                             $nbCatSelected = nbCatSelected($mysqli,$tab_users[$z]['id'],'offre');  
                                                             
                                                             if($nbCatSelected == 0)  {
                                                             	$nbCatSelected = $nbCat; 
                                                             	$tab['tag'] = -1;
                                                             }
                                                             else  $tab['tag'] = 0;
                                                             $i = 0;
                                                             $annonces = array();
                                                             $tab['premium'] = 1;
                                                             $tab_cat = randCategorieSelected($mysqli,$tab_users[$z]['id'],'offre');
                                                             
		                                                     while($i < $nbCatSelected && sizeof($annonces)<1 &&  $tab_cat > -1){
		                                                     	if($tab_cat == 0){ 
		                                                     		$tab['tag'] = -1;
		                                                     		$tab_cat = -1;
		                                                     	}
		                                                     	else{
																 $taille = sizeof($tab_cat);
                                                            	 $nb = rand(0,$taille-1);
                                                            	 $tab['tag'] = $tab_cat[$nb]['tagId'];
		                                                     	}
                                                             	 $annonces  = liste_offres_demandes($mysqli,'offre',$tab,false,$tab_users[$z]['id']);
                                                             	 if(sizeof($annonces)<1){
                                                             	 	if($tab_cat != -1){
                                                             	 		$tab_cat[$nb]['tagId'] = $tab_cat[sizeof($tab_cat)-1]['tagId'];
                                                             	 		unset($tab_cat[$taille-1]);
                                                             	 	}
                                                             	 }
                                                             	 $i++;
                                                             }
                                                             if(sizeof($annonces)<1)  $message .= 'Nous sommes désolé, nous n\'avons pas trouvé d\'offre vous correspondant.</br>';
                                                             else{
                                                             //tableau des annonces triées par distances
                                                             $annonces = tableau_annonces($adresse,$annonces,0,'distance');
                                                             
                                                             $dateExpiration = new DateTime ( $annonces [0]['dateExpiration'] );
                                                             $dateCreation = new DateTime ( $annonces [0]['dateCreation'] );
                                                             
                                                             $lien = racine_serveur().'/images/avatars/';
                                                             $emplacement = '../../images/avatars/';
                                                          	 $message .= '<div style="float:right;margin-left:20px;"><p>'.afficher_avatar($annonces[0]['id'],$lien,$emplacement,100,100).'</p></div>';
                                                          	 $message .=   '<h4>'.$annonces[0]['titre'].' <small>['.$annonces[0]['tagNom'].']</small></h4>';
                                                          	 $message .= '<div style="text-align:right;">par '.$annonces[0]['prenom'].' '.$annonces[0]['nom'].'</div>';
                                                          	 $message .=   $annonces[0]['annonce'].'</br>';
                                                         
                                                             $message .= '</br><p>Créée le '.$dateCreation->format ( 'd-m-Y H:i' ).'</p>';
                                                             $message .= '<p style="text-align:right;">Expirée le '.$dateExpiration->format ( 'd-m-Y H:i' ).'</p> ';
                                                             $message .= '<p style="text-align:right;">à '.distance_km($annonces[0]['distance']).' km de chez vous.</p></br>';
                                                           //  echo var_dump($annonces);
                                                             
                                                             }        

                                                         $message .='</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="w580"  width="580" height="1" bgcolor="#c7c5c5"></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <!-- fin zone -->                                                   

                                                    <!-- une autre zone de contenu -->
                                                    <table class="w580"  width="580" cellspacing="0" cellpadding="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td class="w580"  width="580">
                                                                    <h2 style="color:#0E7693; font-size:22px; padding-top:12px;">
                                                                        Quelqu\'un recherche vos services  </h2>

                                                                    <div align="left" class="article-content">';
                                                                        
                                                            
															if(!empty($_SESSION['visibiliteAdresse']))
                                                               	$adresse = $tab_users[$z] ['rue'] . '+' .$tab_users[$z]['codePost'] . '+' . $tab_users[$z] ['ville'];
                                                             else $adresse =  $tab_users[$z] ['codePost'] . '+' . $tab_users[$z] ['ville'];
                                                             $adresse = str_replace ( " ", "+", $adresse );
                                                             $adresse = str_replace ( "&#039;", "+", $adresse );
                                                             
                                                             
                                                             
															$nbCatSelected = nbCatSelected($mysqli,$tab_users[$z]['id'],'demande');  
                                                             
                                                             if($nbCatSelected == 0)  {
                                                             	$nbCatSelected = $nbCat; 
                                                             	$tab['tag'] = -1;
                                                             }
                                                             else  $tab['tag'] = 0;
                                                             $i = 0;
                                                             $annonces = array();
                                                             $tab['premium'] = 1;
                                                             $tab_cat = randCategorieSelected($mysqli,$tab_users[$z]['id'],'demande');
                                                             
		                                                     while($i < $nbCatSelected && sizeof($annonces)<1 &&  $tab_cat > -1){
		                                                     	if($tab_cat == 0){
		                                                     		$tab['tag'] = -1;
		                                                     		$tab_cat = -1;
		                                                     	}
		                                                     	else{
			                                                     	$taille = sizeof($tab_cat);
	                                                            	 $nb = rand(0,$taille-1);;
	                                                            	 $tab['tag'] = $tab_cat[$nb]['tagId'];
		                                                     	}
                                                            	 
                                                             	 $annonces  = liste_offres_demandes($mysqli,'demande',$tab,false,$tab_users[$z]['id']);
                                                             	 if(sizeof($annonces)<1){
                                                             	 	if($tab_cat != -1){
                                                             	 	$tab_cat[$nb]['tagId'] = $tab_cat[sizeof($tab_cat)-1]['tagId'];
                                                             	 	unset($tab_cat[$taille-1]);
                                                             	 	}
                                                             	 }
                                                             	 $i++;
                                                             }
                                                             if(sizeof($annonces)<1)  $message .= 'Nous sommes désolé, nous n\'avons pas trouvé de demande vous correspondant.</br>';
                                                             else{
                                                             //tableau des annonces triées par distances
                                                             $annonces = tableau_annonces($adresse,$annonces,0,'distance');
                                                             
                                                             $dateExpiration = new DateTime ( $annonces [0]['dateExpiration'] );
                                                             $dateCreation = new DateTime ( $annonces [0]['dateCreation'] );
                                                             
                                                             $lien = racine_serveur().'/images/avatars/';
                                                             $emplacement = '../../images/avatars/';
                                                          	 $message .= '<div style="float:left; margin-right:20px;"><p>'.afficher_avatar($annonces[0]['id'],$lien,$emplacement,100,100).'</p></div>';                                         
                                                          	 $message .=   '<h4>'.$annonces[0]['titre'].' <small>['.$annonces[0]['tagNom'].']</small></h4>';
                                                          	 $message .= '<div style="text-align:right;">par '.$annonces[0]['prenom'].' '.$annonces[0]['nom'].'</div>';
                                                             $message .=   $annonces[0]['annonce'].'</br>';
                                                         
                                                             $message .= '</br><p>Créée le '.$dateCreation->format ( 'd-m-Y H:i' ).'</p>';
                                                             $message .= '<p style="text-align:right;">Expirée le '.$dateExpiration->format ( 'd-m-Y H:i' ).'</p> ';
                                                             $message .= '<p style="text-align:right;">à '.distance_km($annonces[0]['distance']).' km de chez vous.</p></br>';
                                                             
                                                             }    
                                                             	
                                                                    
 
                                                           $message .= '</div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="w580"  width="580" height="1" bgcolor="#c7c5c5"></td>
                                                            </tr>

                                                        </tbody>
                                                    </table>

                                                    <table class="w580"  width="580" cellpadding="0" cellspacing="0" border="0">
                                                        <tbody>
                                                            <tr>
                                                                <td class="w580"  width="580">
                                                                   <h2 style="color:#0E7693; font-size:22px; padding-top:12px;">
                                                                      Plus d\'offres ou demandes ? </h2>
                                                           			<div align="left" class="article-content">
                                                           				Venez vite visitez notre site.Et n\'hésitez pas à sélectionner vos catégories d\'offres et demandes de préférences dans le profil.</br>
                                                           				<a href="http://l4h.be">Looking for help</a>
                                                           			</div>
                                                                </td>
                                                           		<td class="w180"  width="180" valign="top">
                                                                    <div align="left" class="article-content">
                                                                        <p><img class="w180" width="180" src="http://l4h.be/images/site/l4h.png" alt="logo"/></p>
                                                                    </div>
                                                                </td>
                                                           		
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                                <td class="w30" class="w30"  width="30"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>

                            <!--  separateur horizontal de 15px de  haut-->
                            <tr>
                                <td class="w640"  width="640" height="15" bgcolor="#ffffff"></td>
                            </tr>

                            <!-- pied de page -->
                            <tr class="pagebottom">
                                <td class="w640"  width="640">
                                    <table class="w640"  width="640" cellpadding="0" cellspacing="0" border="0" bgcolor="#c7c7c7">
                                        <tbody>
                                            <tr>
                                                <td colspan="5" height="10"></td>
                                            </tr>
                                            <tr>
                                                <td class="w30"  width="30"></td>
                                                <td class="w580"  width="580" valign="top">
                                                <td class="w30"  width="30"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" height="10"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="w640"  width="640" height="60"></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body></html>';                                             
if( envoi_email ('Newsletter - L4H', $message, $tab_users[$z]['email'])) 
                                       $nbmail++;    
} 
    
 
 
 echo $nbmail.' newsletters on été envoyées avec succès!';
 updateDateNewsletter($mysqli,$nbmail);
 $mysqli->close();
}
else redirect('/php/administration/administration.php');
if(!isset($_POST['valider'])){
	$mysqli = connect_bdd();
	$data = lastNewsletter($mysqli);
	$date  = new DateTime ( $data['date'] );
	$nbNewsletters = $data['nbNewsletters'];
?>	

                <p>Etes-vous sûr d'envoyer une newsletter à tous les utilisateurs?</p>
                <?php if(empty($data)){ echo '<p>Aucune newsletter n\'a été envoyée </p>'; }else{?>
                <p><?php echo $nbNewsletters;?> newsletters on été envoyées pour la dernière fois le <?php echo $date->format ( 'd-m-Y H:i' );?></p><?php }?>
                
                <form action="" method="post">
                    <button type="submit" value="oui" name="valider" class="btn btn-primary">Oui</button>
                    <button type="submit" value="non" name="valider" class="btn btn-default">Non</button>
              </form>

	
<?php 	
$mysqli->close();
} 
 
 ?>
 
     </div>
</div>