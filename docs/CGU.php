<?php
	include('../php/header.php');

  	echo"</br></br></br></br></br>";

?>
<div class="container">
    	<h3>CONDITIONS GENERALES D’UTILISATION DU SITE INTERNET l4h.be</h3>
    	</br></br>
		<ol type="I">
			<li> Acceptation des conditions générales d’utilisation </li></br> 
				L’accès au site web de l4h.be (ci-après dénommé « L4H ») se fait sous réserve de l’acceptation des présentes conditions d'utilisation (ci-après dénommées « conditions ») que L4H sera libre de modifier à tout moment en plaçant directement sur le site web des notifications de changement ou des liens vers de telles notifications.
				Toute notification pourra également vous être adressée par e-mail ou par courrier postal. Ces règles et lignes de conduite sont réputées faire partie intégrante des conditions. Il vous est donc conseillé de vous référer régulièrement à la dernière version des présentes conditions disponibles en permanence à l'adresse suivante : <a href="http://l4h.be/docs/CGU.php">www.l4h.be/docs/CGU.php</a>.
				L'utilisation de certains services de L4H peut parfois nécessiter le respect de règles ou lignes de conduite additionnelles et spécifiques qui seront indiquées dans une charte disponible sur le site. Toute notification pourra vous être adressée par e-mail ou par courrier postal. Ces règles et lignes de conduite seront alors réputées faire partie intégrante des présentes conditions. 
				L4H pourrait enfin proposer d'autres services pour lesquels des conditions d'utilisation spécifiques devront être acceptées avant toute utilisation.
				Les présentes conditions traduisent l'intégralité de l’accord entre vous et L4H concernant l’accès et l’utilisation du site web. Elles annulent et remplacent toutes les communications et propositions antérieures ou contemporaines, sous forme électronique, écrites ou orales, entre vous et L4H.
			
			</br></br></br>	
			<li> Description sommaire du service offert par L4H </li></br>
				L4H met à votre disposition, via son site web, un outil convivial et innovant vous permettant :
				
				<ul>
					<li>de gérer à distance les données vous concernant ; </li>
					<li>d’accéder à des offres de services et de poster des demandes de services ;</li>
					<li>d’être mis en relation avec d’autres membres en vue d’échanger des services ;</li>
					<li>de s’abonner moyennant payement (10€ pour 31 jours) en vue d’obtenir plus de fonctionnalités ;</li>
					<li>etc ;</li>
				</ul>
				
			</br></br>
			<li>Modalités d’accès – confidentialité de votre login et mot de passe</li></br>
				En accédant au site web de L4H, vous acceptez de fournir des informations exactes, à jour et complètes et d'en assurer la mise à jour régulière.
				Dans le cas contraire, L4H sera en droit de suspendre ou de résilier votre compte et de vous refuser l'accès à tout ou partie du site web.
				Afin d’accéder à nos services, vous devrez posséder un login et un mot de passe. 
				Vous êtes seul responsable de la conservation du caractère confidentiel de votre login et mot de passe et de toute(s) action(s) qui pourrai (en) t être faite(s) de votre compte avec votre login et/ou mot de passe. 
				Vous vous engagez expressément (1) à informer immédiatement L4H de toute utilisation non autorisée de votre compte et/ou de votre mot de passe et/ou de toute atteinte à la sécurité, et (2) à vous assurer qu'à l'issue de chaque session vous vous déconnectez effectivement du site web.
				L4H ne pourra en aucun cas être tenu responsable de toute perte ou dommage survenant en cas de manquement aux obligations du présent paragraphe.
			
			</br></br></br>
			<li>Conditions d’utilisation du site web L4H</li></br>
				L4H peut poser des règles générales et des limites quant à l'utilisation du service, et notamment, sans que cette énumération soit limitative, fixer un espace mémoire maximum qui vous sera alloué sur les serveurs de L4H, fixer un nombre de jours maximum pendant lesquels les messages seront affichés sur le forum de discussion, etc.
				L4H n'offre aucune garantie en cas de suppression ou défaut de stockage de tout message, communication ou tout autre contenu diffusé ou transmis via le site web.
				L4H est enfin libre de supprimer tout compte resté inutilisé pendant un certain temps ou ne respectant pas les conditions présentes.</br>
				Vous vous interdisez expressément de :
				
				<ul>
					<li>télécharger, envoyer, transmettre par e-mail ou de toute autre manière tout
						contenu qui soit illégal, nuisible, menaçant, abusif, constitutif de harcèlement, diffamatoire, vulgaire, obscène, menaçant pour la vie privée d'autrui, haineux, raciste, ou autrement répréhensible ;</li>
					<li>consulter, afficher, télécharger, transmettre, tout contenu qui serait contraire à la
						loi belge ;</li>
					<li>porter atteinte d'une quelconque manière aux utilisateurs mineurs ;</li>
					<li>tenter d'induire en erreur d'autres utilisateurs en usurpant le nom ou la dénomination sociale d'autres personnes et plus particulièrement en vous faisant passer pour un employé ou un affilié de L4H, un modérateur, un guide, ou un hébergeur ;</li>
					<li>télécharger, afficher, transmettre par e-mail ou de toute autre manière tout contenu violant tout brevet, marque déposée, secret de fabrication, droit de propriété intellectuelle ou tout autre droit de propriété appartenant à autrui ;</li>
					<li>télécharger, afficher, transmettre par e-mail ou de toute autre manière toute publicité ou tout matériel promotionnel non sollicités ou non autorisés (notamment se livrer à du « spam », à la transmission de « junk mail », de chaîne de lettres ou toute autre forme de sollicitation) ;</li>
					<li>télécharger, afficher, transmettre par e-mail ou de toute autre manière tout contenu comprenant des virus informatiques ou tout autre code, dossier ou programme conçus pour interrompre, détruire ou limiter la fonctionnalité de tout logiciel, ordinateur, ou outil de télécommunication sans que cette énumération ne soit limitative ;</li>
					<li>commettre toute action ayant un effet perturbateur entravant la capacité des utilisateurs d’accéder à L4H ;</li>
					<li>entraver ou perturber le service, les serveurs, les réseaux connectés au service, ou refuser de se conformer aux conditions requises, aux procédures, aux règles générales ou aux dispositions réglementaires applicables aux réseaux connectés au service ;</li>
					<li>violer, intentionnellement ou non, toute loi ou réglementation nationale ou internationale en vigueur et toutes autres règles ayant force de loi ;</li>
					<li>harceler de quelque manière que ce soit un autre ou plusieurs autres utilisateurs ;</li>
					<li>collecter et stocker des données personnelles afférentes aux autres utilisateurs.</li>
					<li>reproduire, copier, vendre, revendre, ou exploiter dans un but commercial quel qu'il soit toute partie du Service, toute utilisation du Service, ou tout droit d'accès au Service.</li>
				</ul>
				
			</br></br>
			<li>Suspension et interruption de l’accès au site web</li></br>
				L4H se réserve le droit, à tout moment et pour quelque motif que ce soit, de modifier ou interrompre temporairement ou de manière permanente l’accès à tout ou partie du site web, et ce sans devoir vous en informer préalablement.
				L4H ne pourra être tenu responsable pour tout dommage direct ou indirect lié à une modification, suspension ou interruption du site web. Vous reconnaissez à L4H le droit de mettre fin à tout ou partie du droit d'accès correspondant à votre compte et votre mot de passe, voire de supprimer votre compte et mot de passe, pour tout motif et sans avertissement préalable, notamment si L4H peut légitimement croire que vous avez violé ou agi en contradiction avec les présentes conditions générales et/ou toute prescription légale en vigueur.
				L4H ne pourra être tenu pour responsable pour tout dommage direct ou indirect résultant de la résiliation de votre accès au site web, pour quel que motif que ce soit.
			
			</br></br></br>
			<li>Liens hypertextes</li></br>
				L4H n’exerce aucun contrôle sur les sites pointés et, plus généralement, sur les ressources Internet visées.
				L4H n’est dès lors pas responsable de la mise à disposition de ces sites et sources externes, et ne peut supporter aucune responsabilité quant au contenu, publicités, produits, services ou tout autre matériel disponible sur ou à partir de ces sites ou sources externes.
				L4H ne pourra être tenu responsable de tous dommages ou pertes avérés ou allégués consécutifs ou en relation avec l'utilisation ou avec le fait d'avoir fait confiance au contenu, à des biens ou des services disponibles sur ces sites ou sources externes. 
				Enfin, si dans le cadre d'une recherche conduite sur L4H, le résultat de celle-ci vous amenait à pointer sur des sites, des pages ou des forums dont le titre et/ou les contenus constituent une infraction à la loi belge, compte tenu notamment du fait que L4H ne saurait contrôler le contenu de ces sites et sources externes, vous devez interrompre votre consultation du site concerné sauf à encourir les sanctions prévues par la législation belge ou à répondre des actions en justice initiées à votre encontre.
			
			</br></br></br>
			<li>Droits intellectuels</li></br>
				Les informations, logos, dessins, marques, modèles, slogans, etc. qui vous sont présentés sur le site L4H sont protégés par le droit de la propriété intellectuelle (droit d’auteur, droit des marques, droit des brevets, etc.).
				Il en est de même en ce qui concerne les éléments faisant partie du site web L4H.
				Sauf expressément autorisé à cet effet par L4H ou par l'annonceur publicitaire éventuel, vous n’êtes pas autorisés à modifier, reproduire, louer, emprunter, vendre, distribuer ou créer d’œuvres dérivées basées en tout ou partie sur les éléments présents sur le site. Il vous est interdit (et vous ne pouvez accorder à autrui l'autorisation) de copier, modifier, créer une oeuvre dérivée, inverser la conception ou l'assemblage ou de toute autre manière tenter de trouver le code source, vendre, attribuer, sous licencier ou transférer de quelque manière que se soit tout droit afférent au service. 
				Vous vous engagez enfin à ne modifier en aucune manière le site web ou à ne pas utiliser de versions modifiées du site web notamment (sans que cette énumération soit limitative) pour obtenir un accès non autorisé au site web. Il vous est interdit d’accéder au site web par un autre moyen que par l'interface qui vous est fournie par L4H.
			
			</br></br></br>
			<li>Loi applicable et juridiction compétente</li></br>
				Les présentes conditions générales d'utilisation sont régies par la loi belge.
				Tout litige sera soumis à la compétence exclusive des tribunaux de Bruxelles (Belgique).
				
			</br></br></br>
			<li>Contact</li></br>
				<h4>Looking for help </h4>
				Avenue du ciseau, 15 </br>
				1348 Louvain-la-Neuve </br>
				Belgique </br>
				<a href="mailto:webmaster@l4h.be">webmaster@l4h.be</a> </br>
				
		</ol>
</div>
<?php 
include ('../php/footer.php'); 
?>


