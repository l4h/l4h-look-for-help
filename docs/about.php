<?php
	include('../php/header.php');

  	echo"</br></br></br></br></br>";

?>
<div class="container">
	<div class="row">
			<div class="col-md-7">
			</br></br></br></br></br>
				<h2>
					Qui sommes nous ?</br>
				</h2>
				<p class="lead">
					Looking for help est un site web d'entraide sur lequel vous pouvez
					non seulement trouver de l'aide, mais également proposer la vôtre !
					Nous proposons un service unique dans votre région: un service d'entraide,
					convivial et surtout gratuit !
				</p>	
			</div>
						<div class="col-md-5">
				<img class ="pull-left" 
					src="/images/site/l4h.png" style="heigt:100%;width:100%;"
					alt="Generic placeholder image">
			</div>
	</div>
	<hr>
	<div class="row">
			<div class="col-md-5">
			</br></br>
				<img 
					src="/images/site/listeOffres.png" style="heigt:100%;width:100%;"
					alt="Image">
			</div>
			<div class="col-md-7">
				<h2>
					Comment cela fonctionne-t-il?</br> 
					<span class="text-muted">Vous avez besoin d'aide? </span>
				</h2>
				<p class="lead">
				Vous êtes au bon endroit! En effet, après une brève inscription et la confirmation
				de votre adresse email, vous pourrez directement poster une demande d'aide sur
				looking for help! Dès lors, les personnes voulant vous aider pourrons rentrer
				en contact avec vous pour proposer leurs services. Libre à vous ensuite de 
				choisir le prestataire de votre choix!
				</p>
				<h2>
					<span class="text-muted">Vous proposez de l'aide? </span>
				</h2>
				<p class="lead">
				Vous êtes également au bon endroit! Une fois inscris et votre adresse email
				vérifiée, vous pourrez directement proposer votre aide sur looking for help !
				Vous pourrez également proposer vos services aux personnes demandant de l'aide.
				</p>
			</div>
	</div>
	<hr>
	<div class="row">
			<div class="col-md-7">
				<h2>
					Combien cela coûte-t-il?<br/>
				</h2>
					<p class="lead">
						Notre compte premium est la pour vous! En effet, pour seulement 10€ par mois,
						vous aurez non seulement la possibilité de proposer plusieurs services mais
						également la possibilité d'en demander plusieurs sans devoir attendre que votre
						ancienne demande/offre arrive à expiration.
					</p>
					<p class="lead">
						De plus vous obtiendrez des statistiques détaillées sur les demandeurs 
						ainsi que les prestataires de services, des statistiques par ville ou 
						région, et bien plus encore.
					</p>
			</div>
			<div class="col-md-5">
			</br></br>
				<img style="heigt:100%;width:100%;"
					src="/images/site/euro.jpg" alt="Euro">
			</div>
	</div>
	<hr>
	<div class="row">
			<div class="col-md-6">
				<img style="heigt:100%;width:100%;"
					src="/images/site/grapheAbout.PNG" alt="Graphe">
			</div>
			<div class="col-md-6">
				<h2>
					L4H en quelques chiffres<br/>
				</h2>
				<p class="lead">
				<?php 
					// demandes
					$mysqli = connect_bdd();
					$result = get_count_total_site($mysqli,"demande");
					$data = $result->fetch_array();
					$totDem = $data ['nbTagId'];
					
					// offres
					$result = get_count_total_site($mysqli,"offre");
					$data = $result->fetch_array();
					$totOff = $data ['nbTagId'];
					
					// users
					$result = get_nb_usr($mysqli);
					$data = $result->fetch_array();
					$totUsr = $data['tot'];
					$mysqli->close();
					
				?>
				Looking for help ce n'est pas moins de :
				<ul>
					<li>7 développeurs à temps plein pour assurer un suivi parfait du site,</li>
					<li><?php echo $totDem;?> demandes créées,</li>
					<li><?php echo $totOff;?> offres créées,</li>
					<li><?php echo $totUsr;?> utilisateurs du site,</li>
					<li>et bien plus encore...</li>
				</ul>
				Alors, rejoignez-nous vite en cliquant <a href="/php/inscription/inscription.php">ici</a>
			</div>
		</div>
</div>

		<!-- /END THE FEATURETTES -->
	<?php 
include ('../php/footer.php'); 
?>